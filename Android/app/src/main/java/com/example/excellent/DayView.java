/*
 * Excellent - Student Organizer
 * Copyright (c ) 2014, Naskrina, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */

package com.example.excellent;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.concurrent.ExecutionException;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import excellent.helpers.BasicEntityDAO;
import excellent.helpers.HelperFactory;
import excellent.tasks.LoginTask;
import excellent.tasks.SyncTask;

public class DayView extends Activity implements OnClickListener{

	private Button currentDay; 
	private ImageView nextDay; 
	private ImageView prevDay; 
	private final DateFormat dateFormatter = new DateFormat();
	private static final String dateTemplate = "D MMMM yyyy";
	private GregorianCalendar myCal; 
	private MyDate day; 
	private RelativeLayout rl;
	private static final int REQUEST_CODE = 1; 
	private int date;
	private int weekday;
	private int month;
	private int year; 
	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_day_view);
        
        Bundle b = getIntent().getExtras();
        date = b.getInt("date"); 
        weekday = b.getInt("weekday");
        month = b.getInt("month"); 
        year = b.getInt("year"); 
        
        myCal = new GregorianCalendar(year, month, date); 
        
        day = new MyDate(myCal.get(myCal.DAY_OF_WEEK), myCal.get(myCal.DATE), myCal.get(myCal.MONTH), myCal.get(myCal.YEAR)); //date the user is currently on 
        
        currentDay = (Button) this.findViewById(R.id.currentDay);
		currentDay.setText(day.getMonthString(month) + " " + date + " " + year);
		
		nextDay = (ImageView) this.findViewById(R.id.nextDay); 
		nextDay.setOnClickListener(this); 
		
		prevDay = (ImageView) this.findViewById(R.id.prevDay); 
		prevDay.setOnClickListener(this); 
		
		rl = (RelativeLayout) findViewById(R.id.day_event_frame);

        addEventsOnView();
	}

    private void addEventsOnView() {
        try {
            BasicEntityDAO<excellent.models.Event> eventsDAO
                    = HelperFactory.getDatabaseHelper().getBasicEntityDAO(excellent.models.Event.class);
            List<excellent.models.Event> events = eventsDAO.getUserEntities(GlobalState.getInstance().getCurrentUser());
            for(excellent.models.Event thisEvent:events) {
                Date startTime = thisEvent.getStartTime();
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(startTime);
                if(myCal.get(Calendar.DAY_OF_MONTH) == calendar.get(Calendar.DAY_OF_MONTH)
                        && myCal.get(myCal.MONTH) == calendar.get(Calendar.MONTH)
                        && myCal.get(myCal.YEAR) == calendar.get(Calendar.YEAR) ){
                    addEventView(this, rl, thisEvent);
                }
                Log.d("Events present in databases have ID: ", " " + thisEvent.getId());
            }
        } catch (SQLException e) {
            Log.e("DayView", e.getMessage());
        }
    }
	
	@Override
	public void onClick(View v) {
		
		if(v == prevDay){
			myCal.add(myCal.DATE, -1); 
			Log.d("New date is ", + myCal.get(myCal.MONTH) + " " + myCal.get(myCal.DATE)); 
			
	        setContentView(R.layout.activity_day_view);
	        currentDay = (Button) this.findViewById(R.id.currentDay);
			currentDay.setText(day.getMonthString( myCal.get(myCal.MONTH)) + " " + myCal.get(myCal.DATE) + " " + myCal.get(myCal.YEAR));
			
			nextDay = (ImageView) this.findViewById(R.id.nextDay); 
			nextDay.setOnClickListener(this); 
			
			prevDay = (ImageView) this.findViewById(R.id.prevDay); 
			prevDay.setOnClickListener(this); 
			
			rl = (RelativeLayout) findViewById(R.id.day_event_frame);

            addEventsOnView();
		}
		
		if(v == nextDay){
			myCal.add(myCal.DATE, 1); 
			
			Log.d("New date is ", + myCal.get(myCal.MONTH) + " " + myCal.get(myCal.DATE)); 
			
	        setContentView(R.layout.activity_day_view);
	        currentDay = (Button) this.findViewById(R.id.currentDay);
			currentDay.setText(day.getMonthString( myCal.get(myCal.MONTH)) + " " + myCal.get(myCal.DATE) + " " + myCal.get(myCal.YEAR));
			
			nextDay = (ImageView) this.findViewById(R.id.nextDay); 
			nextDay.setOnClickListener(this); 
			
			prevDay = (ImageView) this.findViewById(R.id.prevDay); 
			prevDay.setOnClickListener(this); 
			
			rl = (RelativeLayout) findViewById(R.id.day_event_frame);

            addEventsOnView();
		}
	}

	public void addEventView(Context ctx, RelativeLayout rl, excellent.models.Event event){
		int width = 443; 
		int leftMargin = 24;  

        long duration = (event.getEndTime().getTime() - event.getStartTime().getTime()) / 1000 ;

		TextView tv = new TextView(ctx);
		tv.setBackgroundResource(R.color.orange);
		tv.setText(event.getName());
		tv.setHeight((int) (duration * getResources().getDisplayMetrics().density));
		tv.setWidth((int) (width * getResources().getDisplayMetrics().density)); 
		tv.setTag(event); 
		
		int topMargin = 0;
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(event.getStartTime());
		int startHour = calendar.get(Calendar.HOUR_OF_DAY);
		int startMinutes = calendar.get(Calendar.MINUTE);
		switch(startHour){
			case 7 : 	topMargin += 1; 
						break; 
			case 8: 	topMargin += 61; 
						break; 
			case 9: 	topMargin += 122; 
						break;
			case 10: 	topMargin += 182; 
						break;
			case 11: 	topMargin += 243; 
						break; 
			case 12: 	topMargin += 304; 
						break; 
			case 13: 	topMargin += 364; 
						break; 
			case 14: 	topMargin += 425; 
						break;
			case 15: 	topMargin += 486; 
						break; 
			case 16: 	topMargin += 547; 
						break; 
			case 17: 	topMargin += 607; 
						break; 
			case 18: 	topMargin += 668;
						break; 
			case 19: 	topMargin += 729; 
						break; 
		}
		
		topMargin += startMinutes; 
		
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
		params.leftMargin = (int) (leftMargin * getResources().getDisplayMetrics().density);
		params.topMargin = (int) (topMargin * getResources().getDisplayMetrics().density);
		tv.setOnClickListener(new OnClickListener() {
			   public void onClick(View v) {
                   excellent.models.Event event = (excellent.models.Event)v.getTag();
				   Log.d("View clicked", event.getDescription());

				   Bundle b = new Bundle(); //create new bundle to pass info via intent into new activity
                   b.putString("name", event.getName());
                   b.putString("description", event.getDescription());
				   b.putString("id", event.getId());

				   Intent newActivity = new Intent("EventView");
				   newActivity.putExtras(b);
	        	   startActivityForResult(newActivity, 0);
			   }
		});
		
		rl.addView(tv, params);
	}
	
	public void onPause(){
		super.onPause(); 
	}
	
	@Override
	protected void onResume() {
	    super.onResume();
		addEventsOnView();
	}


	  @Override
	  protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
 
	    if (resultCode == 1 && requestCode == 0) {
	      if (intent.hasExtra("result") && intent.getExtras().getString("result").equals("Event Deleted")) {
	        Log.d("onActivityResult", "Event Deleted");
	        
	        setContentView(R.layout.activity_day_view);
	        currentDay = (Button) this.findViewById(R.id.currentDay);
			currentDay.setText(day.getMonthString(month) + " " + date + " " + year);
			
			nextDay = (ImageView) this.findViewById(R.id.nextDay); 
			nextDay.setOnClickListener(this); 
			
			prevDay = (ImageView) this.findViewById(R.id.prevDay); 
			prevDay.setOnClickListener(this); 
			
			rl = (RelativeLayout) findViewById(R.id.day_event_frame);
			
			addEventsOnView();

	      }
	      
	    }
	  }
	  
	    @Override
	    public boolean onCreateOptionsMenu(Menu menu) {
	        getMenuInflater().inflate(R.menu.activity_add_event_view, menu);
	        return true;
	    }
	    
	    @Override
	    public boolean onOptionsItemSelected(MenuItem item) {
	        switch (item.getItemId()) {
	            case R.id.activity_add_event_view:
	            	Intent intent = new Intent(this, AddEventView.class);
	                startActivity(intent);
	                return true;
                case R.id.login_button:
                    LoginTask loginTask = new LoginTask(this, "Sergey", "123");

                    try {
                        loginTask.execute().get();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    }
                    return true;
                case R.id.sync_button:
                    SyncTask syncTask = new SyncTask(this);
                    syncTask.execute();
                    return true;
	            default:
	                return super.onOptionsItemSelected(item);
	        }
	    }
}

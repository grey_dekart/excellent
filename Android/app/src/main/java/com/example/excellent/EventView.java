/*
 * Excellent - Student Organizer
 * Copyright (c ) 2014, Naskrina, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */

package com.example.excellent;

import java.sql.SQLException;
import java.util.GregorianCalendar;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import excellent.helpers.BasicEntityDAO;
import excellent.helpers.HelperFactory;
import excellent.models.*;

public class EventView extends Activity implements OnClickListener{
	
	//event info
	private String eventId;
	//calendar and date display
	private final DateFormat dateFormatter = new DateFormat();
	private static final String dateTemplate = "MMMM dd yyyy";
	GregorianCalendar myCal;
	//private TextView eventInfo; 
	private TextView dateView;
	private TextView timeView;
	private TextView subjectView;
	private TextView eventTypeView; 
	private TextView locationView; 
	private TextView descriptionView; 
	private Button editEvent;
	private Button deleteEvent; 
	private Button createEvent; 
	//dialog constants
	private static final int DELETE_EVENT = 0; 
	//database
	private Context ctx = this; 
	
	private static final String tag = "EventView"; 

	String name;
	String description; 
	

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_view);     
        
        Bundle b = getIntent().getExtras();
        eventId = b.getString("id");
        name = b.getString("name");
        description= b.getString("description");;

        
        eventTypeView = (TextView) this.findViewById(R.id.eventTableEvent);
        eventTypeView.setText(name);
        
        descriptionView = (TextView) this.findViewById(R.id.eventTableDescription);
        descriptionView.setText(description); 
        
        createEvent = (Button) this.findViewById(R.id.createEventView); 
        createEvent.setOnClickListener(this); 
        createEvent.setTag("create"); 
        
        editEvent = (Button) this.findViewById(R.id.editEventView); 
        editEvent.setOnClickListener(this); 
        editEvent.setTag("edit"); 
        
        deleteEvent = (Button) this.findViewById(R.id.deleteEventView);
        deleteEvent.setOnClickListener(this); 
		deleteEvent.setTag("delete"); 
	}


	@Override
	public void onClick(View v) {
		
		String tag = (String) v.getTag(); 
		Log.d("View pressed", tag); 
		
		if(tag.equals("edit")){
			//create intent to event EDIT activity 
			Log.d(tag, "Button pressed - Edit"); 
			Bundle b = new Bundle(); //create new bundle to pass info via intent into new activity
			b.putString("id", eventId);
			b.putString("name", name);
			b.putString("description", description); 

			Intent newActivity = new Intent("EditEventView");
			newActivity.putExtras(b); //Put your id to your next Intent
			//startActivityForResult(newActivity, 0);
			startActivity(newActivity);
			EventView.this.finish();
			
		}else if(tag.equals("delete")){
			//first, load dialog prompt confirming deletion
			Log.d(tag, "Button pressed - Delete"); 
			showDialog(DELETE_EVENT);

		}else if(tag.equals("create")){
			//create intent to event CREATE activity
			Log.d(tag, "Button pressed - Create"); 
			Intent newActivity = new Intent("AddEventView");
			//startActivityForResult(newActivity, 0);
			startActivity(newActivity); 
			EventView.this.finish();
 
		}

	}//end onClick()
	
	@Override
	protected Dialog onCreateDialog(int id){
	    switch (id) {
	    case DELETE_EVENT:
	        AlertDialog.Builder builder = new AlertDialog.Builder(this);
	        builder.setTitle("Dialog Title");
	        //builder.setIcon(android.R.drawable.btn_star);
	        builder.setMessage("Would you like to DELETE this Event?");
	        builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
	                  public void onClick(DialogInterface dialog, int which) {
	                		//if YES, make SQL call and delete event entry
	                			Log.d(tag, "User confirmed dialog option delete"); 
	                			//flag has been set to true by user, in the dialog selection. Delete event
                          try {
                              BasicEntityDAO<excellent.models.Event> eventsDAO
                                      = HelperFactory.getDatabaseHelper().getBasicEntityDAO(excellent.models.Event.class);

                              if(GlobalState.getInstance().getCurrentUser() == null) {
                                  eventsDAO.deleteById(eventId);
                              } else {
                                  Event event = eventsDAO.queryForId(eventId);
                                  event.setDeleted(true);
                                  eventsDAO.update(event);
                              }
                          } catch (SQLException e) {
                              Log.e(tag, e.getMessage());
                          }
	                			 
	                			EventView.this.finish();//close this EventView as this event no longer exists - return to previous view 

	                        Toast.makeText(getApplicationContext(),
	                        "Event Deleted", Toast.LENGTH_SHORT).show();
	                        return;
	                } });
	        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
	        	//user does not want to delete event - simply return without doing anything
	                  public void onClick(DialogInterface dialog, int which) {
	                        Toast.makeText(getApplicationContext(),
	                        "No Changes Made", Toast.LENGTH_SHORT).show();
	                      return;
	                } });
	        return builder.create();
	    }
	    return null;
	}//end onCreateDialog()
	  
	
}//end Class

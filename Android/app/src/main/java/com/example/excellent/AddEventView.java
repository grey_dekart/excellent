package com.example.excellent;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import excellent.helpers.BasicEntityDAO;
import excellent.helpers.HelperFactory;

public class AddEventView extends Activity implements OnClickListener{

	private static final String tag = "AddEvent"; 
	private EditText subjectEdit; 
	private EditText eventTypeEdit;
	private EditText locationEdit; 
	private EditText descriptionEdit;
	private EditText durationEdit; 
	
	private int duration; 
	private String subject;
	private String eventType;
	private String location;
	private String description;
	
	private TimePicker timepicker;
	private DatePicker datepicker; 
	private Button saveButton; 
	private Button cancelButton; 
	
	private int hour; 
	private int minutes; 
	private int date; 
	private int month; 
	private int year; 

	private Context ctx = this; 
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_event_view);
        
                subjectEdit = (EditText)this.findViewById(R.id.add_event_subject_edit);
        eventTypeEdit = (EditText)this.findViewById(R.id.add_event_type_edit); 
        locationEdit = (EditText)this.findViewById(R.id.add_event_location_edit); 
        descriptionEdit = (EditText)this.findViewById(R.id.add_event_description_edit); 
        durationEdit = (EditText)this.findViewById(R.id.add_event_duration_edit); 
        
        timepicker = (TimePicker) findViewById(R.id.addEventTimePicker);
        datepicker = (DatePicker) findViewById(R.id.addEventDatePicker);
        
        saveButton = (Button) findViewById(R.id.saveAddEventButton); 
        saveButton.setOnClickListener(this); 
        
        cancelButton = (Button) findViewById(R.id.cancelAddEventButton);
        cancelButton.setOnClickListener(this); 
        
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_add_event_view, menu);
        return true;
    }

	@Override
	public void onClick(View v) {
		
		if(v == saveButton){

try {
    subject = subjectEdit.getText().toString();
    eventType = eventTypeEdit.getText().toString();
    location = locationEdit.getText().toString();
    description = descriptionEdit.getText().toString();
    duration = Integer.parseInt(durationEdit.getText().toString());

    hour = timepicker.getCurrentHour();
    minutes = timepicker.getCurrentMinute();

    date = datepicker.getDayOfMonth();
    month = datepicker.getMonth();
    year = datepicker.getYear();

    Log.d(tag, "Subject: " + subject);
    Log.d(tag, "Event Type: " + eventType);
    Log.d(tag, "Location: " + location);
    Log.d(tag, "Description: " + description);
    Log.d(tag, "Duration: " + duration);
    Log.d(tag, "Hour: " + hour);
    Log.d(tag, "Minutes: " + minutes);
    Log.d(tag, "Date: " + date);
    Log.d(tag, "Month: " + month);
    Log.d(tag, "Year: " + year);

    String timeString = hour + ":" + minutes;
    try {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, date, hour, minutes);
        Date startDate = calendar.getTime();

        BasicEntityDAO<excellent.models.Event> eventsDAO
                = HelperFactory.getDatabaseHelper().getBasicEntityDAO(excellent.models.Event.class);
        excellent.models.Event event = new excellent.models.Event();
        event.setName(eventType);
        event.setUserField(GlobalState.getInstance().getCurrentUser());
        event.setStartTime(startDate);
        event.setEndTime(new Date(startDate.getTime() + duration));
        event.setDescription(description);
        eventsDAO.createEntity(event);
    } catch(SQLException ex) {
        Log.e(tag, ex.getMessage());
    }
    //create toast to let the user know that change was made and event was added
    Toast.makeText(getBaseContext(), "Your event was saved", Toast.LENGTH_SHORT).show();

    AddEventView.this.finish();//return to previous activity
}
catch (Exception ex){
};
	        
		}else if(v == cancelButton){
			//cancel - go back to previous screen 
			AddEventView.this.finish();
		}
	}//end onClick
	
}

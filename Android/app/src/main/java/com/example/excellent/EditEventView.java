/*
 * Excellent - Student Organizer
 * Copyright (c ) 2014, Naskrina, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */

package com.example.excellent;

import android.os.Bundle;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;

import excellent.helpers.BasicEntityDAO;
import excellent.helpers.HelperFactory;

public class EditEventView extends Activity implements OnClickListener {

	private static final String tag = "EditEvent"; 
	private EditText subjectEdit;
	private EditText eventTypeEdit;
	private EditText locationEdit;
	private EditText descriptionEdit;
	private EditText durationEdit;
	
	private TimePicker timepicker;
	private DatePicker datepicker; 
	private Button saveButton; 
	private Button cancelButton; 

	private String id;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_event_view);
        
        
        //get event id that was passed to it, to be able to access and edit that particular event's info by id 
        Bundle b = getIntent().getExtras();
        id = b.getString("id");
        String name = b.getString("name");
        String description= b.getString("description");
        
        //get references to EditTexts in view
        subjectEdit = (EditText)this.findViewById(R.id.edit_event_subject_edit); 
        //subjectEdit.setText(subjectOld);
        
        eventTypeEdit = (EditText)this.findViewById(R.id.edit_event_type_edit); 
        eventTypeEdit.setText(name);
        
        locationEdit = (EditText)this.findViewById(R.id.edit_event_location_edit);
        //locationEdit.setText(locationOld);
            
        durationEdit = (EditText)this.findViewById(R.id.edit_event_duration_edit); 
        //durationEdit.setText(durationOld + " ");
        
        descriptionEdit = (EditText)this.findViewById(R.id.edit_event_description_edit); 
        descriptionEdit.setText(description);

        /*String[] timeArr = startTimeOld.split(":");
        int currentHour = Integer.parseInt(timeArr[0]); 
        int currentMinute = Integer.parseInt(timeArr[1]);*/
        timepicker = (TimePicker) findViewById(R.id.editEventTimePicker);
        //timepicker.setCurrentHour(currentHour);
        //timepicker.setCurrentMinute(currentMinute);
        
        datepicker = (DatePicker) findViewById(R.id.editEventDatePicker);
        //datepicker.updateDate(yearOld, monthOld, dateOld);
        
        saveButton = (Button) findViewById(R.id.saveEditEventButton); 
        saveButton.setOnClickListener(this); 
        
        cancelButton = (Button) findViewById(R.id.cancelEditEventButton);
        cancelButton.setOnClickListener(this); 

      
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_edit_event_view, menu);
        return true;
    }

	@Override
	public void onClick(View v) {
		if(v == saveButton){
	        //get contents of EditTexts as filled out by user 
	        String name = eventTypeEdit.getText().toString();
	        String description = descriptionEdit.getText().toString();
	        int duration = Integer.parseInt(durationEdit.getText().toString().trim());

	        //get contents of pickers
	        int hour = timepicker.getCurrentHour();
	        int minutes = timepicker.getCurrentMinute();

	        int date = datepicker.getDayOfMonth();
	        int month = datepicker.getMonth();
	        int year = datepicker.getYear();

	        Log.d(tag, "Event ID" + id);
	        Log.d(tag, "Description: " + description);
	        Log.d(tag, "Duration: " + duration);
	        Log.d(tag, "Hour: " + hour);
	        Log.d(tag, "Minutes: " + minutes);
	        Log.d(tag, "Date: " + date);
	        Log.d(tag, "Month: " + month);
	        Log.d(tag, "Year: " + year);

	        String timeString = hour + ":" + minutes;

            try {
                Calendar calendar = Calendar.getInstance();
                calendar.set(year, month, date, hour, minutes);
                Date startDate = calendar.getTime();

                BasicEntityDAO<excellent.models.Event> eventsDAO
                        = HelperFactory.getDatabaseHelper().getBasicEntityDAO(excellent.models.Event.class);
                excellent.models.Event event = new excellent.models.Event();
                event.setId(id);
                event.setUserField(GlobalState.getInstance().getCurrentUser());
                event.setName(name);
                event.setStartTime(startDate);
                event.setEndTime(new Date(startDate.getTime() + duration));
                event.setDescription(description);
                eventsDAO.updateEntity(event);
            } catch (SQLException e) {
                Log.e("DayView", e.getMessage());
            }
	        //create toast to let user know that change was made and event was added
	        Toast.makeText(getBaseContext(), "Your event has been saved", Toast.LENGTH_SHORT).show(); 
			
	        EditEventView.this.finish();//return to previous activity
	        
		}else if(v == cancelButton){
			//cancel - go back to previous screen 
			EditEventView.this.finish();
		}
		
	}//end onClick
}

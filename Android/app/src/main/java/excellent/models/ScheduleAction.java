/*
 * Excellent - Student Organizer
 * Copyright (c ) 2014, Sergey Pilipenko, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */

package excellent.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;

import excellent.models.enums.Priority;
import excellent.utils.ActivityDeserializer;
import excellent.utils.BasicEntitySerializer;
import excellent.utils.LecturerDeserializer;

public class ScheduleAction extends BasicEntity {

    @JsonProperty(value = "lecturerId")
    @JsonSerialize(using = BasicEntitySerializer.class)
    @JsonDeserialize(using = LecturerDeserializer.class)
    @DatabaseField(foreign = true)
    protected Lecturer lecturer;

    @JsonProperty(value = "activityId")
    @JsonSerialize(using = BasicEntitySerializer.class)
    @JsonDeserialize(using = ActivityDeserializer.class)
    @DatabaseField(foreign = true)
    protected Activity activity;

    @DatabaseField()
    protected String name;

    @DatabaseField()
    protected boolean withNotification;

    @DatabaseField()
    protected boolean showOnDisplay;

    @DatabaseField()
    protected String description;

    @DatabaseField(dataType = DataType.ENUM_INTEGER)
    protected Priority priority = Priority.NORMAL;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isWithNotification() {
        return withNotification;
    }

    public void setWithNotification(boolean withNotification) {
        this.withNotification = withNotification;
    }

    public boolean isShowOnDisplay() {
        return showOnDisplay;
    }

    public void setShowOnDisplay(boolean showOnDisplay) {
        this.showOnDisplay = showOnDisplay;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPriority() {
        return priority.getType();
    }

    public void setPriority(int priority) {
        this.priority = Priority.toPriority(priority);
    }

    public Priority getPriorityEnum() {
        return priority;
    }

    public void setPriorityEnum(Priority priority) {
        this.priority = priority;
    }

    public Lecturer getLecturer() {
        return lecturer;
    }

    public void setLecturer(Lecturer lecturer) {
        this.lecturer = lecturer;
    }

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }
}
/*
 * Excellent - Student Organizer
 * Copyright (c ) 2014, Sergey Pilipenko, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */

package excellent.models;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import excellent.models.enums.LecturerDifficulty;

@DatabaseTable(tableName = "lecturers")
public class Lecturer extends BasicEntity {

    @DatabaseField()
    protected String name;
    @DatabaseField()
    protected String position;
    @DatabaseField()
    protected String email;
    @DatabaseField()
    protected String phone;

    @DatabaseField()
    protected String additionalInformation;

    @DatabaseField(dataType = DataType.ENUM_INTEGER)
    protected LecturerDifficulty difficulty = LecturerDifficulty.NORMAL;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAdditionalInformation() {
        return additionalInformation;
    }

    public void setAdditionalInformation(String additionalInformation) {
        this.additionalInformation = additionalInformation;
    }

    public int getDifficulty() {
        return difficulty.getType();
    }

    public void setDifficulty(int difficulty) {
        this.difficulty = LecturerDifficulty.toLecturerDifficulty(difficulty);
    }

    public LecturerDifficulty getDifficultyEnum() {
        return difficulty;
    }

    public void setDifficultyEnum(LecturerDifficulty difficulty) {
        this.difficulty = difficulty;
    }
}
/*
 * Excellent - Student Organizer
 * Copyright (c ) 2014, Sergey Pilipenko, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */

package excellent.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import excellent.models.enums.TaskDifficulty;
import java.util.Date;

@DatabaseTable(tableName = "tasks")
public class Task extends ScheduleAction {
    
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    @DatabaseField(dataType = DataType.DATE)
    protected Date deadline;

    @DatabaseField()
    protected boolean completed;

    @DatabaseField(dataType = DataType.ENUM_INTEGER)
    protected TaskDifficulty difficulty = TaskDifficulty.NORMAL;

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public int getDifficulty() {
        return difficulty.getType();
    }

    public void setDifficulty(int difficulty) {
        this.difficulty = TaskDifficulty.toTaskDifficulty(difficulty);
    }
    
    public TaskDifficulty getDifficultyEnum() {
        return difficulty;
    }

    public void setDifficultyEnum(TaskDifficulty difficulty) {
        this.difficulty = difficulty;
    }
}

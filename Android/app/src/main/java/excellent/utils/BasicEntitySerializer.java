/*
 * Excellent - Student Organizer
 * Copyright (c ) 2014, Sergey Pilipenko, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */

package excellent.utils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.jsontype.TypeSerializer;
import excellent.models.BasicEntity;
import java.io.IOException;

public class BasicEntitySerializer extends JsonSerializer<BasicEntity> {

    @Override
    public void serialize(BasicEntity t, JsonGenerator jg, 
            SerializerProvider sp) throws IOException, JsonProcessingException {
        jg.writeString(t != null ? t.getId() : null);
    }
    
    @Override
    public void serializeWithType(BasicEntity t, JsonGenerator jg, 
            SerializerProvider sp, TypeSerializer tpz) throws IOException, JsonProcessingException {
        jg.writeString(t != null ? t.getId() : null);
    }
}

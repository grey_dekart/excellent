/*
 * Excellent - Student Organizer
 * Copyright (c ) 2014, Sergey Pilipenko, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */

package excellent.tasks.requests;

import com.fasterxml.jackson.annotation.JsonFormat;
import excellent.models.BasicEntity;
import java.io.Serializable;
import java.util.Date;

public class SynchronizationRequest implements Serializable {

    private BasicEntity[] entities;
    
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    private Date lastUpdate;

    public BasicEntity[] getEntities() {
        return entities;
    }

    public void setEntities(BasicEntity[] entities) {
        this.entities = entities;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }
}

/*
 * Excellent - Student Organizer
 * Copyright (c ) 2014, Sergey Pilipenko, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */

package excellent.tasks;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.example.excellent.GlobalState;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;

import excellent.helpers.HelperFactory;
import excellent.helpers.UserDAO;
import excellent.models.User;

public class LogoutTask extends HttpAsyncTask {

    public LogoutTask(Context context) {
        super(context);
    }

    @Override
    protected HttpUriRequest getRequest() throws IOException {
        User user = GlobalState.getInstance().getCurrentUser();
        HttpPost httpPost = new HttpPost(SERVER_URL + "/logout");
        httpPost.setHeader("Accept", "application/json");
        httpPost.setHeader("Content-type", "application/json");
        httpPost.setHeader("Cookie", user.getCredentials());
        return httpPost;
    }

    @Override
    protected void parseResponse(InputStream inputStream) throws IOException {
        try {
            User user = GlobalState.getInstance().getCurrentUser();
            user.setCredentials(null);
            UserDAO userDAO = HelperFactory.getDatabaseHelper().getUserDAO();
            userDAO.update(user);
            Toast.makeText(context, "You've Logged Out", Toast.LENGTH_SHORT).show();
            GlobalState.getInstance().setCurrentUser(null);
        } catch(SQLException ex) {
            Log.e("LogoutTask", ex.getMessage());
        }
    }
}
/*
 * Excellent - Student Organizer
 * Copyright (c ) 2014, Sergey Pilipenko, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */

package excellent.tasks;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.example.excellent.GlobalState;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.http.Header;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;

import excellent.helpers.HelperFactory;
import excellent.helpers.UserDAO;
import excellent.models.User;
import excellent.tasks.requests.LoginRequest;
import excellent.tasks.responses.StatusMessage;
import excellent.tasks.responses.UserDetails;

public class LoginTask extends HttpAsyncTask {

    private String login;
    private String password;

    public LoginTask(Context context, String login, String password) {
        super(context);
        this.login = login;
        this.password = password;
    }

    @Override
    protected HttpUriRequest getRequest() throws IOException {
        LoginRequest request = new LoginRequest();
        request.setLogin(login);
        request.setPassword(password);
        ObjectMapper mapper = new ObjectMapper();
        HttpPost httpPost = new HttpPost(SERVER_URL + "/login");
        httpPost.setEntity(new StringEntity(mapper.writeValueAsString(request)));
        httpPost.setHeader("Accept", "application/json");
        httpPost.setHeader("Content-type", "application/json");
        return httpPost;
    }

    @Override
    protected void parseResponse(InputStream inputStream) throws IOException {
        try {
            ObjectMapper mapper = new ObjectMapper();
            if(response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                UserDetails userDetails = mapper.readValue(inputStream, UserDetails.class);
                UserDAO userDAO = HelperFactory.getDatabaseHelper().getUserDAO();
                User user = userDAO.queryForId(userDetails.getId());
                if(user == null) {
                    user = new User();
                    user.setId(userDetails.getId());
                    user.setLogin(userDetails.getLogin());
                }
                Header[] cookies = response.getHeaders("Set-Cookie");
                StringBuilder credentials = new StringBuilder();
                for(Header cookie: cookies) {
                    credentials.append(cookie.getValue());
                    credentials.append("; ");
                }
                user.setCredentials(credentials.toString());
                userDAO.createOrUpdate(user);
                GlobalState.getInstance().setCurrentUser(user);
            } else {
                StatusMessage message = mapper.readValue(inputStream, StatusMessage.class);
                Toast.makeText(context, message.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } catch(SQLException ex) {
            Log.e("LoginTask", ex.getMessage());
        }
    }
}

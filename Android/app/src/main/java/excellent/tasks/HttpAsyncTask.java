/*
 * Excellent - Student Organizer
 * Copyright (c ) 2014, Sergey Pilipenko, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */

package excellent.tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;
import java.io.InputStream;

public abstract class HttpAsyncTask extends AsyncTask<Void, Void, Void> {

    protected final static String SERVER_URL = "http://178.62.237.79:8080";

    protected Context context;
    protected HttpResponse response;

    protected HttpAsyncTask(Context context) {
        this.context = context;
    }

    private void postURL() {
        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpUriRequest request = getRequest();
            if(request != null) {
                response = httpclient.execute(request);
                InputStream inputStream = response.getEntity().getContent();

                if (inputStream != null) {
                    parseResponse(inputStream);
                }
            }
        } catch (Exception e) {
            Log.e("HttpAsyncTask", e.getLocalizedMessage());
        }
    }

    @Override
    protected Void doInBackground(Void... urls) {
        postURL();
        return null;
    }

    protected abstract HttpUriRequest getRequest() throws IOException;
    protected abstract void parseResponse(InputStream inputStream) throws IOException;
}

/*
 * Excellent - Student Organizer
 * Copyright (c ) 2014, Sergey Pilipenko, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */

package excellent.tasks;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.example.excellent.GlobalState;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import excellent.helpers.BasicEntityDAO;
import excellent.helpers.DatabaseHelper;
import excellent.helpers.HelperFactory;
import excellent.helpers.UserDAO;
import excellent.models.BasicEntity;
import excellent.models.User;
import excellent.tasks.requests.SynchronizationRequest;
import excellent.tasks.responses.StatusMessage;

public class SyncTask extends HttpAsyncTask {

    protected Date syncDate;

    public SyncTask(Context context) {
        super(context);
    }

    @Override
    protected HttpUriRequest getRequest() throws IOException {
        try {
            syncDate = new Date();
            User user = GlobalState.getInstance().getCurrentUser();
            SynchronizationRequest request = new SynchronizationRequest();
            request.setLastUpdate(user.getLastSynchronization());

            DatabaseHelper databaseHelper = HelperFactory.getDatabaseHelper();
            List<BasicEntity> entities = new ArrayList<BasicEntity>();
            for(Class<? extends BasicEntity> clazz: DatabaseHelper.ENTITIES) {
                BasicEntityDAO entityDAO = databaseHelper.getBasicEntityDAO(clazz);
                entities.addAll(entityDAO.getUpdatedUserEntities(user, user.getLastSynchronization()));
            }

            BasicEntity[] entitiesArray = new BasicEntity[entities.size()];
            request.setEntities(entities.toArray(entitiesArray));

            ObjectMapper mapper = new ObjectMapper();
            HttpPost httpPost = new HttpPost(SERVER_URL + "/sync");
            httpPost.setEntity(new StringEntity(mapper.writeValueAsString(request)));
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");
            httpPost.setHeader("Cookie", user.getCredentials());
            return httpPost;
        } catch(SQLException ex) {
            Log.e("SyncTask", ex.getMessage());
        }
        return null;
    }

    @Override
    protected void parseResponse(InputStream inputStream) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        if(response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
            try {
                BasicEntity[] entities = mapper.readValue(inputStream, BasicEntity[].class);
                DatabaseHelper databaseHelper = HelperFactory.getDatabaseHelper();
                for (BasicEntity entity : entities) {
                    BasicEntityDAO entityDAO = databaseHelper.getBasicEntityDAO(entity.getClass());
                    entityDAO.createOrUpdate(entity);
                    /*BasicEntity dbEntity = (BasicEntity)entityDAO.queryForId(entity.getId());
                    if(dbEntity == null) {
                        entityDAO.create(entity);
                    } else {
                        entityDAO.update(entity);
                    }*/
                }
                UserDAO userDAO = databaseHelper.getUserDAO();
                User user = GlobalState.getInstance().getCurrentUser();
                user.setLastSynchronization(syncDate);
                userDAO.update(user);
            } catch(SQLException ex) {
                Log.e("SyncTask", ex.getMessage());
            }
        } else {
            StatusMessage message = mapper.readValue(inputStream, StatusMessage.class);
            Toast.makeText(context, message.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}

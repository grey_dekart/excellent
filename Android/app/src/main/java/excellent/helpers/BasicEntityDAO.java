/*
 * Excellent - Student Organizer
 * Copyright (c ) 2014, Sergey Pilipenko, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */

package excellent.helpers;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import excellent.models.BasicEntity;
import excellent.models.User;

public class BasicEntityDAO<T extends BasicEntity> extends BaseDaoImpl<T, String> {

    protected BasicEntityDAO(ConnectionSource connectionSource,
                      Class<T> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    public int createEntity(T entity) throws SQLException {
        entity.setId(UUID.randomUUID().toString());
        entity.setUpdated(new Date());
        return super.create(entity);
    }

    public int updateEntity(T entity) throws SQLException {
        entity.setUpdated(new Date());
        return super.update(entity);
    }

    public List<T> getUserEntities(User user) throws SQLException {
        Where<T, String> conditions = queryBuilder().where();
        if(user != null) {
            conditions.eq("userId", user);
        } else {
            conditions.isNull("userId");
        }
        conditions.and().eq("deleted", false);
        return conditions.query();
    }

    public List<T> getUpdatedUserEntities(User user, Date update) throws SQLException {
        Where<T, String> conditions = queryBuilder().where();
        if(user != null) {
            conditions.eq("userId", user);
        } else {
            conditions.isNull("userId");
        }
        if(update != null) {
            conditions.and().ge("updated", update);
        }
        return conditions.query();
    }
}

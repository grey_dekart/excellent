/*
 * Excellent - Student Organizer
 * Copyright (c ) 2014, Sergey Pilipenko, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */

package excellent.helpers;

import android.content.Context;

import com.j256.ormlite.android.apptools.OpenHelperManager;

public class HelperFactory{

    private static DatabaseHelper databaseHelper;
    private static HttpHelper httpHelper;

    public static DatabaseHelper getDatabaseHelper(){
        return databaseHelper;
    }
    public static HttpHelper getHttpHelper() {
        return httpHelper;
    }
    public static void initHelpers(Context context){
        databaseHelper = OpenHelperManager.getHelper(context, DatabaseHelper.class);
        httpHelper = new HttpHelper(context);
    }
    public static void releaseHelpers(){
        OpenHelperManager.releaseHelper();
        databaseHelper = null;
        httpHelper = null;
    }
}

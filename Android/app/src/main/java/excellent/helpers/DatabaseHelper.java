/*
 * Excellent - Student Organizer
 * Copyright (c ) 2014, Sergey Pilipenko, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.
 */

package excellent.helpers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

import excellent.models.BasicEntity;
import excellent.models.Event;
import excellent.models.Lecturer;
import excellent.models.Mark;
import excellent.models.Place;
import excellent.models.Subject;
import excellent.models.Task;
import excellent.models.User;

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {
    private static final String TAG = DatabaseHelper.class.getSimpleName();

    private static final String DATABASE_NAME ="excellent.db";

    private static final int DATABASE_VERSION = 1;

    public final static LinkedList<Class<? extends BasicEntity>> ENTITIES
            = new LinkedList<Class<? extends BasicEntity>>();

    static {
        ENTITIES.add(Subject.class);
        ENTITIES.add(Lecturer.class);
        ENTITIES.add(Mark.class);
        ENTITIES.add(Subject.class);
        ENTITIES.add(Lecturer.class);
        ENTITIES.add(Place.class);
        ENTITIES.add(Mark.class);
        ENTITIES.add(Event.class);
        ENTITIES.add(Task.class);
    }

    private UserDAO userDAO;

    private HashMap<Class<? extends BasicEntity>, BasicEntityDAO> entitiesDAO
            = new HashMap<Class<? extends BasicEntity>, BasicEntityDAO>();

    public DatabaseHelper(Context context){
        super(context,DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource){
        try {
            TableUtils.createTable(connectionSource, User.class);
            for(Class<? extends BasicEntity> clazz: ENTITIES) {
                TableUtils.createTable(connectionSource, clazz);
            }
        } catch (SQLException e){
            Log.e(TAG, "error creating DB " + DATABASE_NAME);
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVer,
                          int newVer){
        try {
            Iterator<Class<? extends BasicEntity>> iterator = ENTITIES.descendingIterator();
            while(iterator.hasNext()) {
                TableUtils.dropTable(connectionSource, iterator.next(), true);
            }
            TableUtils.dropTable(connectionSource, User.class, true);
            onCreate(db, connectionSource);
        } catch (SQLException e){
            Log.e(TAG, "error upgrading db " + DATABASE_NAME + "from ver " + oldVer);
            throw new RuntimeException(e);
        }
    }

    public UserDAO getUserDAO()  throws SQLException {
        if(userDAO == null) {
            userDAO = new UserDAO(getConnectionSource(), User.class);
        }
        return userDAO;
    }

    public BasicEntityDAO getBasicEntityDAO(Class<? extends BasicEntity> dataClass) throws SQLException {
        BasicEntityDAO entityDAO = entitiesDAO.get(dataClass);
        if(entityDAO == null) {
            entityDAO = new BasicEntityDAO(getConnectionSource(), dataClass);
            entitiesDAO.put(dataClass, entityDAO);
        }
        return entityDAO;
    }


    @Override
    public void close(){
        super.close();
        userDAO = null;
        entitiesDAO.clear();
    }
}

module.exports = function (grunt) {
	grunt.initConfig({
	    pkg: grunt.file.readJSON('package.json'),
	    concat: {
			options: {
				stripBanners: true
			},
			base: {
				src: [
					'src/js/common.js',
					'src/js/oop.js'
				],
				dest: 'build/src/js/base.js'
			},
            models: {
				src: [
					'src/js/models/evaluationType.js',
					'src/js/models/lecturerDifficulty.js',
					'src/js/models/priority.js',
					'src/js/models/taskDifficulty.js',
					'src/js/models/basicEntity.js',
					'src/js/models/activity.js',
					'src/js/models/subject.js',
					'src/js/models/lecturer.js',
					'src/js/models/scheduleAction.js',
					'src/js/models/event.js',
					'src/js/models/task.js',
					'src/js/models/place.js',
					'src/js/models/mark.js',
					'src/js/models/mapping.js'
				],
				dest: 'build/src/js/models.js'
			},
            dao: {
                src: [
                    'src/js/dao/cascades.js',
					'src/js/dao/fieldType.js',
					'src/js/dao/indexes.js',
					'src/js/dao/dao.js'
                ],
                dest: 'build/src/js/dao.js'
            },
            controllers: {
                src: [
                    'src/js/controllers/locales/en.js',
                    'src/js/controllers/translateModule.js',
                    'src/js/controllers/mainPageModule.js',
                    'src/js/controllers/subjectsModule.js',
                    'src/js/controllers/tasksModule.js',
                    'src/js/controllers/eventsModule.js',
                    'src/js/controllers/calendarModule.js',
                    'src/js/controllers/lecturersModule.js',
                    'src/js/controllers/placesModule.js',
                    'src/js/controllers/marksModule.js',
                    'src/js/controllers/authModule.js',
                    'src/js/controllers/services/historyService.js',
                    'src/js/controllers/services/daoService.js',
                    'src/js/controllers/services/syncService.js',
                    'src/js/controllers/services/map.js',
                    'src/js/controllers/services/notificationsService.js',
                    'src/js/controllers/baseModule.js'
                ],
                dest: 'build/src/js/controllers.js'
            }
        },
        uglify: {
            js: {
                options: {
                    mangle: {
                        except: ['$scope', 'daoService', '$location', '$translate', 'historyNavigator', '$cookies',
                            '$route', '$routeParams', '$filter', '$timeout', '$rootScope', '$controller', '$http',
                            'serverAddress', '$cookieStore', '$interval', '$q', 'syncService', 'pingInterval',
                            'syncInterval', 'notificationService']
                    }
                },
                files: [{
					expand: true,
					src: '**/*.js',
					dest: 'build/js',
					cwd: 'build/src/js',
					ext: '.min.js'
                }]
            }
        },
        cssmin: {
            minify: {
                expand: true,
                cwd: 'src/css/',
                src: ['*.css', '!*.min.css'],
                dest: 'build/css/',
                ext: '.min.css'
            }
        },
        copy: {
            js: {
                expand: true,
                cwd: 'lib/js/',
                src: ['**', '!**/angular/i18n/**'],
                dest: 'build/js/'
            },
            css: {
                expand: true,
                cwd: 'lib/css/',
                src: ['**'],
                dest: 'build/css/'
            },
            html: {
                expand: true,
                cwd: 'src/html/',
                src: ['**'],
                dest: 'build/'
            },
            img_src: {
                expand: true,
                cwd: 'src/img/',
                src: ['**'],
                dest: 'build/img'
            },
            htaccess: {
                src: 'src/.htaccess',
                dest: 'build/.htaccess'
            },
            appcache: {
                src: 'src/excellent.appcache',
                dest: 'build/excellent.appcache'
            },
            icon: {
                src: 'src/favicon.png',
                dest: 'build/favicon.png'
            },
            dist: {
                dot: true,
                expand: true,
                cwd: 'build/',
                src: ['**/*', '!**/src/**'],
                dest: "<%= grunt.config(\"server_root\") + grunt.config(\"project_name\") + \"/\"%>"
            }
        },
        watch: {
            files: ['src/js/**'],
            tasks: ['concat', 'uglify']
        },
        clean: {
            options: { force: true },
            build: ["build"],
            dist: ["<%= grunt.config(\"server_root\") + grunt.config(\"project_name\")%>"]
        },
        qunit: {
            dao: ['tests/dao/*.html']
        }
	});

    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-qunit');
    grunt.registerTask('copy_images', ['copy:img_src', 'copy:icon']);
	grunt.registerTask('default', ['clean:build', 'concat', 'uglify', 'cssmin', 'copy:js', 'copy:css', 'copy:html',
        'copy:htaccess', 'copy:appcache', 'copy_images', 'qunit']);
    grunt.registerTask('set_global', function() {
        grunt.config.set('server_root','D:/Programs/Apache/www/');
        grunt.config.set('project_name','excellent');
    });
    grunt.registerTask('deploy', ['set_global', 'default', 'clean:dist', 'copy:dist']);
};
/**
 * Created by Sergey Pilipenko
 */

module("Dao test");

test("Simple dao test", function() {
    var lecturer = new Lecturer();
    lecturer.id = 1;
    lecturer.lecturerName = 'test';
    lecturer.email = 'test@test.com';
    lecturer.actions = [];

    var dao = new DAO();
    dao.cleanAll();

    var dbLecturer = dao.load('Lecturer', 1);
    equal(dbLecturer, null, 'No data stored');

    dao.store(lecturer);
    dbLecturer = dao.load('Lecturer', 1);
    dao.initializeAllProperties(dbLecturer);
    deepEqual(dbLecturer, lecturer, 'Data saved successfully');

    dao.remove('Lecturer', 1);
    dbLecturer = dao.load('Lecturer', 1);
    equal(dbLecturer, null, 'Data was deleted');
});

test("Cascades", function() {
    var mark = new Mark();
    mark.date = new Date();
    mark.id = '55';
    mark.description = '2';

    var activity = new Activity();
    activity.id = 2;
    activity.activityName = 'OOP';
    activity.actions = [];
    activity.marks = [mark];
    mark.activity = activity;

    var dao = new DAO();
    dao.cleanAll();
    dao.store(activity);

    delete mark.activity;
    mark.activity_id = 2;
    var dbMark = dao.load('Mark', '55');
    dbMark.activity_id = dbMark.activity.id;
    delete dbMark.activity;
    dao.initializeAllProperties(dbMark);
    deepEqual(dbMark, mark, 'Cascade child Save');

    var dbActivity = dao.load('Activity',2);
    activity.marks_ids = ['55'];
    delete activity.marks;

    var ids = [];
    for(var i = 0; i < dbActivity.marks.length; i++) {
        ids.push(dbActivity.marks[i].id);
    }
    dbActivity.marks_ids = ids;
    delete dbActivity.marks;
    dao.initializeAllProperties(dbActivity);
    deepEqual(dbActivity, activity, 'Cascade Load');

    dao.remove(Activity, 2);
    dbMark = dao.load('Mark', '55');
    dao.initializeAllProperties(dbMark);
    equal(dbMark, null, 'Child Delete');
});
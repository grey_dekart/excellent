/*
 * Excellent - Student Organizer
 * Copyright (c) 2014, Sergey Pilipenko, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 */

function Mark() {
    Mark.superclass.constructor.call(this);
    this.activity = null;
    this.description = null;
    this.date = null;
    this.value = null;
}

extendClass(Mark, BasicEntity);

Mark.prototype.serialize = function() {
    var obj = Mark.superclass.serialize.call(this);
    obj['type'] = 'Mark';
    obj['activityId'] = this.activity ? this.activity.id : null;
    obj['description'] = this.description;
    obj['date'] = this.date.toISOString();
    obj['value'] = this.value;
    return obj;
};

Mark.prototype.deserialize = function(data) {
    Mark.superclass.deserialize.call(this, data);
    if(data['activityId']) {
        var activity = new ScheduleAction();
        activity.id = data['activityId'];
        this.activity = activity;
    } else {
        this.activity = null;
    }
    this.description = data['description'];
    this.date = new Date(data['date']);
    this.value = data['value'];
};

Mark.mapping = {
    'entity': 'Mark',
    'table': 'mark',
    'generator': function () {
        this.id = uuid();
    },
    'fields': {
        'id': {
            'primary': true,
            'fieldType': FieldType.Field,
            'field': 'id'
        },
        'userId': {
            'field': 'userId',
            'fieldType': FieldType.Field,
            'index': 'markUserId'
        },
        'updated': {
            'fieldType': FieldType.Date,
            'field': 'updated'
        },
        'deleted': {
            'fieldType' : FieldType.Field,
            'field': 'deleted'
        },
        'activity': {
            'field': 'activityId',
            'entity': 'Activity',
            'fieldType': FieldType.ForeignKey,
            'lazy': true,
            'cascade': Cascade.None,
            'index': 'markActivityId'
        },
        'description': {
            'fieldType': FieldType.Field,
            'field': 'description'
        },
        'value': {
            'fieldType': FieldType.Field,
            'field': 'value'
        },
        'date': {
            'fieldType': FieldType.Date,
            'field': 'date'
        }
    }
};
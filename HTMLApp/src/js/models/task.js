/*
 * Excellent - Student Organizer
 * Copyright (c) 2014, Sergey Pilipenko, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 */

function Task() {
    Task.superclass.constructor.call(this);
    this.deadline = null;
    this.completed = false;
    this.difficulty = TaskDifficulty.Normal;
}

extendClass(Task, ScheduleAction);

Task.prototype.serialize = function() {
    var obj = Task.superclass.serialize.call(this);
    obj['type'] = 'Task';
    obj['deadline'] = this.deadline.toISOString();
    obj['completed'] = this.completed;
    obj['difficulty'] = this.difficulty;
    return obj;
};

Task.prototype.deserialize = function(data) {
    Task.superclass.deserialize.call(this, data);
    this.deadline = new Date(data['deadline']);
    this.completed = data['completed'];
    this.difficulty = data['difficulty'];
};

Task.mapping = clone(ScheduleAction.mapping);

extendObject(Task.mapping,
    {
        'entity' : 'Task',
        'fields' : {
            'deadline': {
                'fieldType' : FieldType.Date,
                'field': 'deadline'
            },
            'completed': {
                'fieldType' : FieldType.Field,
                'field': 'completed'
            },
            'difficulty': {
                'fieldType' : FieldType.Field,
                'field': 'difficulty'
            }
        }
    },
    true);
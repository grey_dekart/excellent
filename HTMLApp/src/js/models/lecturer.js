/*
 * Excellent - Student Organizer
 * Copyright (c) 2014, Sergey Pilipenko, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 */

function Lecturer() {
    Lecturer.superclass.constructor.call(this);
    this.lecturerName = null;
    this.position = null;
    this.email = null;
    this.phone = null;
    this.difficulty = LecturerDifficulty.Normal;
    this.additionalInfo = null;
    this.actions = null;
}

extendClass(Lecturer, BasicEntity);

Lecturer.prototype.serialize = function() {
    var obj = Lecturer.superclass.serialize.call(this);
    obj['type'] = 'Lecturer';
    obj['name'] = this.lecturerName;
    obj['position'] = this.position;
    obj['email'] = this.email;
    obj['phone'] = this.phone;
    obj['difficulty'] = this.difficulty;
    obj['additionalInfo'] = this.additionalInfo;
    return obj;
};

Lecturer.prototype.deserialize = function(data) {
    Lecturer.superclass.deserialize.call(this, data);
    this.lecturerName = data['name'];
    this.position = data['position'];
    this.email = data['email'];
    this.phone = data['phone'];
    this.difficulty = data['difficulty'];
    this.additionalInfo = data['additionalInfo'];
    this[DAO.UNINITIALIZED_PREFIX + 'actions'] = true;
};

Lecturer.mapping = {
    'entity': 'Lecturer',
    'table': 'lecturer',
    'generator': function () {
        this.id = uuid();
    },
    'fields': {
        'id': {
            'primary': true,
            'fieldType': FieldType.Field,
            'field': 'id'
        },
        'userId': {
            'field': 'userId',
            'fieldType': FieldType.Field,
            'index': 'lecturerUserId'
        },
        'updated': {
            'fieldType': FieldType.Date,
            'field': 'updated'
        },
        'deleted': {
            'fieldType' : FieldType.Field,
            'field': 'deleted'
        },
        'lecturerName': {
            'fieldType': FieldType.Field,
            'field': 'name'
        },
        'position': {
            'fieldType': FieldType.Field,
            'field': 'position'
        },
        'email': {
            'fieldType': FieldType.Field,
            'field': 'email'
        },
        'phone': {
            'fieldType': FieldType.Field,
            'field': 'phone'
        },
        'difficulty': {
            'fieldType': FieldType.Field,
            'field': 'difficulty'
        },
        'additionalInfo': {
            'fieldType': FieldType.Field,
            'field': 'additionalInfo'
        }
    },
    collections : {
        'actions': {
            'entity' : 'ScheduleAction',
            'key': 'lecturer',
            'lazy': true,
            'cascade' : Cascade.Set_Null
        }
    }
};
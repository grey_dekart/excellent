/*
 * Excellent - Student Organizer
 * Copyright (c) 2014, Sergey Pilipenko, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 */

function Subject() {
    Subject.superclass.constructor.call(this);
    this.evaluationType = EvaluationType.None;
}

extendClass(Subject, Activity);

Subject.prototype.serialize = function() {
    var obj = Subject.superclass.serialize.call(this);
    obj['type'] = 'Subject';
    obj['evaluationType'] = this.evaluationType;
    return obj;
};

Subject.prototype.deserialize = function(data) {
    Subject.superclass.deserialize.call(this, data);
    this.evaluationType = data['evaluationType'];
};

Subject.mapping = clone(Activity.mapping);

extendObject(Subject.mapping,
    {
        'entity' : 'Subject',
        'fields' : {
            'evaluationType': {
                'fieldType' : FieldType.Field,
                'field': 'evaluationType'
            }
        }
    },
    true);
/*
 * Excellent - Student Organizer
 * Copyright (c) 2014, Sergey Pilipenko, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 */

function BasicEntity() {
    this.id = null;
    this.userId = null;
    this.updated = null;
    this.deleted = false;
}

BasicEntity.prototype.serialize = function() {
    return {
        id: this.id,
        userId: this.userId,
        updated: this.updated.toISOString(),
        deleted: this.deleted
    };
};

BasicEntity.prototype.deserialize = function(data) {
    this.id = data.id;
    this.userId = data.userId;
    this.updated = new Date(data.updated);
    this.deleted = data.deleted;
};
/*
 * Excellent - Student Organizer
 * Copyright (c) 2014, Sergey Pilipenko, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 */

var MAPPING = {
    'Activity' : Activity,
    'Subject' : Subject,
    'ScheduleAction' : ScheduleAction,
    'ScheduleEvent' : ScheduleEvent,
    'Task' : Task,
    'Lecturer' : Lecturer,
    'Mark' : Mark,
    'Place' : Place
};

if(typeof Object.freeze == 'function') {
    Object.freeze(MAPPING);
}

var DESERIALIZE_TABLE = {
    'Activity' : Activity,
    'Subject' : Subject,
    'ScheduleAction' : ScheduleAction,
    'Event' : ScheduleEvent,
    'Task' : Task,
    'Lecturer' : Lecturer,
    'Mark' : Mark,
    'Place' : Place
};

if(typeof Object.freeze == 'function') {
    Object.freeze(DESERIALIZE_TABLE);
}
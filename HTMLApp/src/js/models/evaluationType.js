/*
 * Excellent - Student Organizer
 * Copyright (c) 2014, Sergey Pilipenko, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 */

var EvaluationType = {'None':0,'Exam':1,'Test':2};

if(typeof Object.freeze == 'function') {
    Object.freeze(EvaluationType);
}

function toEvaluationType(value) {
    var result =  EvaluationType.None;
    try {
        var val = parseInt(value);
        switch(val) {
            case EvaluationType.None:
                result = EvaluationType.None;
                break;
            case EvaluationType.Exam:
                result = EvaluationType.Exam;
                break;
            case EvaluationType.Test:
                result = EvaluationType.Test;
                break;
        }
    } catch(e) {}
    return result;
}
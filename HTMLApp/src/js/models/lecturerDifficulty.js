/*
 * Excellent - Student Organizer
 * Copyright (c) 2014, Sergey Pilipenko, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 */

var LecturerDifficulty = {'Easy':0,'Normal':1,'Hard':2};

if(typeof Object.freeze == 'function') {
    Object.freeze(LecturerDifficulty);
}

function toLecturerDifficulty(value) {
    var result =  LecturerDifficulty.Normal;
    try {
        var val = parseInt(value);
        switch(val) {
            case LecturerDifficulty.Easy:
                result = LecturerDifficulty.Easy;
                break;
            case LecturerDifficulty.Normal:
                result = TaskDifficulty.Normal;
                break;
            case LecturerDifficulty.Hard:
                result = LecturerDifficulty.Hard;
                break;
        }
    } catch(e) {}
    return result;
}
/*
 * Excellent - Student Organizer
 * Copyright (c) 2014, Sergey Pilipenko, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 */

function ScheduleEvent() {
    ScheduleEvent.superclass.constructor.call(this);
    this.place = null;
    this.startTime = null;
    this.endTime = null;
}

extendClass(ScheduleEvent, ScheduleAction);

ScheduleEvent.prototype.serialize = function() {
    var obj = ScheduleEvent.superclass.serialize.call(this);
    obj['type'] = 'Event';
    obj['placeId'] = this.place ? this.place.id : null;
    obj['startTime'] = this.startTime.toISOString();
    obj['endTime'] = this.endTime.toISOString();
    return obj;
};

ScheduleEvent.prototype.deserialize = function(data) {
    ScheduleEvent.superclass.deserialize.call(this, data);
    if(data['placeId']) {
        var place = new Place();
        place.id = data['placeId'];
        this.place = place;
    } else {
        this.place = null;
    }
    this.startTime = new Date(data['startTime']);
    this.endTime = new Date(data['endTime']);
};

ScheduleEvent.mapping = clone(ScheduleAction.mapping);

extendObject(ScheduleEvent.mapping,
    {
        'entity' : 'ScheduleEvent',
        'fields' : {
            'startTime': {
                'fieldType' : FieldType.Date,
                'field': 'startTime'
            },
            'endTime': {
                'fieldType' : FieldType.Date,
                'field': 'endTime'
            },
            'place': {
                'field': 'placeId',
                'entity': 'Place',
                'fieldType': FieldType.ForeignKey,
                'lazy': true,
                'cascade': Cascade.None,
                'index': 'eventPlaceId'
            }
        }
    },
    true);
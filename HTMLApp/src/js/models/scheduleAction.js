/*
 * Excellent - Student Organizer
 * Copyright (c) 2014, Sergey Pilipenko, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 */

function ScheduleAction() {
    ScheduleAction.superclass.constructor.call(this);
    this.lecturer = null;
    this.activity = null;
    this.actionName = null;
    this.description = null;
    this.withNotification = false;
    this.showOnDisplay = false;
    this.priority = Priority.Normal;
}

extendClass(ScheduleAction, BasicEntity);

ScheduleAction.prototype.serialize = function() {
    var obj = ScheduleAction.superclass.serialize.call(this);
    obj['type'] = 'ScheduleAction';
    obj['lecturerId'] = this.lecturer ? this.lecturer.id : null;
    obj['activityId'] = this.activity ? this.activity.id : null;
    obj['name'] = this.actionName;
    obj['description'] = this.description;
    obj['withNotification'] = this.withNotification;
    obj['showOnDisplay'] = this.showOnDisplay;
    obj['priority'] = this.priority;
    return obj;
};

ScheduleAction.prototype.deserialize = function(data) {
    ScheduleAction.superclass.deserialize.call(this, data);
    if(data['lecturerId']) {
        var lecturer = new Lecturer();
        lecturer.id = data['lecturerId'];
        this.lecturer = lecturer;
    } else {
        this.lecturer = null;
    }
    if(data['activityId']) {
        var activity = new ScheduleAction();
        activity.id = data['activityId'];
        this.activity = activity;
    } else {
        this.activity = null;
    }
    this.actionName = data['name'];
    this.description = data['description'];
    this.withNotification = data['withNotification'];
    this.showOnDisplay = data['showOnDisplay'];
    this.priority = data['priority'];
};

ScheduleAction.mapping = {
    'table' : 'actions',
    'entity' : 'ScheduleAction',
    'generator' : function() { this.id = uuid();},
    'fields' : {
        'id': {
            'primary': true,
            'fieldType': FieldType.Field,
            'field': 'id'
        },
        'userId': {
            'field': 'userId',
            'fieldType': FieldType.Field,
            'index': 'actionUserId'
        },
        'updated': {
            'fieldType': FieldType.Date,
            'field': 'updated'
        },
        'deleted': {
            'fieldType' : FieldType.Field,
            'field': 'deleted'
        },
        'lecturer': {
            'field': 'lecturerId',
            'entity': 'Lecturer',
            'fieldType': FieldType.ForeignKey,
            'lazy': true,
            'cascade': Cascade.None,
            'index': 'actionLecturerId'
        },
        'activity': {
            'field': 'activityId',
            'entity': 'Activity',
            'fieldType': FieldType.ForeignKey,
            'lazy': true,
            'cascade': Cascade.None,
            'index': 'actionActivityId'
        },
        'actionName': {
            'fieldType' : FieldType.Field,
            'field': 'name'
        },
        'description': {
            'fieldType' : FieldType.Field,
            'field': 'description'
        },
        'withNotification': {
            'fieldType' : FieldType.Field,
            'field': 'withNotification'
        },
        'showOnDisplay': {
            'fieldType' : FieldType.Field,
            'field': 'showOnDisplay'
        },
        'priority': {
            'fieldType' : FieldType.Field,
            'field': 'priority'
        }
    }
};
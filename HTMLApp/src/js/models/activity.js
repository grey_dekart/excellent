/*
 * Excellent - Student Organizer
 * Copyright (c) 2014, Sergey Pilipenko, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 */

function Activity() {
    Activity.superclass.constructor.call(this);
    this.activityName = null;
    this.description = null;
    this.marks = null;
    this.actions = null;
}

extendClass(Activity, BasicEntity);

Activity.prototype.serialize = function() {
    var obj = Activity.superclass.serialize.call(this);
    obj['type'] = 'Activity';
    obj['name'] = this.activityName;
    obj['description'] = this.description;
    return obj;
};

Activity.prototype.deserialize = function(data) {
    Activity.superclass.deserialize.call(this, data);
    this.activityName = data['name'];
    this.description = data['description'];
    this[DAO.UNINITIALIZED_PREFIX + 'marks'] = true;
    this[DAO.UNINITIALIZED_PREFIX + 'actions'] = true;
};

Activity.mapping = {
    'entity' : 'Activity',
    'table' : 'activity',
    'generator' : function() { this.id = uuid();},
    'fields' : {
        'id': {
            'primary': true,
            'fieldType' : FieldType.Field,
            'field': 'id'
        },
        'userId': {
            'field': 'userId',
            'fieldType': FieldType.Field,
            'index': 'activityUserId'
        },
        'updated': {
            'fieldType' : FieldType.Date,
            'field': 'updated'
        },
        'deleted': {
            'fieldType' : FieldType.Field,
            'field': 'deleted'
        },
        'activityName': {
            'fieldType' : FieldType.Field,
            'field': 'name'
        },
        'description': {
            'fieldType' : FieldType.Field,
            'field': 'description'
        }
    },
    collections : {
        'marks': {
            'entity' : 'Mark',
            'key': 'activity',
            'lazy': true,
            'cascade' : Cascade.All
        },
        'actions': {
            'entity' : 'ScheduleAction',
            'key': 'activity',
            'lazy': true,
            'cascade' : Cascade.All
        }
    }
};
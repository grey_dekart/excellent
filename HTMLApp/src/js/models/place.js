/*
 * Excellent - Student Organizer
 * Copyright (c) 2014, Sergey Pilipenko, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 */

function Place() {
    Place.superclass.constructor.call(this);
    this.placeName = null;
    this.address = null;
    this.latitude = null;
    this.longitude = null;
    this.events = null;
}

extendClass(Place, BasicEntity);

Place.prototype.serialize = function() {
    var obj = Place.superclass.serialize.call(this);
    obj['type'] = 'Place';
    obj['name'] = this.placeName;
    obj['address'] = this.address;
    obj['latitude'] = this.latitude;
    obj['longitude'] = this.longitude;
    return obj;
};

Place.prototype.deserialize = function(data) {
    Place.superclass.deserialize.call(this, data);
    this.placeName = data['name'];
    this.address = data['address'];
    this.latitude = data['latitude'];
    this.longitude = data['longitude'];
    this[DAO.UNINITIALIZED_PREFIX + 'events'] = true;
};

Place.mapping = {
    'entity' : 'Place',
    'table' : 'place',
    'generator' : function() { this.id = uuid();},
    'fields' : {
        'id': {
            'primary': true,
            'fieldType' : FieldType.Field,
            'field': 'id'
        },
        'userId': {
            'field': 'userId',
            'fieldType' : FieldType.Field,
            'index': 'placeUserId'
        },
        'updated': {
            'fieldType' : FieldType.Date,
            'field': 'updated'
        },
        'deleted': {
            'fieldType' : FieldType.Field,
            'field': 'deleted'
        },
        'placeName': {
            'fieldType' : FieldType.Field,
            'field': 'name'
        },
        'address': {
            'fieldType' : FieldType.Field,
            'field': 'address'
        },
        'latitude': {
            'fieldType' : FieldType.Field,
            'field': 'latitude'
        },
        'longitude': {
            'fieldType' : FieldType.Field,
            'field': 'longitude'
        }
    },
    collections : {
        'events': {
            'entity' : 'ScheduleEvent',
            'key': 'place',
            'lazy': true,
            'cascade' : Cascade.Set_Null
        }
    }
};
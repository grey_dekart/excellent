/*
 * Excellent - Student Organizer
 * Copyright (c) 2014, Sergey Pilipenko, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 */

function extendObject(target, source, rewrite) {
    for(var i in source) {
        if (source.hasOwnProperty(i)) {
            var field = target[i];
            if(field === undefined) {
                target[i] = source[i];
            } else {
                if(typeof source[i] == "object" && typeof field == "object") {
                    extendObject(target[i], source[i], rewrite);
                } else if(rewrite) {
                    target[i] = source[i];
                }
            }
        }
    }
};

function instanceOfClass(object, constructor) {
    var o=object;

    while (o.__proto__ != null) {
        if (o.__proto__ === constructor.prototype) {
            return true;
        }
        o = o.__proto__
    }
    return false
}

function extensionOf(classObj, constructor) {
    var o=classObj.prototype;

    while (o) {
        if (o === constructor.prototype) {
            return true;
        }
        o = o.__proto__
    }
    return false
}

function extendClass(Child, Parent) {
    var F = function() { };
    F.prototype = Parent.prototype;
    Child.prototype = new F();
    Child.prototype.constructor = Child;
    Child.superclass = Parent.prototype;
}

function clone(obj){
    if(null == obj || typeof obj != "object") {
        return obj;
    }

    var copy;
    if (obj instanceof Date) {
        copy = new Date();
        copy.setTime(obj.getTime());
        return copy;
    }

    if (obj instanceof Array) {
        copy = [];
        for (var i = 0, len = obj.length; i < len; i++) {
            copy[i] = clone(obj[i]);
        }
        return copy;
    }

    if (obj instanceof Object) {
        var F = function(){};
        F.prototype = obj.constructor;
        copy = new F();
        for (var attr in obj) {
            if (obj.hasOwnProperty(attr)) {
                copy[attr] = clone(obj[attr]);
            }
        }
        return copy;
    }
    throw new Error("Unable to copy obj! Its type isn't supported.");
}
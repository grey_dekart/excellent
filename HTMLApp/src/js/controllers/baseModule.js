/*
 * Excellent - Student Organizer
 * Copyright (c) 2014, Sergey Pilipenko, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 */

var INTEGER_REGEXP = /^\-?\d+$/;
var EMAIL_PATTERN = /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/;
var PHONE_PATTERN = /^\+?[0-9]{3}-?[0-9]{6,12}$/;

function validateDate(dateValue, $filter, format) {
    var validDate = true;
    try {
        var dateObj = new Date(dateValue);
        var textDate = $filter('date')(dateObj, format);
        if (textDate != dateValue) {
            validDate = false;
        }
    } catch (e) {
        validDate = false;
    }
    return validDate;
}

function initTimeout($scope, field, $timeout, delay) {
    $scope[field] = new Date();
    var updateTime = function() {
        $scope[field] = new Date();
        $timeout(updateTime, delay);
    };

    $timeout(updateTime, delay - $scope[field].getTime() % delay);
}

var applicationModule = angular.module('excellentModule', ['ngRoute','TranslateApp', 'MainPageApp', 'SubjectsApp',
        'TasksApp', 'EventsApp', 'CalendarApp', 'LecturersApp', 'PlacesApp', 'MarksApp', 'AuthApp', 'ngCookies'])
    .service('daoService', function($rootScope) {return new DaoService($rootScope);})
    .service('historyNavigator', HistoryService)
    .constant('serverAddress', 'http://localhost:8080')
    .constant('pingInterval', 10000)
    .constant('syncInterval', 30000)
    .service('syncService', function(serverAddress, $rootScope, $cookies, daoService, $http, $route) {
        return new SyncService(serverAddress, $rootScope, $cookies, daoService, $http, $route);
    })
    .service('notificationService', NotificationService)
    .factory('requestInterceptor', function($q, $rootScope, $location) {
        return {
            'responseError': function(response) {
                if(response.status == 401 && $rootScope.user) {
                    $rootScope.user = null;
                    $location.path("/");
                }
                return $q.reject(response);
            }
        };
    })
    .directive('phoneField', function() {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function($scope, elm, attrs, ctrl) {
                ctrl.$parsers.unshift(function(viewValue) {
                    var validPhone = viewValue == "" || PHONE_PATTERN.test(viewValue);
                    ctrl.$setValidity('invalid', validPhone);
                    return validPhone ? viewValue : undefined;
                });
            }
        };
    })
    .directive('emailField', function() {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function($scope, elm, attrs, ctrl) {
                ctrl.$parsers.unshift(function(viewValue) {
                    var validEmail = viewValue == "" || EMAIL_PATTERN.test(viewValue);
                    ctrl.$setValidity('invalid', validEmail);
                    return validEmail ? viewValue : undefined;
                });
            }
        };
    })
    .directive('dateField',['$filter', function($filter) {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function($scope, elm, attrs, ctrl) {
                var format = attrs.dateField || 'yyyy/MM/dd HH:mm';
                ctrl.$parsers.unshift(function(viewValue) {
                    var validDate = true;
                    if (viewValue != "") {
                        validDate = validateDate(viewValue, $filter, format);
                    }
                    ctrl.$setValidity('invalid', validDate);
                    return validDate ? viewValue :  undefined;
                });
            }
        };
    }])
    .directive('integer', function() {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function(scope, elm, attrs, ctrl) {
                ctrl.$parsers.unshift(function(viewValue) {
                    var valid = viewValue == "" || INTEGER_REGEXP.test(viewValue);
                    ctrl.$setValidity('integer', valid);
                    return valid ? viewValue : undefined;
                });
            }
        };
    })
    .config(['$routeProvider', '$locationProvider', '$httpProvider',
        function($routeProvider, $locationProvider, $httpProvider) {
            $routeProvider
                .when('/', {
                    controller:'MainCtrl',
                    templateUrl:'main.html'
                })
                .when('/index.html', {
                    controller:'MainCtrl',
                    templateUrl:'main.html'
                })
                .when('/subjects', {
                    controller:'ListSubjects',
                    templateUrl:'subjects/subjects.html'
                })
                .when('/createSubject', {
                    controller:'CreateSubject',
                    templateUrl:'subjects/subjectForm.html'
                })
                .when('/editSubject/:subjectId', {
                    controller:'EditSubject',
                    templateUrl:'subjects/subjectForm.html'
                })
                .when('/subject/:subjectId', {
                    controller:'ViewSubject',
                    templateUrl:'subjects/subjectView.html'
                })
                .when('/tasks', {
                    controller:'ListAllTasks',
                    templateUrl:'tasks/tasks.html'
                })
                .when('/createTask', {
                    controller:'CreateTask',
                    templateUrl:'tasks/taskForm.html'
                })
                .when('/editTask/:taskId', {
                    controller:'EditTask',
                    templateUrl:'tasks/taskForm.html'
                })
                .when('/task/:taskId', {
                    controller:'ViewTask',
                    templateUrl:'tasks/taskView.html'
                })
                .when('/events', {
                    controller:'ListAllEvents',
                    templateUrl:'events/events.html'
                })
                .when('/createEvent', {
                    controller:'CreateEvent',
                    templateUrl:'events/eventForm.html'
                })
                .when('/editEvent/:eventId', {
                    controller:'EditEvent',
                    templateUrl:'events/eventForm.html'
                })
                .when('/repeatEvent/:eventId', {
                    controller:'RepeatEvent',
                    templateUrl:'events/repeatEventForm.html'
                })
                .when('/event/:eventId', {
                    controller:'ViewEvent',
                    templateUrl:'events/eventView.html'
                })
                .when('/calendar', {
                    controller:'Calendar',
                    templateUrl:'calendar/calendar.html'
                })
                .when('/daySchedule/:scheduleDate', {
                    controller:'DayScheduler',
                    templateUrl:'calendar/schedule.html'
                })
                .when('/scheduleMap/:scheduleDate', {
                    controller:'MapScheduler',
                    templateUrl:'calendar/scheduleMap.html'
                })
                .when('/lecturers', {
                    controller:'ListLecturers',
                    templateUrl:'lecturers/lecturers.html'
                })
                .when('/createLecturer', {
                    controller:'CreateLecturer',
                    templateUrl:'lecturers/lecturerForm.html'
                })
                .when('/editLecturer/:lecturerId', {
                    controller:'EditLecturer',
                    templateUrl:'lecturers/lecturerForm.html'
                })
                .when('/lecturer/:lecturerId', {
                    controller:'ViewLecturer',
                    templateUrl:'lecturers/lecturerView.html'
                })
                .when('/places', {
                    controller:'ListPlaces',
                    templateUrl:'places/places.html'
                })
                .when('/createPlace', {
                    controller:'CreatePlace',
                    templateUrl:'places/placeForm.html'
                })
                .when('/editPlace/:placeId', {
                    controller:'EditPlace',
                    templateUrl:'places/placeForm.html'
                })
                .when('/place/:placeId', {
                    controller:'ViewPlace',
                    templateUrl:'places/placeView.html'
                })
                .when('/createMark', {
                    controller:'CreateMark',
                    templateUrl:'marks/markForm.html'
                })
                .when('/editMark/:markId', {
                    controller:'EditMark',
                    templateUrl:'marks/markForm.html'
                })
                .when('/mark/:markId', {
                    controller:'ViewMark',
                    templateUrl:'marks/markView.html'
                })
                .when('/register', {
                    controller:'RegisterForm',
                    templateUrl:'auth/registerForm.html'
                })
                .when('/login', {
                    controller:'LoginForm',
                    templateUrl:'auth/loginForm.html'
                })
                .when('/logout', {
                    controller:'LogoutController',
                    template: ' '
                })
                .otherwise({
                    redirectTo:'/'
                });
            $locationProvider.html5Mode(true);
            $httpProvider.interceptors.push('requestInterceptor');
        }
    ])
    .run(function($rootScope, $cookies, $http, $interval, historyNavigator, serverAddress, daoService, $route,
                  syncService, pingInterval, syncInterval, notificationService){
        var ping = function() {
            $http({
                url: serverAddress + "/ping",
                method: "GET",
                withCredentials: true,
                data: ""
            }).success(function() { $rootScope.online = true; })
            .error(function() { $rootScope.online = false; });
        };

        ping();
        $http({
            url: serverAddress + "/auth",
            dataType: "json",
            method: "GET",
            withCredentials: true
        }).success(function(data, status, headers, config) {
            $rootScope.user = data;

        });

        $rootScope.synchronize = syncService.synchronize;

        $interval(ping, pingInterval);
        $interval(syncService.synchronize, syncInterval);
        $interval(function() {notificationService.updateNotifications($rootScope, $cookies, daoService);}, 5000);

        $rootScope.$watch('user', function () {
            $rootScope.tasksNotifications = [];
            $rootScope.eventsNotifications = [];
            syncService.initUnassociatedItemsUtils();
            syncService.synchronize();
        });

        $rootScope.$on('$routeChangeSuccess', function (e, current, pre) {
            $rootScope.withoutReload = false;
            if(pre) {
                var fullRoute = pre.$$route.originalPath;
                var routeParams = pre.params;
                var resolvedUrl = fullRoute;
                for(var param in routeParams) {
                    if(routeParams.hasOwnProperty(param)) {
                        resolvedUrl = resolvedUrl.replace(':' + param, routeParams[param]);
                    }
                }
                historyNavigator.onUrlChanged(resolvedUrl, routeParams);
            }
        });
    });
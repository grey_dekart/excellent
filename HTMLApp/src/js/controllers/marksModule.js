/*
 * Excellent - Student Organizer
 * Copyright (c) 2014, Sergey Pilipenko, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 */

angular.module('MarksApp', [])
    .controller('BaseListMarks', function($scope, daoService, $route, $translate) {
        $scope.markSort = "date";
        $scope.markReverse = true;

        $scope.changeMarkSort = function(value){
            if ($scope.markSort == value){
                $scope.markReverse = !$scope.markReverse;
                return;
            }

            $scope.markSort = value;
            $scope.markReverse = false;
        };

        $scope.deleteMark = function(markId) {
            $translate('CONFIRM_DELETE_MARK')
                .then(function (message) {
                    if (confirm(message)) {
                        daoService.remove('Mark', markId);
                        $route.reload();
                    }
                });
        };
    })
    .controller('markFormController', function($scope, $location, daoService, historyNavigator, $rootScope) {
        $rootScope.withoutReload = true;
        $scope.save = function() {
            $scope.mark.date = new Date($scope.date);
            $scope.mark.activity = $scope.activity;
            daoService.store($scope.mark);
            $location.path(historyNavigator.prevUrl() || '/subjects');
        };
        $scope.cancel = function() {
            $location.path(historyNavigator.prevUrl() || '/subjects');
        }
    })
    .controller('CreateMark', function($scope, $location, $filter, daoService, historyNavigator, $controller, $rootScope) {
        $controller('markFormController', {
            $scope: $scope,
            $location: $location,
            daoService: daoService,
            historyNavigator: historyNavigator,
            $rootScope: $rootScope
        });

        var prevParams = historyNavigator.prevParams();

        if(!prevParams.subjectId) {
            $location.path(historyNavigator.prevUrl() || '/subjects');
            return;
        }

        $scope.activity = daoService.load('Activity', prevParams.subjectId);
        if(!$scope.activity) {
            $location.path(historyNavigator.prevUrl() || '/subjects');
            return;
        }

        $scope.newItem = true;
        $scope.mark = new Mark();

        var initTimeValue = new Date();
        $scope.date = $filter('date')(initTimeValue, 'yyyy/MM/dd HH:mm')
    })
    .controller('EditMark', function($scope, daoService, $translate, $filter, $location, $routeParams, historyNavigator,
            $controller, $rootScope) {
        var markId = $routeParams.markId;
        $scope.newItem = false;
        $scope.mark = daoService.load('Mark', markId);
        if(!$scope.mark) {
            $location.path(historyNavigator.prevUrl() || '/subjects');
            return;
        }

        $controller('markFormController', {
            $scope: $scope,
            $location: $location,
            daoService: daoService,
            historyNavigator: historyNavigator,
            $rootScope: $rootScope
        });

        $scope.activity = $scope.mark.activity;
        $scope.date = $filter('date')($scope.mark.date, 'yyyy/MM/dd HH:mm');
        $scope.delete = function() {
            $translate('CONFIRM_DELETE_MARK')
                .then(function (message) {
                    if (confirm(message)) {
                        daoService.remove('Mark', markId);
                        $location.path(historyNavigator.prevUrl() || '/subjects');
                    }
                });
        };
    })
    .controller('ViewMark', function($scope, daoService, $translate, $location, $routeParams, historyNavigator) {
        var markId = $routeParams.markId;
        $scope.mark = daoService.load('Mark', markId);
        if(!$scope.mark) {
            $location.path(historyNavigator.prevUrl() || '/subjects');
            return;
        }

        $scope.back = function() {
            $location.path(historyNavigator.prevUrl() || '/subjects');
        };

        $scope.delete = function() {
            $translate('CONFIRM_DELETE_MARK')
                .then(function (message) {
                    if (confirm(message)) {
                        daoService.remove('Mark', markId);
                        $location.path(historyNavigator.prevUrl() || '/subjects');
                    }
                });
        };
    });
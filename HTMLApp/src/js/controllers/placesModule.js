/*
 * Excellent - Student Organizer
 * Copyright (c) 2014, Sergey Pilipenko, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 */

angular.module('PlacesApp', [])
    .controller('ListPlaces', function($scope, daoService, $route, $translate) {
        $scope.places = daoService.all('Place');
        $scope.deletePlace = function(placeId) {
            $translate('CONFIRM_DELETE_PLACE')
                .then(function (message) {
                    if (confirm(message)) {
                        daoService.remove('Place', placeId);
                        $route.reload();
                    }
                });
        };

        $scope.sort = "placeName";
        $scope.reverse = false;

        $scope.changeSort = function(value){
            if ($scope.sort == value){
                $scope.reverse = !$scope.reverse;
                return;
            }

            $scope.sort = value;
            $scope.reverse = false;
        };
    })
    .controller('placeFormController', function($scope, $location, daoService, historyNavigator, $rootScope) {
        $rootScope.withoutReload = true;
        $scope.$watchCollection('[place.latitude, place.longitude]', function (newValues, oldValues, scope){
            var valid = newValues[0] == null ^ newValues[1] == null;
            scope.placeForm.$setValidity('emptyCoordinates', !valid);
        });

        $scope.mapMarker = null;
        $scope.map = new Map('placeMap');

        $scope.$on('$viewContentLoaded', function() {
            $scope.map.initMap($scope.place.latitude, $scope.place.longitude);
            Map.getCoordinates($scope.place.latitude, $scope.place.longitude, $scope.place.address,
                function(lat, lng) {
                    $scope.mapMarker = $scope.map.addMarker(lat, lng);
                    $scope.map.moveMap(lat, lng);
                });
            $scope.map.addListener('rightclick', function(event) {
                var lat = event.latLng.lat().toFixed(6);
                var lng = event.latLng.lng().toFixed(6);
                if($scope.mapMarker) {
                    $scope.map.moveMarker($scope.mapMarker, lat, lng);
                } else {
                    $scope.mapMarker = $scope.map.addMarker(lat, lng);
                }
                $scope.map.moveMap(lat, lng);
                $scope.place.latitude = parseFloat(lat);
                $scope.place.longitude = parseFloat(lng);
                $scope.placeForm.address.$pristine = true;
                $scope.placeForm.latitude.$pristine = true;
                $scope.placeForm.longitude.$pristine = true;
                $scope.$apply();
            });
        });

        $scope.updateLocation = function() {
            Map.getCoordinates($scope.place.latitude, $scope.place.longitude, null,
                function(lat, lng) {
                    if($scope.mapMarker) {
                        $scope.map.moveMarker($scope.mapMarker, lat, lng);
                    } else {
                        $scope.mapMarker = $scope.map.addMarker(lat, lng);
                    }
                    $scope.placeForm.address.$pristine = true;
                    $scope.placeForm.latitude.$pristine = true;
                    $scope.placeForm.longitude.$pristine = true;
                    $scope.map.moveMap(lat, lng);
                });
        };

        $scope.getLocationByAddress = function() {
            Map.getCoordinates(null, null, $scope.place.address,
                function(lat, lng) {
                    if($scope.mapMarker) {
                        $scope.map.moveMarker($scope.mapMarker, lat, lng);
                    } else {
                        $scope.mapMarker = $scope.map.addMarker(lat, lng);
                    }
                    $scope.map.moveMap(lat, lng);
                    $scope.place.latitude = parseFloat(lat);
                    $scope.place.longitude = parseFloat(lng);
                    $scope.placeForm.address.$pristine = true;
                    $scope.placeForm.latitude.$pristine = true;
                    $scope.placeForm.longitude.$pristine = true;
                    $scope.$apply();
                });
        };

        $scope.save = function() {
            daoService.store($scope.place);
            $location.path(historyNavigator.prevUrl() || '/places');
        };
        $scope.cancel = function() {
            $location.path(historyNavigator.prevUrl() || '/places');
        }
    })
    .controller('CreatePlace', function($scope, $location, daoService, historyNavigator, $controller, $rootScope) {
        $scope.newItem = true;
        $scope.place = new Place();

        $controller('placeFormController', {
            $scope: $scope,
            $location: $location,
            daoService: daoService,
            historyNavigator: historyNavigator,
            $rootScope: $rootScope
        });
    })
    .controller('EditPlace', function($scope, daoService, $translate, $location, $routeParams, historyNavigator,
                                         $controller, $rootScope) {
        var placeId = $routeParams.placeId;
        $scope.place = daoService.load('Place', placeId);
        if(!$scope.place) {
            $location.path(historyNavigator.prevUrl() || '/places');
            return;
        }

        $controller('placeFormController', {
            $scope: $scope,
            $location: $location,
            daoService: daoService,
            historyNavigator: historyNavigator,
            $rootScope: $rootScope
        });

        $scope.newItem = false;
        $scope.delete = function() {
            $translate('CONFIRM_DELETE_PLACE')
                .then(function (message) {
                    if (confirm(message)) {
                        daoService.remove('Place', placeId);
                        $location.path(historyNavigator.prevUrl() || '/places');
                    }
                });
        };
    })
    .controller('ViewPlace', function($scope, daoService, $translate, $location, $routeParams, historyNavigator,
                                         $controller, $route, $timeout) {
        var placeId = $routeParams.placeId;
        $scope.place = daoService.load('Place', placeId);
        if(!$scope.place) {
            $location.path(historyNavigator.prevUrl() || '/places');
            return;
        }

        $scope.map = new Map('placeMap');
        $scope.$on('$viewContentLoaded', function() {
            $scope.map.initMap($scope.place.latitude, $scope.place.longitude);
            Map.getCoordinates($scope.place.latitude, $scope.place.longitude, $scope.place.address,
                function (lat, lng) {
                    $scope.map.addMarker(lat, lng);
                    $scope.map.moveMap(lat, lng);
                });
        });

        $controller('BaseListEvents', {
            $scope: $scope,
            daoService: daoService,
            $route: $route,
            $translate: $translate
        });

        $scope.events = $scope.place.events;

        initTimeout($scope, 'now', $timeout, 60000);

        $scope.back = function() {
            $location.path(historyNavigator.prevUrl() || '/places');
        };

        $scope.delete = function() {
            $translate('CONFIRM_DELETE_PLACE')
                .then(function (message) {
                    if (confirm(message)) {
                        daoService.remove('Place', placeId);
                        $location.path(historyNavigator.prevUrl() || '/places');
                    }
                });
        };
    });
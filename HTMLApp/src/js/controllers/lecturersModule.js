/*
 * Excellent - Student Organizer
 * Copyright (c) 2014, Sergey Pilipenko, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 */

angular.module('LecturersApp', [])
    .controller('ListLecturers', function($scope, daoService, $route, $translate) {
        $scope.lecturers = daoService.all('Lecturer');
        $scope.deleteLecturer = function(lecturerId) {
            $translate('CONFIRM_DELETE_LECTURER')
                .then(function (message) {
                    if (confirm(message)) {
                        daoService.remove('Lecturer', lecturerId);
                        $route.reload();
                    }
                });
        };

        $scope.sort = "lecturerName";
        $scope.reverse = false;

        $scope.changeSort = function(value){
            if ($scope.sort == value){
                $scope.reverse = !$scope.reverse;
                return;
            }

            $scope.sort = value;
            $scope.reverse = false;
        };
    })
    .controller('lecturerFormController', function($scope, $location, daoService, historyNavigator, $rootScope) {
        $rootScope.withoutReload = true;
        $scope.difficultyKeys = Object.keys(LecturerDifficulty);
        $scope.difficulties = LecturerDifficulty;

        $scope.save = function() {
            daoService.store($scope.lecturer);
            $location.path(historyNavigator.prevUrl() || '/lecturers');
        };
        $scope.cancel = function() {
            $location.path(historyNavigator.prevUrl() || '/lecturers');
        }
    })
    .controller('CreateLecturer', function($scope, $location, daoService, historyNavigator, $controller, $rootScope) {
        $controller('lecturerFormController', {
            $scope: $scope,
            $location: $location,
            daoService: daoService,
            historyNavigator: historyNavigator,
            $rootScope: $rootScope
        });

        $scope.newItem = true;
        $scope.lecturer = new Lecturer();
    })
    .controller('EditLecturer', function($scope, daoService, $translate, $location, $routeParams, historyNavigator,
                                         $controller, $rootScope) {
        var lecturerId = $routeParams.lecturerId;
        $scope.lecturer = daoService.load('Lecturer', lecturerId);
        if(!$scope.lecturer) {
            $location.path(historyNavigator.prevUrl() || '/lecturers');
            return;
        }

        $controller('lecturerFormController', {
            $scope: $scope,
            $location: $location,
            daoService: daoService,
            historyNavigator: historyNavigator,
            $rootScope: $rootScope
        });

        $scope.newItem = false;
        $scope.delete = function() {
            $translate('CONFIRM_DELETE_LECTURER')
                .then(function (message) {
                    if (confirm(message)) {
                        daoService.remove('Lecturer', lecturerId);
                        $location.path(historyNavigator.prevUrl() || '/lecturers');
                    }
                });
        };
    })
    .controller('ViewLecturer', function($scope, daoService, $translate, $location, $routeParams, historyNavigator,
                                         $controller, $route, $timeout) {
        var lecturerId = $routeParams.lecturerId;
        $scope.lecturer = daoService.load('Lecturer', lecturerId);
        if(!$scope.lecturer) {
            $location.path(historyNavigator.prevUrl() || '/lecturers');
            return;
        }

        $controller('BaseListEvents', {
            $scope: $scope,
            daoService: daoService,
            $route: $route,
            $translate: $translate
        });

        $controller('BaseListTasks', {
            $scope: $scope,
            daoService: daoService,
            $route: $route,
            $translate: $translate
        });

        $scope.events = [];
        $scope.tasks = [];
        var actions = $scope.lecturer.actions;

        if(actions) {
            for(var i = 0; i < actions.length; i++) {
                if(instanceOfClass(actions[i], ScheduleEvent)) {
                    $scope.events.push(actions[i]);
                } else if(instanceOfClass(actions[i], Task)) {
                    $scope.tasks.push(actions[i]);
                }
            }
        }

        initTimeout($scope, 'now', $timeout, 60000);

        $scope.back = function() {
            $location.path(historyNavigator.prevUrl() || '/lecturers');
        };

        $scope.delete = function() {
            $translate('CONFIRM_DELETE_LECTURER')
                .then(function (message) {
                    if (confirm(message)) {
                        daoService.remove('Lecturer', lecturerId);
                        $location.path(historyNavigator.prevUrl() || '/lecturers');
                    }
                });
        };
    });
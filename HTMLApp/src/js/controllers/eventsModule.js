/*
 * Excellent - Student Organizer
 * Copyright (c) 2014, Sergey Pilipenko, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 */

angular.module('EventsApp', [])
    .controller('BaseListEvents', function($scope, daoService, $route, $translate) {
        $scope.showOldEvents = true;

        $scope.eventsFilter = function(event) {
            return ($scope.showOldEvents || $scope.now < event.endTime);
        };

        $scope.eventSort = "startTime";
        $scope.eventReverse = false;
        $scope.getActivityName = function(event) {
            return event.activity != null ? event.activity.activityName : '';
        };
        $scope.getPlaceName = function(event) {
            return event.place != null ? event.place.placeName : '';
        };

        $scope.changeOldEventsVisibility = function() {
            $scope.showOldEvents = !$scope.showOldEvents;
        };

        $scope.changeEventSort = function(value){
            if ($scope.eventSort == value){
                $scope.eventReverse = !$scope.eventReverse;
                return;
            }

            $scope.eventSort = value;
            $scope.eventReverse = false;
        };

        $scope.deleteEvent = function(eventId) {
            $translate('CONFIRM_DELETE_EVENT')
                .then(function (message) {
                    if (confirm(message)) {
                        daoService.remove('ScheduleEvent', eventId);
                        $route.reload();
                    }
                });
        };
    })
    .controller('ListAllEvents', function($scope, daoService, $route, $translate, $timeout, $controller) {
        $controller('BaseListEvents', {
            $scope: $scope,
            daoService: daoService,
            $route: $route,
            $translate: $translate
        });

        $scope.events = daoService.all('ScheduleEvent');

        initTimeout($scope, 'now', $timeout, 60000);
    })
    .controller('eventFormController', function($scope, $location, daoService, historyNavigator, $rootScope) {
        $rootScope.withoutReload = true;
        $scope.activities = daoService.all('Activity');
        $scope.lecturers = daoService.all('Lecturer');
        $scope.places = daoService.all('Place');
        $scope.$watchCollection('[startTime, endTime]', function (newValues, oldValues, scope){
            var start = new Date(newValues[0]);
            var end = new Date(newValues[1]);
            scope.eventForm.$setValidity('wrongInterval', (start < end));
        });
        $scope.save = function() {
            $scope.event.startTime = new Date($scope.startTime);
            $scope.event.endTime = new Date($scope.endTime);
            $scope.event.activity = daoService.load('Activity', $scope.activityId);
            $scope.event.lecturer = daoService.load('Lecturer', $scope.lecturerId);
            $scope.event.place = daoService.load('Place', $scope.placeId);
            daoService.store($scope.event);
            $location.path(historyNavigator.prevUrl() || '/events');
        };
        $scope.cancel = function() {
            $location.path(historyNavigator.prevUrl() || '/events');
        }
    })
    .controller('CreateEvent', function($scope, daoService, $filter, $location, historyNavigator, $controller, $rootScope) {
        $controller('eventFormController', {
            $scope: $scope,
            $location: $location,
            daoService: daoService,
            historyNavigator: historyNavigator,
            $rootScope: $rootScope
        });

        var prevParams = historyNavigator.prevParams();
        $scope.newItem = true;
        $scope.event = new ScheduleEvent();
        $scope.activityId = prevParams.subjectId || "";
        $scope.lecturerId = prevParams.lecturerId || "";
        $scope.placeId = prevParams.placeId || "";

        var initStartTimeValue = new Date();
        if(prevParams.scheduleDate) {
            var calendarDate = new Date(prevParams.scheduleDate.replace(/_/g, ' '));
            initStartTimeValue = new Date(calendarDate.getTime() + 9 * 60 * 60 * 1000);
        }

        var initEndTimeValue = new Date(initStartTimeValue.getTime() + 60 * 60 * 1000);
        $scope.startTime = $filter('date')(initStartTimeValue, 'yyyy/MM/dd HH:mm');
        $scope.endTime = $filter('date')(initEndTimeValue, 'yyyy/MM/dd HH:mm');
    })
    .controller('EditEvent', function($scope, daoService, $translate, $filter, $location, $routeParams, historyNavigator,
                                      $controller, $rootScope) {
        var eventId = $routeParams.eventId;
        $scope.event = daoService.load('ScheduleEvent', eventId);
        if(!$scope.event) {
            $location.path(historyNavigator.prevUrl() || '/events');
            return;
        }

        $controller('eventFormController', {
            $scope: $scope,
            $location: $location,
            daoService: daoService,
            historyNavigator: historyNavigator,
            $rootScope: $rootScope
        });

        $scope.newItem = false;
        $scope.activityId = $scope.event.activity ? $scope.event.activity.id : '';
        $scope.lecturerId = $scope.event.lecturer ? $scope.event.lecturer.id : '';
        $scope.placeId = $scope.event.place ? $scope.event.place.id : '';
        $scope.startTime = $filter('date')($scope.event.startTime, 'yyyy/MM/dd HH:mm');
        $scope.endTime = $filter('date')($scope.event.endTime, 'yyyy/MM/dd HH:mm')

        $scope.delete = function() {
            $translate('CONFIRM_DELETE_EVENT')
                .then(function (message) {
                    if (confirm(message)) {
                        daoService.remove('ScheduleEvent', eventId);
                        $location.path(historyNavigator.prevUrl() || '/events');
                    }
                });
        };
    })
    .controller('ViewEvent', function($scope, daoService, $translate, $location, $routeParams, historyNavigator) {
        var eventId = $routeParams.eventId;
        $scope.event = daoService.load('ScheduleEvent', eventId);
        if(!$scope.event) {
            $location.path(historyNavigator.prevUrl() || '/events');
            return;
        }

        $scope.back = function() {
            $location.path(historyNavigator.prevUrl() || '/events');
        };

        $scope.delete = function() {
            $translate('CONFIRM_DELETE_EVENT')
                .then(function (message) {
                    if (confirm(message)) {
                        daoService.remove('ScheduleEvent', eventId);
                        $location.path(historyNavigator.prevUrl() || '/events');
                    }
                });
        };
    })
    .controller('RepeatEvent', function($scope, daoService, $translate, $filter, $location, $routeParams,
                                        historyNavigator, $rootScope) {
        $rootScope.withoutReload = true;
        var eventId = $routeParams.eventId;
        $scope.event = daoService.load('ScheduleEvent', eventId);
        if(!$scope.event) {
            $location.path(historyNavigator.prevUrl() || '/events');
            return;
        }

        $scope.frequencies = {
            Day: { name: 'REPEAT_EVERY_DAY', value: 0 },
            Week: { name: 'REPEAT_EVERY_WEEK', value: 1 },
            Month: { name: 'REPEAT_EVERY_MONTH', value: 2 },
            Year: { name: 'REPEAT_EVERY_YEAR', value: 3 }
        };
        $scope.frequenciesKeys = Object.keys($scope.frequencies);
        $scope.frequency = $scope.frequencies.Day.value;

        $scope.untilOptions = {
            Times: { name: 'UNTIL_TIMES', value: 0},
            Date: { name: 'UNTIL_DATE', value: 1}
        };
        $scope.untilOptionsKeys = Object.keys($scope.untilOptions);
        $scope.until = $scope.untilOptions.Times.value;

        $scope.times = 1;
        $scope.endTime =  $filter('date')(new Date(), 'yyyy/MM/dd HH:mm');;

        $scope.confirm = function() {
            var i = 0;
            var dateStart = new Date($scope.event.startTime.getTime());
            var dateEnd = new Date($scope.event.endTime.getTime());
            var finishDate = new Date($scope.endTime);
            var frequency = +$scope.frequency;
            while(true) {
                switch(frequency) {
                    case $scope.frequencies.Day.value:
                        dateStart.setDate(dateStart.getDate() + 1);
                        dateEnd.setDate(dateEnd.getDate() + 1);
                        break;
                    case $scope.frequencies.Week.value:
                        dateStart.setDate(dateStart.getDate() + 7);
                        dateEnd.setDate(dateEnd.getDate() + 7);
                        break;
                    case $scope.frequencies.Month.value:
                        dateStart.setMonth(dateStart.getMonth() + 1);
                        dateEnd.setMonth(dateEnd.getMonth() + 1);
                        break;
                    case $scope.frequencies.Year.value:
                        dateStart.setFullYear(dateStart.getFullYear() + 1);
                        dateEnd.setFullYear(dateEnd.getFullYear() + 1);
                        break;
                    default:
                        return;
                }
                if(($scope.until == $scope.untilOptions.Times.value && i < $scope.times)
                    || ($scope.until == $scope.untilOptions.Date.value && finishDate.getTime() >= dateStart.getTime())) {
                    i++;
                    var event = new ScheduleEvent();
                    event.actionName = $scope.event.actionName;
                    event.startTime = new Date(dateStart.getTime());
                    event.endTime = new Date(dateEnd.getTime());
                    event.activity = $scope.event.activity;
                    event.lecturer = $scope.event.lecturer;
                    event.place = $scope.event.place;
                    event.description = $scope.event.description;
                    daoService.store(event);
                } else {
                    break;
                }
            }
            $location.path(historyNavigator.prevUrl() || '/events');
        };
        $scope.cancel = function() {
            $location.path(historyNavigator.prevUrl() || '/events');
        };
    });
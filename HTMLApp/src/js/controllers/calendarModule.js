/*
 * Excellent - Student Organizer
 * Copyright (c) 2014, Sergey Pilipenko, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 */

angular.module('CalendarApp', [])
    .controller('Calendar', function($scope, daoService, $location, $filter) {
        var taskDeadlines = daoService.filter(Task, undefined, {'deadline' : 'deadline'});
        var deadlinesDates = {};
        for(var i = 0; i < taskDeadlines.length; i++) {
            deadlinesDates[$filter('date')(taskDeadlines[i].deadline, 'yyyy MM dd')] = true;
        }

        var eventsRanges = daoService.filter(ScheduleEvent, undefined, {'startTime' : 'startTime', 'endTime' : 'endTime'});
        var eventsDates = {};
        for(var j = 0; j < eventsRanges.length; j++) {
            var startTime = eventsRanges[j].startTime;
            var endTime = eventsRanges[j].endTime;
            while(startTime < endTime) {
                eventsDates[$filter('date')(startTime, 'yyyy MM dd')] = true;
                startTime = new Date(startTime.getTime() + 24*60*60*1000);
            }
            eventsDates[$filter('date')(endTime, 'yyyy MM dd')] = true;
        }

        $scope.addActionsOnCalendar = function() {
            var prop;
            for(prop in deadlinesDates) {
                if(deadlinesDates.hasOwnProperty(prop)) {
                    var deadline = new Date(prop);
                    jQuery('td[data-date="' + deadline.getDate() + '"]'
                        + '[data-month="' + deadline.getMonth() + '"]'
                        + '[data-year="' + deadline.getFullYear() + '"].xdsoft_date')
                        .addClass('taskCalendarItem');
                }
            }
            for(prop in eventsDates) {
                if(eventsDates.hasOwnProperty(prop)) {
                    var eventDate = new Date(prop);
                    jQuery('td[data-date="' + eventDate.getDate() + '"]'
                        + '[data-month="' + eventDate.getMonth() + '"]'
                        + '[data-year="' + eventDate.getFullYear() + '"].xdsoft_date')
                        .addClass('eventCalendarItem');
                }
            }
        };

        $scope.goToDay = function(value) {
            $scope.$apply(function() {
               $location.path('/daySchedule/' + value);
            });
        };
    })
    .controller('DayScheduler', function($scope, daoService, $routeParams, $filter, $location, $timeout, $translate,
                                         $route, $controller) {
        var day = $routeParams.scheduleDate.replace(/_/g, ' ');
        if(validateDate(day, $filter, 'yyyy MM dd')) {
            $controller('BaseListEvents', {
                $scope: $scope,
                daoService: daoService,
                $route: $route,
                $translate: $translate
            });

            $controller('BaseListTasks', {
                $scope: $scope,
                daoService: daoService,
                $route: $route,
                $translate: $translate
            });

            var dayStart = new Date(day);
            var dayEnd = new Date(dayStart.getTime() + 24 * 60 * 60 * 1000);
            var tasksDeadlineFilter = function(deadline) {
                return deadline >= dayStart && deadline < dayEnd;
            };

            var eventsDatabaseFilter = function(event) {
                return (event.startTime >= dayStart && event.startTime < dayEnd)
                    || (event.endTime >= dayStart && event.endTime < dayEnd)
                    || (event.startTime < dayStart && event.endTime >= dayEnd);
            };

            $scope.tasks = daoService.filter(Task, {deadline: tasksDeadlineFilter});
            $scope.events = daoService.filter(ScheduleEvent, {complexConditions: [eventsDatabaseFilter]});

            initTimeout($scope, 'now', $timeout, 60000);
            $scope.scheduleDate = dayStart;
        } else {
            $location.path('/calendar');
        }
    })
    .controller('MapScheduler', function($scope, daoService, $routeParams, $filter, $location, $rootScope) {
        var day = $routeParams.scheduleDate.replace(/_/g, ' ');
        if(validateDate(day, $filter, 'yyyy MM dd') && $rootScope.online) {
            var dayStart = new Date(day);
            var dayEnd = new Date(dayStart.getTime() + 24 * 60 * 60 * 1000);
            var eventsDatabaseFilter = function(event) {
                return (event.startTime >= dayStart && event.startTime < dayEnd)
                    || (event.endTime >= dayStart && event.endTime < dayEnd)
                    || (event.startTime < dayStart && event.endTime >= dayEnd);
            };

            $scope.events = daoService.filter(ScheduleEvent,
                {'place.id': function(placeId) { return (placeId != "null")}, complexConditions: [eventsDatabaseFilter]},
                undefined, ['startTime']);

			$scope.scheduleDate = dayStart;
			$scope.map = new Map('scheduleMap');
			$scope.$on('$viewContentLoaded', function() {
				$scope.map.initMap();
				var places = [];
				var firstPlace = null;
				var markersCount = 0;
				for(var i = 0; i < $scope.events.length; i++) {
					var place = $scope.events[i].place;
					for(var j = 0; j < i; j++) {
						if(place.id == $scope.events[j].place.id ) {
							places[i] = places[j];
							break;
						}
					}
					if(places[i] === undefined) {
						Map.getCoordinates(place.latitude, place.longitude, place.address,
							function () {
								var index = i;
								var plc = place;
								return function (lat, lng) {
									$scope.map.addMarker(lat, lng);
									places[index] = markersCount;
									markersCount++;
									if (firstPlace == null) {
										firstPlace = plc;
										$scope.map.moveMap(lat, lng);
									}
								};
							}());
					}
				}
				for(i = 1; i < $scope.events.length; i++) {
					for(j =  i - 1; j >= 0; j--) {
						if(places[j] !== undefined) {
							$scope.map.addRoute($scope.map.markers[places[j]], $scope.map.markers[places[i]]);
							break;
						}
					}
				}
				var labels = new Array(markersCount);
				for(i = 0; i < $scope.events.length; i++) {
					if(places[i] !== undefined) {
						var event = $scope.events[i];
						var text = labels[places[i]] || "";
						labels[places[i]] = text + "<a href='event/" + event.id + "'>" + event.actionName + "</a>"
							+"&nbsp;&nbsp;&nbsp;"
							+ $filter('date')(event.startTime, 'dd/MM HH:mm') + ' - '
							+ $filter('date')(event.endTime, 'dd/MM HH:mm') + '<br/>';
					}
				}
				for(i = 0; i < markersCount; i++) {
					$scope.map.addLabel($scope.map.markers[i], labels[i]);
				}
			});
        } else {
            $location.path('/calendar');
        }
    });
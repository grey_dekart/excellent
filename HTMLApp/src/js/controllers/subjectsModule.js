/*
 * Excellent - Student Organizer
 * Copyright (c) 2014, Sergey Pilipenko, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 */

angular.module('SubjectsApp', [])
    .controller('ListSubjects', function($scope, daoService, $route, $translate) {
        $scope.subjects = daoService.all('Subject');
        $scope.deleteSubject = function(subjectId) {
            $translate('CONFIRM_DELETE_SUBJECT')
                .then(function (message) {
                    if (confirm(message)) {
                        daoService.remove('Subject', subjectId);
                        $route.reload();
                    }
                });
        };

        $scope.sort = "activityName";
        $scope.reverse = false;

        $scope.changeSort = function(value){
            if ($scope.sort == value){
                $scope.reverse = !$scope.reverse;
                return;
            }

            $scope.sort = value;
            $scope.reverse = false;
        };
    })
    .controller('subjectFormController', function($scope, $location, daoService, historyNavigator, $rootScope) {
        $rootScope.withoutReload = true;
        $scope.evaluationTypeKeys = Object.keys(EvaluationType);
        $scope.evaluationTypes = EvaluationType;

        $scope.save = function() {
            daoService.store($scope.subject);
            $location.path(historyNavigator.prevUrl() || '/subjects');
        };
        $scope.cancel = function() {
            $location.path(historyNavigator.prevUrl() || '/subjects');
        }
    })
    .controller('CreateSubject', function($scope, $location, daoService, historyNavigator, $controller, $rootScope) {
        $controller('subjectFormController', {
            $scope: $scope,
            daoService: daoService,
            $location: $location,
            historyNavigator: historyNavigator,
            $rootScope: $rootScope
        });

        $scope.newItem = true;
        $scope.subject = new Subject();
    })
    .controller('EditSubject', function($scope, daoService, $translate, $location, $routeParams, historyNavigator,
                                        $controller, $rootScope) {
        var subjectId = $routeParams.subjectId;
        $scope.subject = daoService.load('Subject', subjectId);
        if(!$scope.subject) {
            $location.path(historyNavigator.prevUrl() || '/subjects');
            return;
        }

        $controller('subjectFormController', {
            $scope: $scope,
            daoService: daoService,
            $location: $location,
            historyNavigator: historyNavigator,
            $rootScope: $rootScope
        });

        $scope.newItem = false;
        $scope.delete = function() {
            $translate('CONFIRM_DELETE_SUBJECT')
                .then(function (message) {
                    if (confirm(message)) {
                        daoService.remove('Subject', subjectId);
                        $location.path(historyNavigator.prevUrl() || '/subjects');
                    }
                });
        };
    })
    .controller('ViewSubject', function($scope, daoService, $translate, $location, $routeParams, historyNavigator,
                                         $controller, $route, $timeout) {
        var subjectId = $routeParams.subjectId;
        $scope.subject = daoService.load('Subject', subjectId);
        if(!$scope.subject) {
            $location.path(historyNavigator.prevUrl() || '/subjects');
            return;
        }

        $scope.evaluationTypes = EvaluationType;

        $controller('BaseListEvents', {
            $scope: $scope,
            daoService: daoService,
            $route: $route,
            $translate: $translate
        });

        $controller('BaseListTasks', {
            $scope: $scope,
            daoService: daoService,
            $route: $route,
            $translate: $translate
        });
        $controller('BaseListMarks', {
            $scope: $scope,
            daoService: daoService,
            $route: $route,
            $translate: $translate
        });

        $scope.marks = $scope.subject.marks;

        var totalMark = 0;
        for(var j = 0; j < $scope.marks.length; j++) {
            var value = +$scope.marks[j].value;
            if (value === NaN) {
                totalMark = null;
            } else {
                totalMark += value;
            }
        }
        $scope.totalMark = totalMark;

        $scope.events = [];
        $scope.tasks = [];
        var actions = $scope.subject.actions;

        if(actions) {
            for(var i = 0; i < actions.length; i++) {
                if(instanceOfClass(actions[i], ScheduleEvent)) {
                    $scope.events.push(actions[i]);
                } else if(instanceOfClass(actions[i], Task)) {
                    $scope.tasks.push(actions[i]);
                }
            }
        }

        initTimeout($scope, 'now', $timeout, 60000);

        $scope.back = function() {
            $location.path(historyNavigator.prevUrl() || '/subjects');
        };

        $scope.delete = function() {
            $translate('CONFIRM_DELETE_SUBJECT')
                .then(function (message) {
                    if (confirm(message)) {
                        daoService.remove('Subject', subjectId);
                        $location.path(historyNavigator.prevUrl() || '/subjects');
                    }
                });
        };
    });
/*
 * Excellent - Student Organizer
 * Copyright (c) 2014, Sergey Pilipenko, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 */

angular.module('TasksApp', [])
    .controller('BaseListTasks', function($scope, daoService, $route, $translate) {
        $scope.showCompletedTasks = true;
        $scope.showOverdueTasks = true;

        $scope.changeCompletedTasksVisibility = function() {
            $scope.showCompletedTasks = !$scope.showCompletedTasks;
        };

        $scope.changeOverdueTasksVisibility = function() {
            $scope.showOverdueTasks = !$scope.showOverdueTasks;
        };

        $scope.tasksFilter = function(task) {
            return ($scope.showCompletedTasks || !task.completed)
                && ($scope.showOverdueTasks || $scope.now < task.deadline);
        };

        $scope.taskSort = "deadline";
        $scope.taskReverse = false;
        $scope.getActivityName = function(task) {
            return task.activity != null ? task.activity.activityName : '';
        };

        $scope.changeTaskSort = function(value){
            if ($scope.taskSort == value){
                $scope.taskReverse = !$scope.taskReverse;
                return;
            }

            $scope.taskSort = value;
            $scope.taskReverse = false;
        };

        $scope.deleteTask = function(taskId) {
            $translate('CONFIRM_DELETE_TASK')
                .then(function (message) {
                    if (confirm(message)) {
                        daoService.remove('Task', taskId);
                        $route.reload();
                    }
                });
        };
        $scope.saveTasks = function() {
            for(var i = 0; i < $scope.tasks.length; i++) {
                daoService.store($scope.tasks[i]);
            }
        }
    })
    .controller('ListAllTasks', function($scope, daoService, $route, $translate, $timeout, $controller) {
        $controller('BaseListTasks', {
            $scope: $scope,
            daoService: daoService,
            $route: $route,
            $translate: $translate
        });

        $scope.tasks = daoService.all('Task');
        initTimeout($scope, 'now', $timeout, 60000);
    })
    .controller('taskFormController', function($scope, $location, daoService, historyNavigator, $rootScope) {
        $rootScope.withoutReload = true;
        $scope.difficultyTypeKeys = Object.keys(TaskDifficulty);
        $scope.difficultyTypes = TaskDifficulty;
        $scope.activities = daoService.all('Activity');
        $scope.lecturers = daoService.all('Lecturer');

        $scope.save = function() {
            $scope.task.difficulty = toTaskDifficulty($scope.task.difficulty);
            $scope.task.deadline = new Date($scope.deadline);
            $scope.task.activity = daoService.load('Activity', $scope.activityId);
            $scope.task.lecturer = daoService.load('Lecturer', $scope.lecturerId);
            daoService.store($scope.task);
            $location.path(historyNavigator.prevUrl() || '/tasks');
        };
        $scope.cancel = function() {
            $location.path(historyNavigator.prevUrl() || '/tasks');
        }
    })
    .controller('CreateTask', function($scope, $location, $filter, daoService, historyNavigator, $controller, $rootScope) {
        $controller('taskFormController', {
            $scope: $scope,
            $location: $location,
            daoService: daoService,
            historyNavigator: historyNavigator,
            $rootScope: $rootScope
        });

        var prevParams = historyNavigator.prevParams();

        $scope.newItem = true;
        $scope.task = new Task();
        $scope.activityId = prevParams.subjectId || "";
        $scope.lecturerId = prevParams.lecturerId || "";

        var initTimeValue = new Date();
        if(prevParams.scheduleDate) {
            var calendarDate = new Date(prevParams.scheduleDate.replace(/_/g, ' '));
            initTimeValue = new Date(calendarDate.getTime() + 9 * 60 * 60 * 1000);
        }

        $scope.deadline = $filter('date')(initTimeValue, 'yyyy/MM/dd HH:mm')
    })
    .controller('EditTask', function($scope, daoService, $translate, $filter, $location, $routeParams, historyNavigator,
            $controller, $rootScope) {
        var taskId = $routeParams.taskId;
        $scope.newItem = false;
        $scope.task = daoService.load('Task', taskId);
        if(!$scope.task) {
            $location.path(historyNavigator.prevUrl() || '/tasks');
            return;
        }

        $controller('taskFormController', {
            $scope: $scope,
            $location: $location,
            daoService: daoService,
            historyNavigator: historyNavigator,
            $rootScope: $rootScope
        });

        $scope.activityId = $scope.task.activity ? $scope.task.activity.id : '';
        $scope.lecturerId = $scope.task.lecturer ? $scope.task.lecturer.id : '';
        $scope.deadline = $filter('date')($scope.task.deadline, 'yyyy/MM/dd HH:mm');
        $scope.delete = function() {
            $translate('CONFIRM_DELETE_TASK')
                .then(function (message) {
                    if (confirm(message)) {
                        daoService.remove('Task', taskId);
                        $location.path(historyNavigator.prevUrl() || '/tasks');
                    }
                });
        };
    })
    .controller('ViewTask', function($scope, daoService, $translate, $location, $routeParams, historyNavigator) {
        var taskId = $routeParams.taskId;
        $scope.task = daoService.load('Task', taskId);
        if(!$scope.task) {
            $location.path(historyNavigator.prevUrl() || '/tasks');
            return;
        }

        $scope.back = function() {
            $location.path(historyNavigator.prevUrl() || '/tasks');
        };

        $scope.delete = function() {
            $translate('CONFIRM_DELETE_TASK')
                .then(function (message) {
                    if (confirm(message)) {
                        daoService.remove('Task', taskId);
                        $location.path(historyNavigator.prevUrl() || '/tasks');
                    }
                });
        };
    });
/*
 * Excellent - Student Organizer
 * Copyright (c) 2014, Sergey Pilipenko, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 */

function NotificationService() {}

NotificationService.prototype.updateNotifications = function($rootScope, $cookies, daoService) {
    var now = new Date();

    var easyTaskNotificationLimit = new Date(now.getTime());
    easyTaskNotificationLimit.setHours(easyTaskNotificationLimit.getHours() + 12);

    var normalTaskNotificationLimit = new Date(now.getTime());
    normalTaskNotificationLimit.setHours(normalTaskNotificationLimit.getHours() + 24);

    var difficultTaskNotificationLimit = new Date(now.getTime());
    difficultTaskNotificationLimit.setHours(difficultTaskNotificationLimit.getHours() + 24);
    var tasksDatabaseFilter = function(task) {
        var showNotification;
        switch(task.difficulty) {
            case TaskDifficulty.Easy:
                showNotification = task.deadline > now && task.deadline <= easyTaskNotificationLimit;
                break;
            case TaskDifficulty.Normal:
                showNotification = task.deadline > now && task.deadline <= normalTaskNotificationLimit;
                break;
            case TaskDifficulty.Hard:
                showNotification = task.deadline > now && task.deadline <= difficultTaskNotificationLimit;
                break;
        }
        return showNotification;
    };
    var tasks = daoService.filter(Task, {complexConditions: [tasksDatabaseFilter]});

    var eventNotificationLimit = new Date(now.getTime());
    eventNotificationLimit.setHours(eventNotificationLimit.getHours() + 5);
    var eventsDatabaseFilter = function(event) {
        return (event.startTime <= eventNotificationLimit && event.endTime > now);
    };
    var events = daoService.filter(ScheduleEvent,
        {'withNotification': true, complexConditions: [eventsDatabaseFilter]});

    var tasksNotifications = [];
    for(var i = 0; i < tasks.length; i++) {
        if(!$cookies['hide_task_notif_' + tasks[i].id]) {
            tasksNotifications.push(tasks[i]);
        }
    }

    var eventsNotifications = [];
    for(i = 0; i < events.length; i++) {
        if(!$cookies['hide_event_notif_' + events[i].id]) {
            eventsNotifications.push(events[i]);
        }
    }

    $rootScope.tasksNotifications = tasksNotifications;
    $rootScope.eventsNotifications = eventsNotifications;

    $rootScope.hideTaskNotification = function(id, index) {
        $cookies['hide_task_notif_' + id] = true;
        $rootScope.tasksNotifications.splice(index, 1);
    };
    $rootScope.hideEventNotification = function(id, index) {
        $cookies['hide_event_notif_' + id] = true;
        $rootScope.eventsNotifications.splice(index, 1);
    };
};
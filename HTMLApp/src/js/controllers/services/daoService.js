/*
 * Excellent - Student Organizer
 * Copyright (c) 2014, Sergey Pilipenko, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 */

function DaoService($rootScope) {
    this.rootScope = $rootScope;
    this.dao = new DAO();
    this.syncTables = ['Activity', 'ScheduleAction', 'Lecturer', 'Mark', 'Place'];
}

DaoService.prototype.store = function(obj, transaction) {
    var userId = this.rootScope.user ? this.rootScope.user.id : null;

    if(obj.userId == null && userId) {
        obj.userId = userId;
    }

    if(obj.userId == userId) {
        obj.updated = new Date();
        this.dao.store(obj, transaction);
    }
};

DaoService.prototype.load = function(entity, id) {
    var obj = this.dao.load(entity, id);
    var userId = this.rootScope.user ? this.rootScope.user.id : null;
    if(obj && obj.userId == userId) {
        return obj;
    }
    return null;
};

DaoService.prototype.remove = function(entity, id, transaction) {
    var userId = this.rootScope.user ? this.rootScope.user.id : null;
    var obj = this.dao.load(entity, id);
    if(obj && userId == obj.userId) {
        if(userId) {
            entity = typeof entity == "function" ? entity : MAPPING[entity];
            var mapping = entity ? entity.mapping : undefined;
            if(mapping.collections !== undefined) {
                for (var collection in mapping.collections) {
                    if (mapping.collections.hasOwnProperty(collection)) {
                        var collectionInfo = mapping.collections[collection];
                        switch (collectionInfo.cascade) {
                            case Cascade.All:
                                var children = obj[collection];
                                for(var i = 0; i < children.length; i++) {
                                    this.store(children[i], transaction);
                                }
                                break;
                            case Cascade.Set_Null:
                                var children = obj[collection];
                                for(var i = 0; i < children.length; i++) {
                                    var child = children[i];
                                    child[collectionInfo.key] = null;
                                    this.store(child, transaction);
                                }
                                break;
                        }
                        obj[collection] = [];
                    }
                }
            }
            obj.updated =  new Date();
            obj.deleted = true;
            this.dao.store(obj, transaction);
        } else {
            this.dao.remove(entity, id, transaction);
        }
    }
};

DaoService.prototype.all = function(entity) {
    var userId = this.rootScope.user ? this.rootScope.user.id : null;
    return this.dao.filter(entity, {'userId':userId, 'deleted' : function(deleted) { return !deleted; }});
};

DaoService.prototype.filter = function(entity, conditions, selectValues, order) {
    var userId = this.rootScope.user ? this.rootScope.user.id : null;
    var newConditions = clone(conditions || {});
    extendObject(newConditions, {'userId':userId, 'deleted' : function(deleted) { return !deleted; }});
    return this.dao.filter(entity, newConditions, selectValues, order);
};

DaoService.prototype.getUnassociatedItems = function() {
    var unassociatedItems = [];
    for(var i = 0; i < this.syncTables.length; i++) {
        unassociatedItems = unassociatedItems.concat(this.dao.filter(this.syncTables[i], {'userId':null }));
    }
    return unassociatedItems;
};

DaoService.prototype.associateItems = function(unassociatedItems) {
    if(this.rootScope.user) {
        var transaction = [];
        for(var i = 0; i < unassociatedItems.length; i++) {
            this.store(unassociatedItems[i], transaction);
        }
        try {
            for (var j = 0; j < transaction.length; j++) {
                var operation = transaction[j];
                operation.apply(this.dao);
            }
        } catch(e) {
            //TODO: Transaction behaviour
        }
    }
};

DaoService.prototype.getLastSyncDate = function() {
    if (this.rootScope.user) {
        var userId = this.rootScope.user.id;
        var lastSyncDate = localStorage.getItem('sync_' + userId);
        return lastSyncDate != null ? new Date(lastSyncDate) : null;
    }
    return null;
};

DaoService.prototype.getUnsyncItems = function() {
    if(this.rootScope.user) {
        var userId =  this.rootScope.user.id;
        var unsyncItems = [];
        var filter = {'userId': userId};
        var lastSyncDate = localStorage.getItem('sync_' + userId);
        if(lastSyncDate) {
            filter['updated'] = function(updated) { return (updated > new Date(lastSyncDate)); };
        }
        for(var i = 0; i < this.syncTables.length; i++) {
            unsyncItems = unsyncItems.concat(this.dao.filter(this.syncTables[i], filter));
        }
        return unsyncItems;
    } else {
        return null;
    }
};

DaoService.prototype.syncItems = function(items, syncDate) {
    if(this.rootScope.user) {
        var userId =  this.rootScope.user.id;
        var transaction = [];
        for(var i = 0; i < items.length; i++) {
            this.dao.store(items[i], transaction);
        }
        try {
            for (var j = 0; j < transaction.length; j++) {
                var operation = transaction[j];
                operation.apply(this.dao);
            }
        } catch(e) {
            //TODO: Transaction behaviour
        }
        localStorage.setItem('sync_' + userId, syncDate.toISOString());

        var filter = {
            'userId': userId,
            'deleted': function(deleted) { return deleted; },
            'updated': function(updated) { return (updated < syncDate); }
        };
        for(i = 0; i < this.syncTables.length; i++) {
            var deletedItems = (this.dao.filter(this.syncTables[i], filter));
            for(j = 0; j < deletedItems.length; j++) {
                this.dao.remove(this.syncTables[i], deletedItems[j].id);
            }
        }
    }
};
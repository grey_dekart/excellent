/*
 * Excellent - Student Organizer
 * Copyright (c) 2014, Sergey Pilipenko, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 */

function SyncService(serverAddress, $rootScope, $cookies, daoService, $http, $route) {
    var initUnassociatedItemsUtils = function() {
        if($rootScope.user && !$cookies.hideUnassociatedItemsMessage) {
            $rootScope.unassociatedItems = daoService.getUnassociatedItems();
            $rootScope.closeUnassociatedItemsMessage = function() {
                $rootScope.unassociatedItems = null;
                $cookies.hideUnassociatedItemsMessage = true;
            };

            $rootScope.associateItems = function() {
                if($rootScope.unassociatedItems) {
                    daoService.associateItems($rootScope.unassociatedItems);
                    $rootScope.unassociatedItems = null;
                    $cookies.hideUnassociatedItemsMessage = true;
                    $route.reload();
                }
            };
        } else {
            $rootScope.unassociatedItems = [];
        }
    };

    var syncFunction = function() {
        if($rootScope.online && $rootScope.user) {
            var syncDate = new Date();
            var unsyncItems = daoService.getUnsyncItems() || [];
            var entities = [];
            for (var i = 0; i < unsyncItems.length; i++) {
                entities.push(unsyncItems[i].serialize());
            }
            var requestData = {
                lastUpdate: daoService.getLastSyncDate(),
                entities: entities
            };
            $http({
                url: serverAddress + "/sync",
                dataType: "json",
                method: "POST",
                withCredentials: true,
                data: angular.toJson(requestData),
                headers: {
                    "Content-Type": "application/json"
                }
            }).success(function (data, status, headers, config) {
                var items = [];
                for (var j = 0; j < data.length; j++) {
                    var item = deserializer(data[j]);
                    if (item) {
                        items.push(item);
                    }
                }
                daoService.syncItems(items, syncDate);
                if(!$rootScope.withoutReload) {
                    $route.reload();
                }
            });
        }
    };

    return {
        initUnassociatedItemsUtils : initUnassociatedItemsUtils,
        synchronize: syncFunction
    };
}
/*
 * Excellent - Student Organizer
 * Copyright (c) 2014, Sergey Pilipenko, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 */

function HistoryService() {
    this.history = [];
    this.paramsHistory = [];
    this.historyBack = false;
}

HistoryService.prototype.onUrlChanged = function(prevUrl,prevParams) {
    if(!this.historyBack) {
        this.history.push(prevUrl);
        this.paramsHistory.push(prevParams);
    }
    this.historyBack = false;
};

HistoryService.prototype.prevParams = function() {
    var params = {};
    if (this.paramsHistory.length > 0) {
        params = this.paramsHistory[this.paramsHistory.length - 1];
    }
    return params;
};

HistoryService.prototype.prevUrl = function() {
    this.historyBack = true;
    var url = null;
    if (this.history.length > 0) {
        var lastElem = this.history.length - 1;
        url = this.history.splice(lastElem, 1)[0];
        this.paramsHistory.splice(lastElem, 1);
    }
    return url;
};
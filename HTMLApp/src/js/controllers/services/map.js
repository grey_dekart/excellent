/*
 * Excellent - Student Organizer
 * Copyright (c) 2014, Sergey Pilipenko, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 */

var geocoder = new google.maps.Geocoder();
var directionsService = new google.maps.DirectionsService();

function Map(containerId) {
    this.containerId = containerId;
    this.map = null;
    this.markers = [];
    this.default_location = new google.maps.LatLng(50.383219, 30.470824);
    this.directionsDisplays = [];
}

Map.prototype.initMap = function(lat, lng) {
    var latLng = (lat != null && lng != null)
        ? new google.maps.LatLng(lat, lng)
        : this.default_location;
    var mapOptions = {
        zoom: 14,
        center: latLng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    this.map = new google.maps.Map(document.getElementById(this.containerId), mapOptions);
};

Map.prototype.addMarker = function(lat, lng) {
    var latLng = new google.maps.LatLng(lat, lng);
    var mapOptions = {
        zoom: 14,
        center: latLng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var marker = new google.maps.Marker({ 'position': latLng, 'map': this.map });
    this.markers.push(marker);
    return marker;
};

Map.prototype.moveMap = function(lat, lng) {
    var latLng = new google.maps.LatLng(lat, lng);
    this.map.panTo(latLng);
};

Map.prototype.moveMarker = function(marker, lat, lng) {
    var latLng = new google.maps.LatLng(lat, lng);
    marker.setOptions({ 'position': latLng, 'map': this.map });
};

Map.prototype.addListener = function(action, callback) {
    google.maps.event.addListener(this.map, action, callback);
};

Map.getCoordinates = function(lat, lng, address, onSuccess) {
    if(lat != null && lng != null) {
        onSuccess(lat, lng);
    } else if (address != null) {
        geocoder.geocode({ 'address': address }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                var latLng = results[0].geometry.location;
                var lat = latLng.lat().toFixed(6);
                var lng = latLng.lng().toFixed(6);
                onSuccess(parseFloat(lat), parseFloat(lng));
            }
        });
    }
};

Map.prototype.addRoute = function(startMarker, endMarker) {
    if(startMarker && endMarker) {
        var request = {
            origin: startMarker.getPosition(),
            destination: endMarker.getPosition(),
            travelMode: google.maps.TravelMode.DRIVING
        };
        var display = new google.maps.DirectionsRenderer({
            suppressMarkers: true,
            preserveViewport: true
        });
        display.setMap(this.map);
        this.directionsDisplays.push(display);
        directionsService.route(request, function (response, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                display.setDirections(response);
            }
        });
    }
};

Map.prototype.addLabel = function(marker, content) {
    var contentDiv = document.createElement('div');
    contentDiv.className = 'mapMarker';
    contentDiv.innerHTML = content;

    var infoWindow = new google.maps.InfoWindow({
        content: contentDiv
    });
    var map = this.map;
    google.maps.event.addListener(marker, 'click', function () { infoWindow.open(map, marker); });
};
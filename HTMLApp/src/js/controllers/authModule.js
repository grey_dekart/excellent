/*
 * Excellent - Student Organizer
 * Copyright (c) 2014, Sergey Pilipenko, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 */

angular.module('AuthApp', [])
    .controller('LogoutController' , function($rootScope, $cookieStore, serverAddress, $http, $location) {
        if(!$rootScope.user) {
            $location.path('/');
            return;
        }
        $http({
            withCredentials: true,
            url: serverAddress + "/logout",
            method: "POST",
            data: {}
        });
        $cookieStore.remove('JSESSIONID');
        $cookieStore.remove('hideUnassociatedItemsMessage');
        $rootScope.user = null;
        $location.path('/');
    })
    .controller('LoginForm', function($scope, $http, $cookies, $location, $rootScope, serverAddress) {
        if($rootScope.user) {
            $location.path('/');
            return;
        }
        $scope.loginRequest = {};
        $scope.disableButtons = false;

        $scope.login = function() {
            $scope.disableButtons = true;
            $http({
                url: serverAddress + "/login",
                dataType: "json",
                method: "POST",
                withCredentials: true,
                data: angular.toJson($scope.loginRequest),
                headers: {
                    "Content-Type": "application/json"
                }
            }).success(function(data, status, headers, config) {
                $rootScope.user = data;
                $location.path('/');
            })
            .error(function(data, status, headers, config) {
                $scope.loginError = data.message;
                $scope.disableButtons = false;
            });
        };
    })
    .controller('RegisterForm',  function($scope, $http, $cookies, $location, $rootScope, serverAddress) {
        if($rootScope.user) {
            $location.path('/');
            return;
        }
        $scope.registrationRequest = {};
        $scope.disableButtons = false;
        $scope.$watchCollection('[registrationRequest.password, registrationRequest.confirmPassword]',
                function (newValues, oldValues, scope){
                    var password = newValues[0];
                    var confirmPassword = newValues[1];
                    scope.registerForm.password.$setValidity('mismatch', (password == confirmPassword));
                }
        );

        $scope.register = function() {
            $scope.disableButtons = true;
            $http({
                url: serverAddress + "/register",
                dataType: "json",
                method: "POST",
                withCredentials: true,
                data: angular.toJson($scope.registrationRequest),
                headers: {
                    "Content-Type": "application/json"
                }
            }).success(function(data, status, headers, config) {
                $rootScope.user = data;
                $location.path('/');
            })
            .error(function(data, status, headers, config) {
                $scope.registerError = data.message;
                $scope.disableButtons = false;
            });
        };
    });
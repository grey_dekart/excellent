/*
 * Excellent - Student Organizer
 * Copyright (c) 2014, Sergey Pilipenko, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 */

var validMapping = false;

function DAO() {
    if(!validMapping) {
        validateMapping();
        validMapping = true;
    }
    var uid = new Date().toISOString(), result;
    if(localStorage) {
        localStorage.setItem(uid, uid);
        result = localStorage.getItem(uid) == uid;
        localStorage.removeItem(uid);
        if(!result) {
            throw new Error('Local Storage Problem!');
        }
        this.cache = {};
    } else {
        throw new Error('Local Storage Problem!')
    }
}

function validateMapping() {
    if(MAPPING !== undefined) {
        for (var item in MAPPING) {
            if(MAPPING.hasOwnProperty(item)) {
                var entity = MAPPING[item];
                if (entity === undefined) {
                    throw new Error(item + ' is undefined ');
                }
                var mapping = entity.mapping;
                if (mapping === undefined) {
                    throw new Error(item + ': Mapping is not defined ');
                }
                if(mapping.entity === undefined) {
                    throw new Error(item + ': Entity is not defined');
                }
                if(mapping.entity != item) {
                    throw new Error(item + ': Mapping is not equal');
                }
                if (mapping.table === undefined) {
                    throw new Error(item + ': Table is not defined');
                }
                if (mapping.fields === undefined) {
                    throw new Error(item + ': Fields are not defined');
                }
                var fields = mapping.fields;
                var primaryDefined = false;

                for(var field in fields) {
                    if(fields.hasOwnProperty(field)) {
                        var fieldInfo = fields[field];
                        if (fieldInfo.field === undefined || fieldInfo.field == null) {
                            throw new Error(item + '.' + field + ': field is not defined correctly');
                        }
                        if(fieldInfo.primary) {
                            primaryDefined = true;
                        }
                    }
                }

                if(!primaryDefined) {
                    throw new Error(item + ': primary is not defined');
                }
            }
        }
    } else {
        throw new Error('Mapping is not defined');
    }
}

DAO.getPrimaryKeys = function(fields) {
    var primaryFields = [];
    for(var field in fields) {
        if (fields.hasOwnProperty(field)) {
            var fieldInfo = fields[field];
            if (fieldInfo.primary) {
                primaryFields.push(field);
            }
        }
    }
    return primaryFields;
};

DAO.getEntityId = function(obj, fields) {
    if(obj == null) {
        return null;
    }
    if(fields == undefined) {
        var mapping = obj.constructor.mapping;
        if(mapping === undefined || MAPPING[mapping.entity] === undefined) {
            throw new Error(obj.constructor.name + ' is not mapped');
        }
        fields = mapping.fields;
    }
    var primary = '';
    var emptyKey = true;
    var primaryFields = DAO.getPrimaryKeys(fields);
    for(var i = 0; i < primaryFields.length; i++) {
        var fieldValue = null;
        var fieldInfo = fields[primaryFields[i]];
        switch (fieldInfo.fieldType) {
            case FieldType.Field:
                fieldValue = obj[primaryFields[i]];
                break;
            case FieldType.Date:
                fieldValue = obj[primaryFields[i]] ? obj[primaryFields[i]].toISOString() : null;
                break;
            case FieldType.ForeignKey:
                fieldValue = DAO.getEntityId(obj[primaryFields[i]]);
                break;
            case FieldType.Formula:
                fieldValue = fieldInfo.formula.apply(obj);
                break;
        }
        if(i != 0) {
            primary += '_';
        }
        if(fieldValue) {
            emptyKey = false;
            primary += fieldValue;
        }
    }
    return emptyKey ? null : primary;
};

DAO.prototype._cloneObject = function(obj) {
    var mapping = obj.constructor.mapping;
    if(mapping === undefined || MAPPING[mapping.entity] === undefined) {
        throw new Error(obj.constructor.name + ' is not mapped');
    }
    var entityType = MAPPING[mapping.entity];
    if(entityType === undefined) {
        throw new Error(mapping.entity + ' is not mapped');
    }

    var fields = mapping.fields;
    var collections = mapping.collections;
    var cloneObj = new entityType;
    var dao = this;

    for(var field in fields) {
        if(fields.hasOwnProperty(field)) {
            var fieldInfo = fields[field];
            switch (fieldInfo.fieldType) {
                case FieldType.Field:
                case FieldType.Date:
                    cloneObj[field] = obj[field];
                    break;
                case FieldType.ForeignKey:
                    if(fieldInfo.lazy) {
                        var foreignKey;
                        if(obj[DAO.UNINITIALIZED_PREFIX + field]) {
                            foreignKey = obj[field + DAO._FOREIGN_KEY_SUFFIX];
                        } else {
                            foreignKey = DAO.getEntityId(obj[field]);
                        }
                        cloneObj[DAO.UNINITIALIZED_PREFIX + field] = true;
                        cloneObj[field + DAO._FOREIGN_KEY_SUFFIX] = foreignKey;
                        (function() {
                            var f = field;
                            var e = fieldInfo.entity;
                            var key = foreignKey;
                            Object.defineProperty(cloneObj, f, {
                                get: function () {
                                    var val = dao.load(e, key);
                                    delete this[DAO.UNINITIALIZED_PREFIX + f];
                                    delete this[f + DAO._FOREIGN_KEY_SUFFIX];
                                    Object.defineProperty(this, f, {value: val, enumerable: true, writable:true});
                                    return val;
                                },
                                set: function (val) {
                                    Object.defineProperty(this, f, {value: val, enumerable: true, writable:true});
                                },
                                enumerable: true
                            });
                        })();
                    } else {
                        cloneObj[field] = obj[field];
                    }
                    break;
            }
        }
    }

    if(collections !== undefined) {
        var primaryKey = DAO.getEntityId(obj, fields);
        for (var collection in collections) {
            if (collections.hasOwnProperty(collection)) {
                var collectionInfo = collections[collection];
                if(collectionInfo.lazy) {
                    cloneObj[DAO.UNINITIALIZED_PREFIX + collection] = true;
                    (function() {
                        var c = collection;
                        var e = collectionInfo.entity;
                        var key = collectionInfo.key;
                        Object.defineProperty(cloneObj, c, {
                            get: function () {
                                var val = dao.loadCollection(e, key, primaryKey);
                                delete this[DAO.UNINITIALIZED_PREFIX + c];
                                Object.defineProperty(this, c, {value: val, enumerable: true, writable:true});
                                return val;
                            },
                            set: function (val) {
                                Object.defineProperty(this, c, {value: val, enumerable: true, writable:true});
                            },
                            enumerable: true
                        });
                    })();
                } else {
                    cloneObj[collection] = obj[collection];
                }
            }
        }
    }
    return cloneObj;
};

DAO.prototype._addToCache = function(table, primaryKey, obj) {
    if(this.cache[table] === undefined) {
        this.cache[table] = {};
    }
    var tableCache = this.cache[table];
    tableCache[primaryKey] = this._cloneObject(obj);
};

DAO.prototype._loadFromCache = function(table, primaryKey, entityType) {
    if(this.cache[table] !== undefined) {
        var tableCache = this.cache[table];
        var entity = tableCache[primaryKey];
        return (entity && instanceOfClass(entity,entityType)) ? this._cloneObject(tableCache[primaryKey]) : null;
    }
};

DAO.prototype._removeFromCache = function(table, primaryKey) {
    var tableCache = this.cache[table];
    delete tableCache[primaryKey];
};

DAO.prototype._clearCache = function() {
    this.cache = {};
};

DAO._getIndexName = function(index, table, field) {
    return index || table + '_' + field;
};

DAO.UNINITIALIZED_PREFIX = 'Uninitialized_';
DAO._FOREIGN_KEY_SUFFIX = '_FKID';

DAO.prototype._insertIndex = function(key, value) {
    var indexStorage = JSON.parse(localStorage.getItem(key) || '[]');
    insertIndex(indexStorage, value);
    localStorage.setItem(key, JSON.stringify(indexStorage));
};

DAO.prototype._removeIndex = function(key, value) {
    var indexStorage = JSON.parse(localStorage.getItem(key) || '[]');
    removeIndex(indexStorage, value);
    localStorage.setItem(key, JSON.stringify(indexStorage));
};

DAO.prototype.getChildren = function(entity, keyField, primaryKey) {
    entity = typeof entity == "function" ? entity : MAPPING[entity];
    var mapping = entity ? entity.mapping : undefined;
    if(mapping === undefined || MAPPING[mapping.entity] === undefined) {
        throw new Error(entity.constructor.name + ' is not mapped');
    }
    var table = mapping.table;
    var fields = mapping.fields;
    var fieldInfo = fields[keyField];
    var index = null;
    if(fieldInfo.fieldType == FieldType.ForeignKey || fieldInfo.index !== undefined) {
        index = DAO._getIndexName(fieldInfo.index, table, fieldInfo.field);
    }
    var ids = [];
    if(index != null) {
        ids = JSON.parse(localStorage.getItem(index + '_' + primaryKey) || '[]');
    } else {
        var filterObject = {};
        filterObject[keyField + '.id'] = primaryKey;
        var children = this.filter(entity, filterObject, DAO.getPrimaryKeys(fields));
        for(var i = 0; i < children.length; i++ ) {
            var child = children[i];
            var primary = '';
            for(var prop in child) {
                if(child.hasOwnProperty(prop)) {
                    if(primary != '') {
                        primary += '_';
                    }
                    if(child[prop]) {
                        primary += child[prop];
                    }
                }
            }
            ids.push(primary);
        }
    }
    return ids;
};

DAO.prototype.loadCollection = function(entity, keyField, primaryKey) {
    var ids = this.getChildren(entity, keyField, primaryKey);
    var collection = [];
    for(var i = 0; i < ids.length; i++) {
        var item = this.load(entity, ids[i]);
        if(item) {
            collection.push(item);
        }
    }
    return collection;
};

DAO.prototype.store = function(obj, currentTransaction) {
    var transaction = currentTransaction || [];
    var mapping = obj.constructor.mapping;
    if(mapping === undefined || MAPPING[mapping.entity] === undefined) {
        throw new Error(obj.constructor.name + ' is not mapped');
    }

    var table = mapping.table;
    var fields = mapping.fields;
    var collections = mapping.collections;
    var indexes = [];
    var dbEntity = {'entity' : mapping.entity};

    var primaryKey = DAO.getEntityId(obj, fields);
    if(primaryKey == null && mapping.generator !== undefined) {
        mapping.generator.apply(obj);
        primaryKey = DAO.getEntityId(obj, fields);
    }

    if(primaryKey == null) {
        throw new Error(obj.constructor.name + ': primary is null');
    }

    for(var field in fields) {
        if(fields.hasOwnProperty(field)) {
            var fieldInfo = fields[field];
            switch (fieldInfo.fieldType) {
                case FieldType.Field:
                    dbEntity[fieldInfo.field] = obj[field];
                    if (fieldInfo.index !== undefined) {
                        indexes.push(field);
                    }
                    break;
                case FieldType.Date:
                    dbEntity[fieldInfo.field] = obj[field] ? obj[field].toISOString() : null;
                    if (fieldInfo.index !== undefined) {
                        indexes.push(field);
                    }
                    break;
                case FieldType.ForeignKey:
                    if(!fieldInfo.lazy || !obj[DAO.UNINITIALIZED_PREFIX + field]) {
                        dbEntity[fieldInfo.field] = DAO.getEntityId(obj[field]);
                        indexes.push(field);
                        switch (fieldInfo.cascade) {
                            case Cascade.All:
                                this.store(obj[field], transaction);
                                break;
                        }
                    }
                    break;
                case FieldType.Formula:
                    dbEntity[fieldInfo.field] = fieldInfo.formula.apply(obj);
                    if (fieldInfo.index !== undefined) {
                        indexes.push(field);
                    }
                    break;
            }
        }
    }

    transaction.push(function() {
        this._insertIndex(table, primaryKey);
        var storedObj = JSON.parse(localStorage.getItem(table + '-' + primaryKey) || '{}');
        for(var i = 0; i < indexes.length; i++) {
            var fieldInfo = fields[indexes[i]];
            var prevIndex = storedObj ? storedObj[fieldInfo.field] : null;
            var index = dbEntity[fieldInfo.field] + '';
            var indexName = DAO._getIndexName(fieldInfo.index, table, fieldInfo.field);
            this._removeIndex(indexName + '_' + prevIndex, primaryKey);
            this._insertIndex(indexName, index);
            this._insertIndex(indexName + '_' + index, primaryKey);
        }

        localStorage.setItem(table + '-' + primaryKey, JSON.stringify(dbEntity));
    });

    if(collections !== undefined) {
        for(var collection in collections) {
            if(collections.hasOwnProperty(collection)) {
                var collectionInfo = collections[collection];
                if(!collectionInfo.lazy || !obj[DAO.UNINITIALIZED_PREFIX + collection]) {
                    var items = obj[collection];
                    if(items) {
                        switch (collectionInfo.cascade) {
                            case Cascade.All:
                                for (i = 0; i < items.length; i++) {
                                    this.store(items[i], transaction);
                                }
                                break;
                        }
                    }
                }
            }
        }
    }

    if(!currentTransaction) {
        try {
            for (var i = 0; i < transaction.length; i++) {
                var operation = transaction[i];
                operation.apply(this);
            }
        } catch(e) {
            if (e == QUOTA_EXCEEDED_ERR) {
                //TODO: localStorage Limit quota exceeded
            }
            //TODO: Transaction behaviour
        }
    }
    this._addToCache(table, primaryKey, obj);
};

DAO.prototype.load = function(entity, id) {
    entity = typeof entity == "function" ? entity : MAPPING[entity];
    var mapping = entity ? entity.mapping : undefined;
    if(mapping === undefined || MAPPING[mapping.entity] === undefined) {
        throw new Error(entity.constructor.name + ' is not mapped');
    }

    var table = mapping.table;
    var cachedObj = this._loadFromCache(table, id, entity);
    if(cachedObj != null) {
        return cachedObj;
    }
    var objStr = localStorage.getItem(table + '-' + id);
    if(objStr) {
        var dbEntity = JSON.parse(objStr);
        var entityType = MAPPING[dbEntity.entity];
        if(entityType === undefined) {
            throw new Error(dbEntity.entity + ' is not mapped');
        }
        if(!extensionOf(entityType, entity)) {
            return null;
        }
        mapping = entityType.mapping;

        var fields = mapping.fields;
        var collections = mapping.collections;
        var obj = new entityType;
        var dao = this;
        for(var field in fields) {
            if(fields.hasOwnProperty(field)) {
                var fieldInfo = fields[field];
                switch (fieldInfo.fieldType) {
                    case FieldType.Field:
                        obj[field] = dbEntity[fieldInfo.field];
                        break;
                    case FieldType.Date:
                        obj[field] = dbEntity[fieldInfo.field] ? new Date(dbEntity[fieldInfo.field]) : null;
                        break;
                    case FieldType.ForeignKey:
                        if(fieldInfo.lazy) {
                            obj[DAO.UNINITIALIZED_PREFIX + field] = true;
                            obj[field + DAO._FOREIGN_KEY_SUFFIX] = dbEntity[fieldInfo.field];
                            (function() {
                                var f = field;
                                var e = fieldInfo.entity;
                                var key = dbEntity[fieldInfo.field];
                                Object.defineProperty(obj, f, {
                                    get: function () {
                                        var val = dao.load(e, key);
                                        delete this[DAO.UNINITIALIZED_PREFIX + f];
                                        delete this[f + DAO._FOREIGN_KEY_SUFFIX];
                                        Object.defineProperty(this, f, {value: val, enumerable: true, writable:true});
                                        return val;
                                    },
                                    set: function (val) {
                                        Object.defineProperty(this, f, {value: val, enumerable: true, writable:true});
                                    },
                                    enumerable: true
                                });
                            })();
                        } else {
                            obj[field] = this.load(fieldInfo.entity, dbEntity[fieldInfo.field]);
                        }
                        break;
                }
            }
        }
        if(collections !== undefined) {
            var primaryKey = DAO.getEntityId(obj, fields);
            for (var collection in collections) {
                if (collections.hasOwnProperty(collection)) {
                    var collectionInfo = collections[collection];
                    if(collectionInfo.lazy) {
                        obj[DAO.UNINITIALIZED_PREFIX + collection] = true;
                        (function() {
                            var c = collection;
                            var e = collectionInfo.entity;
                            var key = collectionInfo.key;
                            Object.defineProperty(obj, c, {
                                get: function () {
                                    var val = dao.loadCollection(e, key, primaryKey);
                                    delete this[DAO.UNINITIALIZED_PREFIX + c];
                                    Object.defineProperty(this, c, {value: val, enumerable: true, writable:true});
                                    return val;
                                },
                                set: function (val) {
                                    Object.defineProperty(this, c, {value: val, enumerable: true, writable:true});
                                },
                                enumerable: true
                            });
                        })();
                    } else {
                        obj[collection] = this.loadCollection(collectionInfo.entity, collectionInfo.key, primaryKey);
                    }
                }
            }
        }
        this._addToCache(table, id, obj);
        return obj;
    } else {
        return null;
    }
};

DAO.prototype.remove = function(entity, id, currentTransaction) {
    var transaction = currentTransaction || [];
    entity = typeof entity == "function" ? entity : MAPPING[entity];
    var mapping = entity ? entity.mapping : undefined;
    if(mapping === undefined || MAPPING[mapping.entity] === undefined) {
        throw new Error(entity.constructor.name + ' is not mapped');
    }

    var table = mapping.table;
    var objStr = localStorage.getItem(table + '-' + id);
    if(objStr) {
        var dbEntity = JSON.parse(objStr);
        var entityType = MAPPING[dbEntity.entity];
        if (entityType === undefined) {
            throw new Error(dbEntity.entity + ' is not mapped');
        }
        if (!extensionOf(entityType, entity)) {
            return;
        }
        mapping = entityType.mapping;

        var fields = mapping.fields;
        var collections = mapping.collections;
        var indexes = [];
        for(var field in fields) {
            if(fields.hasOwnProperty(field)) {
                var fieldInfo = fields[field];
                if(fieldInfo.fieldType == FieldType.ForeignKey) {
                    switch (fieldInfo.cascade) {
                        case Cascade.All:
                            this.remove(fieldInfo.entity, dbEntity[fieldInfo.field], transaction);
                            break;
                    }
                }
                if(fieldInfo.fieldType == FieldType.ForeignKey || fieldInfo.index !== undefined) {
                    indexes.push(field);
                }
            }
        }
        if(collections !== undefined) {
            for (var collection in collections) {
                if (collections.hasOwnProperty(collection)) {
                    var collectionInfo = collections[collection];
                    switch (collectionInfo.cascade) {
                        case Cascade.All:
                            var ids = this.getChildren(collectionInfo.entity, collectionInfo.key, id);
                            for(var i = 0; i < ids.length; i++) {
                                this.remove(collectionInfo.entity, ids[i], transaction);
                            }
                            break;
                        case Cascade.Set_Null:
                            var ids = this.getChildren(collectionInfo.entity, collectionInfo.key, id);
                            for(var i = 0; i < ids.length; i++) {
                                var child = this.load(collectionInfo.entity, ids[i]);
                                child[collectionInfo.key] = null;
                                this.store(child, transaction);
                            }
                            break;
                    }
                }
            }
        }
        transaction.push(function() {
            this._removeIndex(table, id);
            localStorage.removeItem(table + '-' + id);
            for(var i = 0; i < indexes.length; i++) {
                var fieldInfo = fields[indexes[i]];
                var index = dbEntity[fieldInfo.field];
                var indexName = DAO._getIndexName(fieldInfo.index, table, fieldInfo.field);
                this._removeIndex(indexName + '_' + index, id);
            }
        });

        if(!currentTransaction) {
            try {
                for (var j = 0; j < transaction.length; j++) {
                    var operation = transaction[j];
                    operation.apply(this);
                }
            } catch(e) {
                //TODO: Transaction behaviour
            }
        }
        this._removeFromCache(table, id);
    }
};

DAO.prototype.all = function(entity) {
    entity = typeof entity == "function" ? entity : MAPPING[entity];
    var mapping = entity ? entity.mapping : undefined;
    if(mapping === undefined || MAPPING[mapping.entity] === undefined) {
        throw new Error(entity.constructor.name + ' is not mapped');
    }
    var indexStorage = JSON.parse(localStorage.getItem(mapping.table) || '[]');
    var items = [];
    for(var i = 0; i < indexStorage.length; i++) {
        var item = this.load(entity, indexStorage[i]);
        if(item) {
            items.push(item);
        }
    }
    return items;
};

DAO.prototype.filter = function(entity, conditions, selectValues, order) {
    entity = typeof entity == "function" ? entity : MAPPING[entity];
    var mapping = entity ? entity.mapping : undefined;
    if(mapping === undefined || MAPPING[mapping.entity] === undefined) {
        throw new Error(entity.constructor.name + ' is not mapped');
    }
    var fields = mapping.fields;
    var table = mapping.table;
    var field, fieldInfo;
    var ids = JSON.parse(localStorage.getItem(table) || '[]');
    var i, j, k;
    var entities = [];
    var item;
    if(conditions !== undefined) {
        var indexed = [];
        var notIndexed = [];
        var complexConditions = null;
        var condition;
        var flag;
        for(field in conditions) {
            if(conditions.hasOwnProperty(field)) {
                if(field != 'complexConditions') {
                    if(field.endsWith(".id")) {
                        var foreignKey = field.substring(0, field.length - 3);
                        fieldInfo = fields[foreignKey];
                        if(fieldInfo.fieldType == FieldType.ForeignKey) {
                            indexed.push(field);
                        }
                    } else {
                        fieldInfo = fields[field];
                        if(fieldInfo.fieldType != FieldType.ForeignKey && fieldInfo.index !== undefined) {
                            indexed.push(field);
                        } else {
                            notIndexed.push(field);
                        }
                    }
                } else {
                    complexConditions = conditions['complexConditions'];
                }
            }
        }
        var all, filtered = ids;
        for(i = 0; i < indexed.length && filtered.length > 0; i++) {
            all = filtered;
            filtered = [];
            field = indexed[i];
            condition = conditions[field];
            fieldInfo = fields[field.replace(/\.id$/, "")];
            var indexName = DAO._getIndexName(fieldInfo.index, table, fieldInfo.field);
            var indexStorage = JSON.parse(localStorage.getItem(indexName) || '[]');
            for(j = 0; j < indexStorage.length; j++) {
                var index = indexStorage[j];
                flag = true;
                if(typeof condition == 'function') {
                    flag = condition.call(this, index);
                } else {
                    flag = (condition + '' == index);
                }
                if(flag) {
                    var objectIds = JSON.parse(localStorage.getItem(indexName + '_' + index) || '[]');
                    for(k = 0; k < objectIds.length; k++) {
                        if(searchIndex(all, objectIds[k]) != -1) {
                            insertIndex(filtered, objectIds[k]);
                        }
                    }
                }
            }
        }
        for(i = 0; i < filtered.length; i++) {
            item = this.load(entity, filtered[i]);
            if(item) {
                flag = true;
                for(j = 0; j < notIndexed.length && flag; j++) {
                    condition = conditions[notIndexed[j]];
                    if(typeof condition == 'function') {
                        flag = condition.call(this, item[notIndexed[j]]);
                    } else {
                        flag = (condition == item[notIndexed[j]]);
                    }
                }
                if(complexConditions != null) {
                    for (j = 0; j < complexConditions.length && flag; j++) {
                        condition = complexConditions[j];
                        if (typeof condition == 'function') {
                            flag = condition.call(this, item);
                        }
                    }
                }
                if(flag) {
                    entities.push(item);
                }
            }
        }
    } else {
        for(i = 0; i < ids.length; i++) {
            item = this.load(entity, ids[i]);
            if(item) {
                entities.push(item);
            }
        }
    }

    var result = [];
    if(selectValues !== undefined) {
        for(i = 0; i < entities.length; i++) {
            item = entities[i];
            var obj = {};
            for(field in selectValues) {
                if(selectValues.hasOwnProperty(field)) {
                    var selectOpt = selectValues[field];
                    if(typeof selectOpt == 'function') {
                        obj[field] = selectOpt.call(this, item);
                    } else {
                        obj[field] = item[selectOpt];
                    }
                }
            }
            result.push(obj);
        }
    } else {
        result = entities;
    }
    if(order !== undefined) {
        for(i = 0; i < order.length; i++) {
            var comparator = order[i];
            if(typeof comparator == 'function') {
                result.sort(comparator);
            } else {
                result.sort(dynamicSort(comparator));
            }
        }
    }
    return result;
};

DAO.prototype.cleanAll = function() {
    localStorage.clear();
};

DAO.prototype.initializeProperty = function(obj, prop) {
    var temp = obj[prop];
    return temp;
}

DAO.prototype.initializeAllProperties = function(obj) {
    if(obj) {
        for (var field in obj) {
            if (obj.hasOwnProperty(field) && field.indexOf(DAO.UNINITIALIZED_PREFIX) == 0) {
                var fName = field.substr(DAO.UNINITIALIZED_PREFIX.length);
                var temp = obj[fName];
            }
        }
    }
}
/*
 * Excellent - Student Organizer
 * Copyright (c) 2014, Sergey Pilipenko, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 */

function searchIndex(arr, value) {
    var minIndex = 0;
    var maxIndex = arr.length - 1;
    var middle;
    var curElem;

    while (minIndex <= maxIndex) {
        middle = (minIndex + maxIndex) / 2 | 0;
        curElem = arr[middle];

        if (curElem < value) {
            minIndex = middle + 1;
        } else if (curElem > value) {
            maxIndex = middle - 1;
        } else {
            return middle;
        }
    }

    return -1;
}

function insertIndex(arr, value) {
    var minIndex = 0;
    var maxIndex = arr.length - 1;
    var middle;
    var curElem;

    while (minIndex <= maxIndex) {
        middle = (minIndex + maxIndex) / 2 | 0;
        curElem = arr[middle];

        if (curElem < value) {
            minIndex = middle + 1;
        } else if (curElem > value) {
            maxIndex = middle - 1;
        } else {
            return;
        }
    }
    arr.splice(minIndex, 0, value);
}

function removeIndex(arr, value) {
    var index = searchIndex(arr, value);
    if(index != -1) {
        arr.splice(index, 1);
    }
}
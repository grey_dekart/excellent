DELETE FROM actions;
ALTER TABLE actions CHANGE COLUMN endTime endTime DATETIME NULL,
CHANGE COLUMN startTime startTime DATETIME NULL DEFAULT NULL,
CHANGE COLUMN deadline deadline DATETIME NULL DEFAULT NULL;

DELETE FROM marks;
ALTER TABLE marks CHANGE COLUMN dateField dateField DATETIME NULL DEFAULT NULL;

ALTER TABLE activities ADD COLUMN deleted BIT(1) NOT NULL, ADD COLUMN synchronizationDate datetime;
ALTER TABLE lecturers ADD COLUMN deleted BIT(1) NOT NULL, ADD COLUMN synchronizationDate datetime;
ALTER TABLE marks ADD COLUMN deleted BIT(1) NOT NULL, ADD COLUMN synchronizationDate datetime;
ALTER TABLE places ADD COLUMN deleted BIT(1) NOT NULL, ADD COLUMN synchronizationDate datetime;
ALTER TABLE actions ADD COLUMN deleted BIT(1) NOT NULL, ADD COLUMN synchronizationDate datetime;
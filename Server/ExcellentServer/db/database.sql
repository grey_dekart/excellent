create table users (
	id varchar(255) not null,
	birthday date,
	email varchar(255),
	login varchar(255),
	enabled bit not null,
	passwordHash varchar(255),
	primary key (id)
);

create table activities (
	activityType varchar(31) not null,
	id varchar(255) not null,
	updated datetime,
        deleted bit not null,
        synchronizationDate datetime,
	description varchar(1000),
	name varchar(255),
	evaluationType integer,
	userId varchar(255) not null,
	primary key (id, userId),
	constraint FK_ACTIVITIES_USER foreign key (userId) references users (id) on delete cascade
);

create table lecturers (
	id varchar(255) not null,
	updated datetime,
        deleted bit not null,
        synchronizationDate datetime,
	additionalInformation varchar(1000),
	difficulty integer,
	email varchar(255),
	name varchar(255),
	phone varchar(255),
	position varchar(255),
	userId varchar(255) not null,
	primary key (id, userId),
	constraint FK_LECTURERS_USER foreign key (userId) references users (id) on delete cascade
);

create table places (
	id varchar(255) not null,
	updated datetime,
        deleted bit not null,
        synchronizationDate datetime,
	address varchar(255),
	latitude double not null,
	longitude double not null,
	name varchar(255),
	userId varchar(255) not null,
	primary key (id, userId),
	constraint FK_PLACES_USER foreign key (userId) references users (id) on delete cascade
);

create table marks (
	id varchar(255) not null,
	updated datetime,
        deleted bit not null,
        synchronizationDate datetime,
	dateField datetime,
	description varchar(255),
	val varchar(255),
	userId varchar(255) not null,
	activity_id varchar(255),
	activity_userId varchar(255),
	primary key (id, userId),
	constraint FK_MARKS_USER foreign key (userId) references users (id) on delete cascade,
	constraint FK_MARKS_ACTIVITY foreign key (activity_id, activity_userId) references activities (id, userId) on delete cascade
);

create table actions (
	entityType varchar(31) not null,
	id varchar(255) not null,
	updated datetime,
        deleted bit not null,
        synchronizationDate datetime,
	description varchar(1000),
	name varchar(255),
	priority integer,
	showOnDisplay bit not null,
	withNotification bit not null,
	endTime datetime,
	startTime datetime,
	completed bit,
	deadline datetime,
	difficulty integer,
	userId varchar(255) not null,
	activity_id varchar(255),
	activity_userId varchar(255),
	lecturer_id varchar(255),
	lecturer_userId varchar(255),
	place_id varchar(255),
	place_userId varchar(255),
	placeId varchar(255),
	primary key (id, userId),
	constraint FK_ACTIONS_USER foreign key (userId) references users (id) on delete cascade,
	constraint FK_ACTIONS_ACTIVITY foreign key (activity_id, activity_userId) references activities (id, userId) on delete cascade,
	constraint FK_ACTIONS_LECTURER foreign key (lecturer_id, lecturer_userId) references lecturers (id, userId)  on delete set null,
	constraint FK_EVENTS_PLACE foreign key (place_id, place_userId) references places (id, userId) on delete set null
);


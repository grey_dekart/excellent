/*
 * Excellent - Student Organizer
 * Copyright (c) 2014, Sergey Pilipenko, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 */
package excellent.server.handlers;

import excellent.models.User;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

@Component
public class AuthorizeRequestInterceptor extends HandlerInterceptorAdapter {
    
    private static final Logger logger = LogManager.getLogger(AuthorizeRequestInterceptor.class);

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        if (handler instanceof HandlerMethod) {
            HandlerMethod handlerMethod = (HandlerMethod) handler;
            AuthorizeRequest ann = handlerMethod.getMethodAnnotation(AuthorizeRequest.class);
            if(ann != null) {
                HttpSession session = request.getSession();
                User user = (User)session.getAttribute("user");
                if(user != null && user.isEnabled()) {
                    return true;
                } else {
                    try {
                        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                        response.setContentType("application/json");
                        PrintWriter writer = response.getWriter();
                        writer.print("{ \"status\": \"Error\", \"message\": \"Unauthorized.\" }");
                        writer.flush();
                    } catch(IOException ex) {
                        logger.error(ex.getMessage(), ex);
                    }
                    return false;
                }
            }
        }
        return true;
    }
}

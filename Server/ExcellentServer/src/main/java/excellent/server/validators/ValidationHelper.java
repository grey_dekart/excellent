/*
 * Excellent - Student Organizer
 * Copyright (c) 2014, Sergey Pilipenko, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 */
package excellent.server.validators;

import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.FieldError;

public class ValidationHelper {
    
    public static String getErrorMessage(BeanPropertyBindingResult result) {
        StringBuilder errorMessage = new StringBuilder();
        for(FieldError error:result.getFieldErrors()) {
            errorMessage.append(String.format("*%s\n", error.getDefaultMessage()));
        }
        return errorMessage.toString();
    }
}

/*
 * Excellent - Student Organizer
 * Copyright (c) 2014, Sergey Pilipenko, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 */
package excellent.server.validators;

import excellent.server.requests.RegistrationRequest;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

public class RegistrationRequestValidator implements Validator {

    public boolean supports(Class type) {
        return RegistrationRequest.class.isAssignableFrom(type);
    }

    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "login", "field.required", "Login is required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "field.required", "Password is required");
        RegistrationRequest registrationRequest = (RegistrationRequest)o;
        if(!registrationRequest.getPassword().equals(registrationRequest.getConfirmPassword())) {
             errors.rejectValue("password", "field.mismatch", "Passwords must match.");
        }
        if(!EmailValidator.validate(registrationRequest.getEmail())) {
             errors.rejectValue("email", "field.invalid", "Invalid email address");
        }
    }
    
}

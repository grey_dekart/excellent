/*
 * Excellent - Student Organizer
 * Copyright (c) 2014, Sergey Pilipenko, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 */
package excellent.server.controllers;

import excellent.models.User;
import excellent.server.handlers.AuthorizeRequest;
import excellent.server.responses.JSONResponse;
import excellent.server.responses.UserDetails;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/auth")
public class AuthController {
    
    @AuthorizeRequest
    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody JSONResponse processRegistration(HttpSession session) {
        User user = (User)session.getAttribute("user");
        return new UserDetails(user);
    }
}

/*
 * Excellent - Student Organizer
 * Copyright (c) 2014, Sergey Pilipenko, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 */
package excellent.server.controllers;

import excellent.models.BasicEntity;
import excellent.models.BasicEntityComparator;
import excellent.models.BasicEntityPK;
import excellent.models.User;
import excellent.server.handlers.AuthorizeRequest;
import excellent.server.requests.SynchronizationRequest;
import excellent.server.responses.JSONResponse;
import excellent.server.responses.StatusMessage;
import excellent.services.BasicEntityService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import javax.servlet.http.HttpSession;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
@RequestMapping("/sync")
public class SyncController {
    
    @Autowired
    private BasicEntityService basicEntityService;
    
    @AuthorizeRequest
    @RequestMapping(method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public @ResponseBody BasicEntity[] synchronize(@RequestBody SynchronizationRequest syncRequest, 
            HttpSession session) throws IllegalAccessException {
        User user = (User)session.getAttribute("user");
        BasicEntity[] entities = syncRequest.getEntities();
        if(entities != null) {
            Date syncDate = new Date();
            Arrays.sort(entities, new BasicEntityComparator());
            for(BasicEntity entity:entities) {
                if(!user.equals(entity.getUserField())) {
                    throw new IllegalAccessException("Entity is not owned by user");
                }
                entity.postDeserialize();
                BasicEntityPK primaryKey = new BasicEntityPK();
                primaryKey.setId(entity.getId());
                primaryKey.setUserField(user);
                BasicEntity dbEntity = basicEntityService.load(entity.getClass(), primaryKey);
                if(dbEntity == null) {
                    entity.setSynchronizationDate(syncDate);
                    basicEntityService.store(entity);
                } else if(entity.getUpdated().compareTo(dbEntity.getUpdated()) >= 0) {
                    dbEntity.update(entity);
                    dbEntity.setSynchronizationDate(syncDate);
                    basicEntityService.update(dbEntity);
                }
            }
        }
        ArrayList<BasicEntity> unsynchronizedItems = new ArrayList<>();
        BasicEntityComparator.typeOrder.stream().forEach((clazz) -> {
            unsynchronizedItems.addAll(basicEntityService.getChangedUserEntities(clazz, user, syncRequest.getLastUpdate()));
        });
        BasicEntity[] responseItems = new BasicEntity[unsynchronizedItems.size()];
        return unsynchronizedItems.toArray(responseItems);
    }
    
    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ExceptionHandler(IllegalAccessException.class)
    public @ResponseBody JSONResponse handleForbiddenAction() {
        return new StatusMessage("Error", "You do not have permission to perform this operation");
    }
    
    @ResponseStatus(HttpStatus.CONFLICT)
    @ExceptionHandler(HibernateException.class)
    public @ResponseBody JSONResponse handleDatabaseException(Exception exception) {
        return new StatusMessage("Error", "Sync data is incorrect, please check it carefully");
    }
}

/*
 * Excellent - Student Organizer
 * Copyright (c) 2014, Sergey Pilipenko, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 */
package excellent.services;

import excellent.dao.UserDAO;
import excellent.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDAO userDAO;

    public UserServiceImpl() { }

    public UserServiceImpl(UserDAO userDAO) {
        this.userDAO = userDAO;
    }
    
    public User load(String id) {
        return userDAO.load(id);
    }

    @Transactional(readOnly = false)
    public void remove(User user) {
        userDAO.remove(user);
    }

    @Transactional(readOnly = false)
    public void store(User user) {
        userDAO.store(user);
    }

    public User getUserByName(String name) {
        return userDAO.getUserByName(name);
    }
}

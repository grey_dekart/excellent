/*
 * Excellent - Student Organizer
 * Copyright (c) 2014, Sergey Pilipenko, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 */
package excellent.services;

import excellent.dao.BasicEntityDAO;
import excellent.models.BasicEntity;
import excellent.models.BasicEntityPK;
import excellent.models.User;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class BasicEntityServiceImpl implements BasicEntityService {

    @Autowired
    private BasicEntityDAO basicEntityDAO;

    public BasicEntityServiceImpl() {
    }

    public BasicEntityServiceImpl(BasicEntityDAO basicEntityDAO) {
        this.basicEntityDAO = basicEntityDAO;
    }
    
    @Override
    public <T extends BasicEntity> T load(Class<T> entityClass, BasicEntityPK id) {
        return basicEntityDAO.load(entityClass, id);
    }

    @Override
    @Transactional(readOnly = false)
    public void remove(BasicEntity entity) {
        basicEntityDAO.remove(entity);
    }

    @Override
    @Transactional(readOnly = false)
    public void store(BasicEntity entity) {
        basicEntityDAO.store(entity);
    }
    
    @Override
    @Transactional(readOnly = false)
    public void update(BasicEntity entity) {
        basicEntityDAO.update(entity);
    }

    @Override
    public <T extends BasicEntity> List<T> getUserEntities(Class<T> entityClass, User user) {
        return basicEntityDAO.getUserEntities(entityClass, user);
    }
    
    @Override
    public <T extends BasicEntity> List<T> getChangedUserEntities(Class<T> entityClass, 
            User user, Date updateTime) {
        return basicEntityDAO.getChangedUserEntities(entityClass, user, updateTime);
    }
}

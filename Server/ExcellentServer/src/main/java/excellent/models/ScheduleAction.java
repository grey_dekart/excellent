/*
 * Excellent - Student Organizer
 * Copyright (c) 2014, Sergey Pilipenko, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 */
package excellent.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import excellent.models.enums.Priority;
import excellent.utils.ActivityDeserializer;
import excellent.utils.BasicEntitySerializer;
import excellent.utils.LecturerDeserializer;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Table(name="actions")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn( name = "entityType", discriminatorType = DiscriminatorType.STRING)
@DiscriminatorValue("action")
public class ScheduleAction extends BasicEntity {
    
    @ManyToOne
    @JsonProperty(value="lecturerId")
    @JsonSerialize(using = BasicEntitySerializer.class)
    @JsonDeserialize(using = LecturerDeserializer.class)
    protected Lecturer lecturer;
    
    @ManyToOne
    @OnDelete(action=OnDeleteAction.CASCADE)
    @JsonProperty(value="activityId")
    @JsonSerialize(using = BasicEntitySerializer.class)
    @JsonDeserialize(using = ActivityDeserializer.class)
    protected Activity activity;
    
    protected String name;
    protected boolean withNotification;
    protected boolean showOnDisplay;
    
    @Column(length=1000)
    protected String description;
    
    protected Priority priority;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isWithNotification() {
        return withNotification;
    }

    public void setWithNotification(boolean withNotification) {
        this.withNotification = withNotification;
    }

    public boolean isShowOnDisplay() {
        return showOnDisplay;
    }

    public void setShowOnDisplay(boolean showOnDisplay) {
        this.showOnDisplay = showOnDisplay;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public int getPriority() {
        return priority.getType();
    }

    public void setPriority(int priority) {
        this.priority = Priority.toPriority(priority);
    }

    public Priority getPriorityEnum() {
        return priority;
    }

    public void setPriorityEnum(Priority priority) {
        this.priority = priority;
    }

    public Lecturer getLecturer() {
        return lecturer;
    }

    public void setLecturer(Lecturer lecturer) {
        this.lecturer = lecturer;
    }

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }
    
    @Override
    public void postDeserialize() {
        super.postDeserialize();
        if(lecturer != null) {
            lecturer.setUserField(userField);
        }
        if(activity != null) {
            activity.setUserField(userField);
        }
    }

    @Override
    public void update(BasicEntity entity) {
        if(entity instanceof ScheduleAction) {
            ScheduleAction action = (ScheduleAction)entity;
            super.update(action);
            this.lecturer = action.lecturer;
            this.activity = action.activity;
            this.name = action.name;
            this.withNotification = action.withNotification;
            this.showOnDisplay = action.showOnDisplay;
            this.description = action.description;
            this.priority = action.priority;
        }
    }
}

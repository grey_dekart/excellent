/*
 * Excellent - Student Organizer
 * Copyright (c) 2014, Sergey Pilipenko, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 */
package excellent.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import excellent.utils.UserDeserializer;
import excellent.utils.UserSerializer;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@JsonTypeInfo(
    use = JsonTypeInfo.Id.NAME,
    include = JsonTypeInfo.As.PROPERTY,
    property = "type")
@JsonSubTypes({
    @JsonSubTypes.Type(value = Activity.class, name = "Activity"),
    @JsonSubTypes.Type(value = Subject.class, name = "Subject"),
    @JsonSubTypes.Type(value = Lecturer.class, name = "Lecturer"),
    @JsonSubTypes.Type(value = Place.class, name = "Place"),
    @JsonSubTypes.Type(value = ScheduleAction.class, name = "ScheduleAction"),
    @JsonSubTypes.Type(value = Task.class, name = "Task"),
    @JsonSubTypes.Type(value = Event.class, name = "Event"),
    @JsonSubTypes.Type(value = Mark.class, name = "Mark")})
@MappedSuperclass
@IdClass(BasicEntityPK.class)
public class BasicEntity implements Serializable {
    
    @Id
    protected String id;
    
    @Id
    @ManyToOne
    @JoinColumn(name = "userId")
    @JsonProperty(value="userId")
    @JsonSerialize(using = UserSerializer.class)
    @JsonDeserialize(using = UserDeserializer.class)
    protected User userField;
    
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    protected Date updated;
    
    @JsonIgnore
    @Temporal(TemporalType.TIMESTAMP)
    protected Date synchronizationDate;
    
    protected boolean deleted;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public User getUserField() {
        return userField;
    }

    public void setUserField(User userField) {
        this.userField = userField;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Date getSynchronizationDate() {
        return synchronizationDate;
    }

    public void setSynchronizationDate(Date synchronizationDate) {
        this.synchronizationDate = synchronizationDate;
    }
    
    public void postDeserialize() { }
    
    public void update(BasicEntity entity) {
        this.deleted = entity.deleted;
        this.updated = entity.updated;
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + (this.id != null ? this.id.hashCode() : 0);
        hash = 67 * hash + (this.userField != null ? this.userField.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BasicEntity other = (BasicEntity) obj;
        if ((this.id == null) ? (other.id != null) : !this.id.equals(other.id)) {
            return false;
        }
        if (this.userField != other.userField && (this.userField == null || !this.userField.equals(other.userField))) {
            return false;
        }
        return true;
    }
}

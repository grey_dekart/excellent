/*
 * Excellent - Student Organizer
 * Copyright (c) 2014, Sergey Pilipenko, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 */
package excellent.models.enums;

public enum EvaluationType {
    NONE(0), EXAM(1), TEST(2);
    
    private final int type;

    private EvaluationType(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }
    
    public static EvaluationType toEvaluationType(int type) {
        EvaluationType result = NONE;
        for(EvaluationType item:EvaluationType.values()) {
            if(item.getType() == type) {
                result = item;
                break;
            }
        }
        return result;
    }
}

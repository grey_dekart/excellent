/*
 * Excellent - Student Organizer
 * Copyright (c) 2014, Sergey Pilipenko, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 */
package excellent.models;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class BasicEntityComparator implements Comparator<BasicEntity>{

    public final static List<Class> typeOrder;
    
    static {
        typeOrder = new ArrayList<>();
        typeOrder.add(Activity.class);
        typeOrder.add(Lecturer.class);
        typeOrder.add(Place.class);
        typeOrder.add(ScheduleAction.class);
        typeOrder.add(Mark.class);
    }
    
    private int getIndex(Class clazz) {
        int index = 0;
        for(Class cls : typeOrder) {
            if(cls.isAssignableFrom(clazz)) {
                break;
            }
            index++;    
        }
        return index;
    }
    
    @Override
    public int compare(BasicEntity o1, BasicEntity o2) {
        int val1 = getIndex(o1.getClass());
        int val2 = getIndex(o2.getClass());
        return (val1 - val2);
    }
}

/*
 * Excellent - Student Organizer
 * Copyright (c) 2014, Sergey Pilipenko, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 */
package excellent.models;

import excellent.models.enums.EvaluationType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("subject")
public class Subject extends Activity {

    @Column(table="activities")
    protected EvaluationType evaluationType;

    public int getEvaluationType() {
        return evaluationType.getType();
    }

    public void setEvaluationType(int evaluationType) {
        this.evaluationType = EvaluationType.toEvaluationType(evaluationType);
    }
    public EvaluationType getEvaluationTypeEnum() {
        return evaluationType;
    }

    public void setEvaluationTypeEnum(EvaluationType evaluationType) {
        this.evaluationType = evaluationType;
    }
    
    @Override
    public void update(BasicEntity entity) {
        if(entity instanceof Subject) {
            Subject subject = (Subject)entity;
            super.update(subject);
            this.evaluationType = subject.evaluationType;
        }
    }
}

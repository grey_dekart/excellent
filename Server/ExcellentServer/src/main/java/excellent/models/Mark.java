/*
 * Excellent - Student Organizer
 * Copyright (c) 2014, Sergey Pilipenko, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 */
package excellent.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import excellent.utils.ActivityDeserializer;
import excellent.utils.BasicEntitySerializer;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Table(name="marks")
public class Mark extends BasicEntity {
    
    @JsonProperty(value="activityId")
    @JsonSerialize(using = BasicEntitySerializer.class)
    @JsonDeserialize(using = ActivityDeserializer.class)
    @ManyToOne
    @OnDelete(action=OnDeleteAction.CASCADE)
    protected Activity activity;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="dateField")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    protected Date date;
    
    @Column(name="val")
    protected String value;
    protected String description;

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    @Override
    public void postDeserialize() {
        super.postDeserialize();
        if(activity != null) {
            activity.setUserField(userField);
        }
    }
    
    @Override
    public void update(BasicEntity entity) {
        if(entity instanceof Mark) {
            Mark mark = (Mark)entity;
            super.update(mark);
            this.activity = mark.activity;
            this.date = mark.date;
            this.description = mark.description;
            this.value = mark.value;
        }
    }
}

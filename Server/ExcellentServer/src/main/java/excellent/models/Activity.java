/*
 * Excellent - Student Organizer
 * Copyright (c) 2014, Sergey Pilipenko, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 */
package excellent.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.LinkedHashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="activities")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn( name = "activityType", discriminatorType = DiscriminatorType.STRING)
@DiscriminatorValue("activity")
public class Activity extends BasicEntity {
    
    protected String name;
    
    @Column(length=1000)
    protected String description;
    
    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, orphanRemoval = true)
    @JoinColumns({@JoinColumn(name="activity_id"),@JoinColumn(name="activity_userId")})
    protected Set<ScheduleAction> actions = new LinkedHashSet<>();
    
    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY, orphanRemoval = true)
    @JoinColumns({@JoinColumn(name="activity_id"),@JoinColumn(name="activity_userId")})
    protected Set<Mark> marks = new LinkedHashSet<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<ScheduleAction> getActions() {
        return actions;
    }

    public void setActions(Set<ScheduleAction> actions) {
        this.actions = actions;
    }

    public Set<Mark> getMarks() {
        return marks;
    }

    public void setMarks(Set<Mark> marks) {
        this.marks = marks;
    }
    
    @Override
    public void update(BasicEntity entity) {
        if(entity instanceof Activity) {
            Activity activity = (Activity)entity;
            super.update(activity);
            this.name = activity.name;
            this.description = activity.description;
        }
    }
}

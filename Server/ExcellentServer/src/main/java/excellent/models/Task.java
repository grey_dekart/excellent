/*
 * Excellent - Student Organizer
 * Copyright (c) 2014, Sergey Pilipenko, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 */
package excellent.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import excellent.models.enums.TaskDifficulty;
import java.util.Date;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@DiscriminatorValue("task")
public class Task extends ScheduleAction {
    
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    protected Date deadline;
    
    protected boolean completed;
    protected TaskDifficulty difficulty;

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public int getDifficulty() {
        return difficulty.getType();
    }

    public void setDifficulty(int difficulty) {
        this.difficulty = TaskDifficulty.toTaskDifficulty(difficulty);
    }
    
    public TaskDifficulty getDifficultyEnum() {
        return difficulty;
    }

    public void setDifficultyEnum(TaskDifficulty difficulty) {
        this.difficulty = difficulty;
    }

    @Override
    public void update(BasicEntity entity) {
        if(entity instanceof Task) {
            Task task = (Task)entity;
            super.update(task);
            this.deadline = task.deadline;
            this.completed = task.completed;
            this.difficulty = task.difficulty;
        }
    }
}

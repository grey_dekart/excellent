/*
 * Excellent - Student Organizer
 * Copyright (c) 2014, Sergey Pilipenko, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 */
package excellent.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import excellent.models.enums.LecturerDifficulty;
import java.util.LinkedHashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="lecturers")
public class Lecturer extends BasicEntity {
    
    protected String name;
    protected String position;
    protected String email;
    protected String phone;
    
    @Column(length=1000)
    protected String additionalInformation;
    
    protected LecturerDifficulty difficulty;
    
    @JsonIgnore
    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumns({@JoinColumn(name="lecturer_id"),@JoinColumn(name="lecturer_userId")})
    protected Set<ScheduleAction> actions = new LinkedHashSet<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAdditionalInformation() {
        return additionalInformation;
    }

    public void setAdditionalInformation(String additionalInformation) {
        this.additionalInformation = additionalInformation;
    }
    
    public int getDifficulty() {
        return difficulty.getType();
    }

    public void setDifficulty(int difficulty) {
        this.difficulty = LecturerDifficulty.toLecturerDifficulty(difficulty);
    }

    public LecturerDifficulty getDifficultyEnum() {
        return difficulty;
    }

    public void setDifficultyEnum(LecturerDifficulty difficulty) {
        this.difficulty = difficulty;
    }

    public Set<ScheduleAction> getActions() {
        return actions;
    }

    public void setActions(Set<ScheduleAction> actions) {
        this.actions = actions;
    }
    
    @Override
    public void update(BasicEntity entity) {
        if(entity instanceof Lecturer) {
            Lecturer lecturer = (Lecturer)entity;
            super.update(lecturer);
            this.name = lecturer.name;
            this.email = lecturer.email;
            this.phone = lecturer.phone;
            this.difficulty = lecturer.difficulty;
            this.additionalInformation = lecturer.additionalInformation;
            this.position = lecturer.position;
        }
    }
}

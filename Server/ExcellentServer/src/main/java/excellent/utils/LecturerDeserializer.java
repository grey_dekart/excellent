/*
 * Excellent - Student Organizer
 * Copyright (c) 2014, Sergey Pilipenko, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 */
package excellent.utils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.jsontype.TypeDeserializer;
import excellent.models.Lecturer;
import java.io.IOException;

public class LecturerDeserializer extends JsonDeserializer<Lecturer> {

    @Override
    public Lecturer deserialize(JsonParser jp, DeserializationContext dc) throws IOException, JsonProcessingException {
        Lecturer lecturer = new Lecturer();
        lecturer.setId(jp.getText());
        return lecturer;
    }
    
    @Override
    public Lecturer deserializeWithType(JsonParser jp, DeserializationContext dc, TypeDeserializer td) 
            throws IOException, JsonProcessingException {
        return deserialize(jp, dc);
    }
}

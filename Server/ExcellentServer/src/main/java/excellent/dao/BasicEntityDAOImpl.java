/*
 * Excellent - Student Organizer
 * Copyright (c) 2014, Sergey Pilipenko, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 */
package excellent.dao;

import excellent.models.BasicEntity;
import excellent.models.BasicEntityPK;
import excellent.models.User;
import java.util.Date;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class BasicEntityDAOImpl implements BasicEntityDAO {
    
    @Autowired
    SessionFactory sessionFactory;
    
    public BasicEntityDAOImpl() {}
    
    public BasicEntityDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    @Override
    public <T extends BasicEntity> T load(Class<T> entityClass, BasicEntityPK id) {
        return (T)sessionFactory.getCurrentSession().get(entityClass, id);
    }
    
    @Override
    public void remove(BasicEntity entity) {
        sessionFactory.getCurrentSession().delete(entity);
        sessionFactory.getCurrentSession().flush();
    }

    @Override
    public void store(BasicEntity entity) {
        sessionFactory.getCurrentSession().save(entity);
        sessionFactory.getCurrentSession().flush();
    }
    
    @Override
    public void update(BasicEntity entity) {
        sessionFactory.getCurrentSession().update(entity);
        sessionFactory.getCurrentSession().flush();
    }
    
    @Override
    public <T extends BasicEntity> List<T> getUserEntities(Class<T> entityClass, User user) {
        Criteria criteria = sessionFactory.getCurrentSession()
                .createCriteria(entityClass)
                .add(Restrictions.eq("userField", user));
        return (List<T>)criteria.list();
    }
    
    @Override
    public <T extends BasicEntity> List<T> getChangedUserEntities(Class<T> entityClass, 
            User user, Date changeTime) {
        Criteria criteria = sessionFactory.getCurrentSession()
                .createCriteria(entityClass).add(Restrictions.eq("userField", user));
        if(changeTime != null) {
            criteria = criteria.add(Restrictions.ge("synchronizationDate", changeTime));
        }
        return (List<T>)criteria.list();
    }
}
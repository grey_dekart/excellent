/*
 * Excellent - Student Organizer
 * Copyright (c) 2014, Sergey Pilipenko, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 */
package excellent.dao;

import excellent.models.BasicEntity;
import excellent.models.BasicEntityPK;
import excellent.models.User;
import java.util.Date;
import java.util.List;

public interface BasicEntityDAO {
    public <T extends BasicEntity> T load(Class<T> entityClass, BasicEntityPK id);
    public void remove(BasicEntity entity);
    public void store(BasicEntity entity);
    public void update(BasicEntity entity);
    public <T extends BasicEntity> List<T> getUserEntities(Class<T> entityClass, User user);
    public <T extends BasicEntity> List<T> getChangedUserEntities(Class<T> entityClass, 
            User user, Date updateTime);
}

/*
 * Excellent - Student Organizer
 * Copyright (c) 2014, Sergey Pilipenko, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 */
package excellent.server.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import excellent.models.User;
import excellent.server.config.ApplicationContextConfig;
import excellent.server.requests.RegistrationRequest;
import excellent.services.UserService;
import java.util.Date;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {ApplicationContextConfig.class})
@TransactionConfiguration
@Transactional
public class RegistrationContollerTest {
    
    @Autowired
    private UserService userService;
    
    @Autowired
    private WebApplicationContext wac;
    
    private MockMvc mockMvc;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }
    
    @Test
    public void testNormalRegistration() throws Exception {
        RegistrationRequest registrationRequest = new RegistrationRequest();
        registrationRequest.setLogin("john");
        registrationRequest.setEmail("john@test.com");
        registrationRequest.setPassword("12345");
        registrationRequest.setConfirmPassword("12345");
        registrationRequest.setBirthday(new Date());
        ObjectMapper mapper = new ObjectMapper();
        String requestContent = mapper.writeValueAsString(registrationRequest);
        
        this.mockMvc.perform(post("/register")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestContent))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.login", is("john")));
        
        User user = userService.getUserByName("john");
        Assert.assertNotNull(user);
    }
    
    @Test
    public void testPasswordMismatchRegistration() throws Exception {
        RegistrationRequest registrationRequest = new RegistrationRequest();
        registrationRequest.setLogin("john");
        registrationRequest.setEmail("john@test.com");
        registrationRequest.setPassword("12345");
        registrationRequest.setConfirmPassword("1235");
        registrationRequest.setBirthday(new Date());
        ObjectMapper mapper = new ObjectMapper();
        String requestContent = mapper.writeValueAsString(registrationRequest);
        
        this.mockMvc.perform(post("/register")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestContent))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status", is("Error")));
    }
    
    @Test
    public void testUserExistRegistration() throws Exception {
        User user = new User();
        user.setLogin("john");
        user.setEmail("john@test.com");
        userService.store(user);        

        
        RegistrationRequest registrationRequest = new RegistrationRequest();
        registrationRequest.setLogin("john");
        registrationRequest.setEmail("john@test.com");
        registrationRequest.setPassword("12345");
        registrationRequest.setConfirmPassword("12345");
        registrationRequest.setBirthday(new Date());
        ObjectMapper mapper = new ObjectMapper();
        String requestContent = mapper.writeValueAsString(registrationRequest);
        
        this.mockMvc.perform(post("/register")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestContent))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status", is("Error")));
    }
}

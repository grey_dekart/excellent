/*
 * Excellent - Student Organizer
 * Copyright (c) 2014, Sergey Pilipenko, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 */
package excellent.server.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import excellent.models.BasicEntity;
import excellent.models.Event;
import excellent.models.Lecturer;
import excellent.models.Subject;
import excellent.models.User;
import excellent.models.enums.EvaluationType;
import excellent.models.enums.LecturerDifficulty;
import excellent.models.enums.Priority;
import excellent.server.config.ApplicationContextConfig;
import excellent.server.requests.SynchronizationRequest;
import excellent.services.BasicEntityService;
import excellent.services.UserService;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {ApplicationContextConfig.class})
@TransactionConfiguration
@Transactional
public class SyncControllerTest {
    
    @Autowired
    private WebApplicationContext wac;
    
    @Autowired
    private UserService userService;
    
    @Autowired
    private BasicEntityService entityService;
    
    private MockMvc mockMvc;
    private MockHttpSession mockSession;
    
    private final SimpleDateFormat isoFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
        this.mockSession = new MockHttpSession(wac.getServletContext(), UUID.randomUUID().toString());
    }
    
    private User initUser(String email) {
        User u = new User();
        u.setLogin("john");
        u.setEmail(email);
        u.setEnabled(true);
        return u;
    }
    
    private Subject createSubject(String name, User user, Date updated) {
        Subject subject = new Subject();
        subject.setId(UUID.randomUUID().toString());
        subject.setName(name);
        subject.setUserField(user);
        subject.setUpdated(updated);
        subject.setEvaluationTypeEnum(EvaluationType.NONE);
        return subject;
    }
    
    private Event createEvent(String name, User user, Date updated) {
        String id = UUID.randomUUID().toString();
        Event event = new Event();
        event.setId(id);
        event.setUpdated(updated);
        event.setName(name);
        event.setUserField(user);
        event.setPriorityEnum(Priority.NORMAL);
        return event;
    }
    
    private Lecturer createLecturer(String name, User user, Date updated) {
        Lecturer lecturer = new Lecturer();
        lecturer.setId(UUID.randomUUID().toString());
        lecturer.setName(name);
        lecturer.setUserField(user);
        lecturer.setUpdated(updated);
        lecturer.setDifficultyEnum(LecturerDifficulty.NORMAL);
        return lecturer;
    }
    
    @Test
    public void testUnauthorizedSync() throws Exception {
        User user = initUser("john@test.com");
        mockSession.setAttribute("user", user);
        
        User user2 = initUser("john1@test.com");
        userService.store(user2);
        
        Subject subject = createSubject("s1", user2, isoFormat.parse("2014-09-30T00:00:00.000Z"));
        
        SynchronizationRequest syncRequest1 = new SynchronizationRequest();
        syncRequest1.setLastUpdate(null);
        syncRequest1.setEntities(new BasicEntity[] { subject });
        
        ObjectMapper mapper = new ObjectMapper();
        String requestContent1 = mapper.writeValueAsString(syncRequest1);
        this.mockMvc.perform(post("/sync")
                .session(mockSession)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestContent1))
                .andExpect(status().isForbidden())
                .andExpect(jsonPath("$.status", is("Error")));
    }
    
    @Test
    public void testUnlinkedData() throws Exception {
        User user = initUser("john@test.com");
        mockSession.setAttribute("user", user);
        userService.store(user);
        
        Subject subject = createSubject("s1", user, isoFormat.parse("2014-09-30T00:00:00.000Z"));
        Event event = createEvent("e1", user, isoFormat.parse("2014-10-01T00:00:00.000Z"));
        event.setActivity(subject);
        
        SynchronizationRequest syncRequest1 = new SynchronizationRequest();
        syncRequest1.setLastUpdate(null);
        syncRequest1.setEntities(new BasicEntity[] { event });
        
        ObjectMapper mapper = new ObjectMapper();
        String requestContent1 = mapper.writeValueAsString(syncRequest1);
        this.mockMvc.perform(post("/sync")
                .session(mockSession)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestContent1))
                .andExpect(status().isConflict())
                .andExpect(jsonPath("$.status", is("Error")));
    }
    
   
    @Test
    public void testSynchronization() throws Exception {
        User user = initUser("john@test.com");
        mockSession.setAttribute("user", user);
        userService.store(user);
        
        Subject subject = createSubject("s1", user, isoFormat.parse("2014-09-30T00:00:00.000Z"));
        Lecturer lecturer = createLecturer("l1", user, isoFormat.parse("2014-11-01T00:00:00.000Z"));
        Event event1 = createEvent("e1", user, isoFormat.parse("2014-10-01T00:00:00.000Z"));
        event1.setLecturer(lecturer);
        Event event2 = createEvent("e2", user, isoFormat.parse("2014-10-02T00:00:00.000Z"));
        Event event3 = createEvent("e3", user, isoFormat.parse("2014-10-22T00:00:00.000Z"));
        
        
        SynchronizationRequest syncRequest1 = new SynchronizationRequest();
        syncRequest1.setLastUpdate(null);
        syncRequest1.setEntities(new BasicEntity[] {
            subject, event1, event2, event3, lecturer
        });
        
        ObjectMapper mapper = new ObjectMapper();
        String requestContent1 = mapper.writeValueAsString(syncRequest1);
        this.mockMvc.perform(post("/sync")
                .session(mockSession)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestContent1))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(5)));
        
        List<Subject> subjectsTest1 = entityService.getUserEntities(Subject.class, user);
        assertThat(subjectsTest1, hasSize(1));
        assertThat(subjectsTest1, hasItem(hasProperty("name", is("s1"))));
        
        List<Event> eventsTest1 = entityService.getUserEntities(Event.class, user);
        assertThat(eventsTest1, hasSize(3));
        assertThat(eventsTest1, containsInAnyOrder(hasProperty("name", is("e1")), hasProperty("name", is("e2")), 
                hasProperty("name", is("e3"))));
        
        Subject syncSubject = createSubject("s2", user, isoFormat.parse("2014-10-29T00:00:00.000Z"));
        syncSubject.setEvaluationTypeEnum(EvaluationType.TEST);
        syncSubject.setDeleted(true);
        syncSubject.setId(subject.getId());
        
        Event syncEvent1 = createEvent("e1", user, isoFormat.parse("2014-10-13T12:43:59.190Z"));
        syncEvent1.setId(event1.getId());
        syncEvent1.setDescription("test");
        
        Event syncEvent2 = createEvent("e3", user, isoFormat.parse("2014-10-20T00:00:00.000Z"));
        syncEvent2.setId(event3.getId());
        syncEvent2.setDescription("test");
        
        Event syncEvent3 = createEvent("e4", user, isoFormat.parse("2014-10-27T00:00:00.000Z"));
        
        Lecturer syncLecturer = createLecturer("l2", user, isoFormat.parse("2014-11-01T00:00:00.000Z"));
        syncLecturer.setId(lecturer.getId());
        
        Thread.sleep(1000);
        SynchronizationRequest syncRequest2 = new SynchronizationRequest();
        syncRequest2.setLastUpdate(new Date());
        syncRequest2.setEntities(new BasicEntity[] {
            syncSubject, syncEvent1, syncEvent2, syncEvent3, syncLecturer
        });
        
        String requestContent2 = mapper.writeValueAsString(syncRequest2);
        this.mockMvc.perform(post("/sync")
                .session(mockSession)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestContent2))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(4)));
        
        List<Subject> subjectsTest2 = entityService.getUserEntities(Subject.class, user);
        assertThat(subjectsTest2, hasSize(1));
        assertThat(subjectsTest2, hasItem(hasProperty("name", is("s2"))));
        
        List<Event> eventsTest2 = entityService.getUserEntities(Event.class, user);
        assertThat(eventsTest2, hasSize(4));
        assertThat(eventsTest2, containsInAnyOrder(hasProperty("name", is("e1")), hasProperty("name", is("e2")), 
                hasProperty("name", is("e3")), hasProperty("name", is("e4"))));
    }
}

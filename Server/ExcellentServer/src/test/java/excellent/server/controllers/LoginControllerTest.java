/*
 * Excellent - Student Organizer
 * Copyright (c) 2014, Sergey Pilipenko, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 */
package excellent.server.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import excellent.models.User;
import excellent.server.config.ApplicationContextConfig;
import excellent.server.requests.LoginRequest;
import excellent.services.UserService;
import java.util.UUID;
import static org.hamcrest.Matchers.is;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {ApplicationContextConfig.class})
@TransactionConfiguration
@Transactional
public class LoginControllerTest {
    
    @Autowired
    private UserService userService;
    
    @Autowired
    private WebApplicationContext wac;
    
    @Autowired
    private PasswordEncoder passwordEncoder;

    private MockMvc mockMvc;
    private MockHttpSession mockSession;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
        this.mockSession = new MockHttpSession(wac.getServletContext(), UUID.randomUUID().toString());
    }

    @Test
    public void testLogin() throws Exception {
        User user = new User();
        user.setLogin("john");
        user.setEmail("john@test.com");
        user.setEnabled(true);
        user.setPasswordHash(passwordEncoder.encode("12345"));
        userService.store(user);        

        
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setLogin("john");
        loginRequest.setPassword("12345");
        ObjectMapper mapper = new ObjectMapper();
        String requestContent = mapper.writeValueAsString(loginRequest);
        
        this.mockMvc.perform(post("/login")
                .session(mockSession)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestContent))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.email", is("john@test.com")));
        
        Assert.assertNotNull(mockSession.getAttribute("user"));
    }
    
    @Test
    public void testWrongUsernameLogin() throws Exception {
        User user = new User();
        user.setLogin("john");
        user.setEmail("john@test.com");
        user.setEnabled(true);
        user.setPasswordHash(passwordEncoder.encode("12345"));
        userService.store(user);        

        
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setLogin("jane");
        loginRequest.setPassword("12345");
        ObjectMapper mapper = new ObjectMapper();
        String requestContent = mapper.writeValueAsString(loginRequest);
        
        this.mockMvc.perform(post("/login")
                .session(mockSession)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestContent))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status", is("Error")));
        
        Assert.assertNull(mockSession.getAttribute("user"));
    }
    
    @Test
    public void testWrongPasswordLogin() throws Exception {
        User user = new User();
        user.setLogin("john");
        user.setEmail("john@test.com");
        user.setEnabled(true);
        user.setPasswordHash(passwordEncoder.encode("12345"));
        userService.store(user);        

        
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setLogin("john");
        loginRequest.setPassword("qwerty");
        ObjectMapper mapper = new ObjectMapper();
        String requestContent = mapper.writeValueAsString(loginRequest);
        
        this.mockMvc.perform(post("/login")
                .session(mockSession)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestContent))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status", is("Error")));
        
        Assert.assertNull(mockSession.getAttribute("user"));
    }
}

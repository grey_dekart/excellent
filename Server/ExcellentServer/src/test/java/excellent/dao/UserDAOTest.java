/*
 * Excellent - Student Organizer
 * Copyright (c) 2014, Sergey Pilipenko, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 */
package excellent.dao;

import excellent.models.User;
import excellent.server.config.ApplicationContextConfig;
import java.util.Date;
import java.util.UUID;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;


@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {ApplicationContextConfig.class})
@TransactionConfiguration
@Transactional
public class UserDAOTest {
    
    @Autowired
    private UserDAO userDAO;
    
    @Test
    public void testStoreUser() {
        User user = new User();
        user.setLogin("John");
        user.setEmail("john@test.com");
        user.setBirthday(new Date());
        userDAO.store(user);
        
        Assert.assertNotNull(user.getId());
    }
    
    @Test
    public void testLoadUser() {
        String id = UUID.randomUUID().toString();
        User dbUser1 = userDAO.load(id);
        Assert.assertNull(dbUser1);
        
        User user = new User();
        user.setLogin("John");
        user.setEmail("john@test.com");
        user.setBirthday(new Date());
        userDAO.store(user);
        
        User dbUser2 = userDAO.load(user.getId());
        Assert.assertEquals(user, dbUser2);
        Assert.assertEquals(user.getLogin(), dbUser2.getLogin());
        Assert.assertEquals(user.getEmail(), dbUser2.getEmail());
        Assert.assertEquals(user.getBirthday(), dbUser2.getBirthday());
    }
    
    @Test
    public void testRemoveUser() {
        User tempUser = new User();
        tempUser.setLogin("Jane");
        tempUser.setEmail("jane@google.com");
        userDAO.store(tempUser);
        
        userDAO.remove(tempUser);

        User dbUser = userDAO.load(tempUser.getId());
        Assert.assertNull(dbUser);
    }
    
    @Test
    public void testGetByName() {
        User user = new User();
        user.setLogin("John");
        user.setEmail("john@test.com");
        userDAO.store(user);
        
        User dbUser = userDAO.getUserByName(user.getLogin());
        Assert.assertEquals(user, dbUser);
    }
}

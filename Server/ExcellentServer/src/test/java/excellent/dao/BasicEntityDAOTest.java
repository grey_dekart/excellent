/*
 * Excellent - Student Organizer
 * Copyright (c) 2014, Sergey Pilipenko, All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 */
package excellent.dao;

import excellent.models.BasicEntityPK;
import excellent.models.Event;
import excellent.models.Lecturer;
import excellent.models.Place;
import excellent.models.User;
import excellent.server.config.ApplicationContextConfig;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {ApplicationContextConfig.class})
@TransactionConfiguration
@Transactional
public class BasicEntityDAOTest {
    
    @Autowired
    private BasicEntityDAO entityDAO;
    
    @Autowired
    private UserDAO userDAO;
    
    private User createUser(String name) {
        User user = new User();
        user.setLogin(name);
        return user;
    }
    
    @Test
    public void testStoreLoad() {
        User user = createUser("John");
        userDAO.store(user);
        
        String id = UUID.randomUUID().toString();
        BasicEntityPK key = new BasicEntityPK();
        key.setId(id);
        key.setUserField(user);
        
        Lecturer lecturer = new Lecturer();
        lecturer.setId(id);
        lecturer.setUserField(user);
        lecturer.setName("TestName");
        lecturer.setEmail("test@gmail.com");
        entityDAO.store(lecturer);
        
        Lecturer dbLecturer = entityDAO.load(Lecturer.class, key);
        Assert.assertEquals(lecturer, dbLecturer);
        Assert.assertEquals(lecturer.getUserField(), dbLecturer.getUserField());
        Assert.assertEquals(lecturer.getName(), dbLecturer.getName());
        Assert.assertEquals(lecturer.getEmail(), dbLecturer.getEmail());
    }
      
    @Test
    public void testRemove() {
        User user = createUser("John");
        userDAO.store(user);
        
        String id = UUID.randomUUID().toString();
        BasicEntityPK key = new BasicEntityPK();
        key.setId(id);
        key.setUserField(user);
        
        Place place = new Place();
        place.setId(id);
        place.setUserField(user);
        place.setAddress("Kiev");
        entityDAO.store(place);
        
        entityDAO.remove(place);

        Place dbPlace = entityDAO.load(Place.class, key);
        Assert.assertNull(dbPlace);
    }
    
    private Event createEvent(String name, User user, Date updated) {
        String id = UUID.randomUUID().toString();
        Event event = new Event();
        event.setId(id);
        event.setSynchronizationDate(updated);
        event.setName(name);
        event.setUserField(user);
        return event;
    }
    
    @Test
    public void testGetByUser() {
        User user1 = createUser("John");
        userDAO.store(user1);
        
        User user2 = createUser("Jane");
        userDAO.store(user2);
        
        Event event1 = createEvent("1", user1, null);
        entityDAO.store(event1);
        Event event2 = createEvent("2", user2, null);
        entityDAO.store(event2);
        Event event3 = createEvent("3", user1, null);
        entityDAO.store(event3);
        
        List<Event> johnEvents = entityDAO.getUserEntities(Event.class, user1);
        Assert.assertEquals(2, johnEvents.size());
        
        List<Event> janeEvents = entityDAO.getUserEntities(Event.class, user2);
        Assert.assertEquals(1, janeEvents.size());
        
        Event dbEvent = janeEvents.get(0);
        Assert.assertEquals(event2.getName(), dbEvent.getName());
    }
    
    @Test
    public void testUpdate() {
        User user = createUser("John");
        userDAO.store(user);
        
        String id = UUID.randomUUID().toString();
        BasicEntityPK key = new BasicEntityPK();
        key.setId(id);
        key.setUserField(user);
        
        Lecturer lecturer = new Lecturer();
        lecturer.setId(id);
        lecturer.setUserField(user);
        lecturer.setName("TestName");
        lecturer.setEmail("test@gmail.com");
        entityDAO.store(lecturer);
        
        lecturer.setName("123");
        entityDAO.update(lecturer);
        
        Lecturer dbLecturer = entityDAO.load(Lecturer.class, key);
        Assert.assertEquals(lecturer.getName(), dbLecturer.getName());
    }
    
    @Test
    public void testGetUpdatedEntities() {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date updated = dateFormat.parse("2014-11-02 12:33:57");

            User user = createUser("John");
            userDAO.store(user);

            Event event1 = createEvent("1", user, dateFormat.parse("2014-11-05 20:33:00"));
            entityDAO.store(event1);
            Event event2 = createEvent("2", user, null);
            entityDAO.store(event2);
            Event event3 = createEvent("3", user, dateFormat.parse("2014-10-29 00:27:35"));
            entityDAO.store(event3);
            Event event4 = createEvent("4", user, dateFormat.parse("2014-11-02 12:33:56"));
            entityDAO.store(event4);
            Event event5 = createEvent("5", user, dateFormat.parse("2014-11-02 12:33:57"));
            entityDAO.store(event5);

            List<Event> updatedEvents = entityDAO.getChangedUserEntities(Event.class, user, updated);
            Assert.assertEquals(2, updatedEvents.size());

            updatedEvents.stream().forEach((dbEvent) -> {
                Assert.assertTrue(dbEvent.getSynchronizationDate().compareTo(updated) >= 0);
            });
        } catch(ParseException ex) {}
    }
}
